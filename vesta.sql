-- MySQL dump 10.13  Distrib 5.6.21, for Win32 (x86)
--
-- Host: localhost    Database: vestatavan
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `vtf_cache`
--

DROP TABLE IF EXISTS `vtf_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_cache` (
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE KEY `cache_key_unique` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_cache`
--

LOCK TABLES `vtf_cache` WRITE;
/*!40000 ALTER TABLE `vtf_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `vtf_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_companies`
--

DROP TABLE IF EXISTS `vtf_companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_name` varchar(30) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_name` varchar(255) DEFAULT NULL,
  `ru_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fa_name` (`fa_name`),
  KEY `en_name` (`en_name`),
  KEY `ru_name` (`ru_name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_companies`
--

LOCK TABLES `vtf_companies` WRITE;
/*!40000 ALTER TABLE `vtf_companies` DISABLE KEYS */;
INSERT INTO `vtf_companies` VALUES (3,'بهینه سازان صنعت تاسیسات','backend/assets/images/company/1446961621behineh-sazan-logo.jpg','www.behineh-sazan.ir','2015-09-29 03:26:04','2015-11-08 02:17:02','Behineh Sazaneh Sanat Tasisat','Behineh Sazaneh Sanat Tasisat'),(4,'کنترل صنعت فراگیر','backend/assets/images/company/1447328990Logo-CSF.jpg','www.csfco.co','2015-11-12 08:18:36','2015-11-12 08:19:50','CONTROL SANAT FARAGIR','CONTROL SANAT FARAGIR'),(5,'ماد صنعت آذربایجان','backend/assets/images/company/1447330931Logo-MSA (2).jpg','www.maadsanat.com','2015-11-12 08:27:01','2015-11-12 08:52:11','maad sanat azerbayjan','maad sanat azerbayjan'),(6,'پارس ترک سیلو','backend/assets/images/company/Logo-ptsilo.jpg','www.ptsilo.com','2015-11-12 09:00:18','2015-11-12 09:00:18','pars turke  silo','pars turke  silo'),(7,'MSA','backend/assets/images/company/MSA-Electronics.jpg','www.maadsanat.com','2015-11-12 09:25:18','2015-11-12 09:25:18','MSA','MSA'),(8,'نساجی دالاهو','backend/assets/images/company/DALAHOO.jpg','#','2015-11-12 09:40:07','2015-11-12 09:40:07','DALAHOO TEXTILE','DALAHOO TEXTILE');
/*!40000 ALTER TABLE `vtf_companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_company_categories`
--

DROP TABLE IF EXISTS `vtf_company_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_company_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_name` varchar(50) DEFAULT NULL,
  `en_name` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ru_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_company_categories`
--

LOCK TABLES `vtf_company_categories` WRITE;
/*!40000 ALTER TABLE `vtf_company_categories` DISABLE KEYS */;
INSERT INTO `vtf_company_categories` VALUES (1,'اتوماسیون صنعتی','industrial automation','2015-10-12 10:24:29','2015-10-12 12:00:23','Индустриальная автоматизация'),(2,'توزین','Weighing','2015-10-12 12:02:31','2015-10-12 12:02:31','взвешивание'),(3,'پژوهش و فناوری','','2015-10-13 10:23:59','2015-10-13 10:23:59','');
/*!40000 ALTER TABLE `vtf_company_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_company_company_category`
--

DROP TABLE IF EXISTS `vtf_company_company_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_company_company_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(10) unsigned NOT NULL,
  `company_category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `company_company_category_company_id_index` (`company_id`),
  KEY `company_company_category_company_category_id_index` (`company_category_id`),
  CONSTRAINT `company_company_category_company_category_id_foreign` FOREIGN KEY (`company_category_id`) REFERENCES `vtf_company_categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `company_company_category_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `vtf_companies` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_company_company_category`
--

LOCK TABLES `vtf_company_company_category` WRITE;
/*!40000 ALTER TABLE `vtf_company_company_category` DISABLE KEYS */;
INSERT INTO `vtf_company_company_category` VALUES (4,3,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,4,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,4,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,5,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,5,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,5,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,6,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,7,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,8,3,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vtf_company_company_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_datasheet_codes`
--

DROP TABLE IF EXISTS `vtf_datasheet_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_datasheet_codes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `short` varchar(120) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_datasheet_codes`
--

LOCK TABLES `vtf_datasheet_codes` WRITE;
/*!40000 ALTER TABLE `vtf_datasheet_codes` DISABLE KEYS */;
INSERT INTO `vtf_datasheet_codes` VALUES (1,'CSF-01201201-SCS','2015-08-31 02:43:29','2015-08-31 02:43:29'),(3,'ECO-01201301-IAU','2015-11-01 04:42:44','2015-11-01 04:42:44'),(4,'CSF-11201309-EBSYS','2015-11-01 04:42:59','2015-11-01 04:42:59'),(5,'PTSILO-02201304-EBSYS','2015-11-11 04:05:19','2015-11-11 04:05:19');
/*!40000 ALTER TABLE `vtf_datasheet_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_datasheet_companies`
--

DROP TABLE IF EXISTS `vtf_datasheet_companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_datasheet_companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_name` varchar(50) NOT NULL,
  `en_name` varchar(50) NOT NULL,
  `short` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_datasheet_companies`
--

LOCK TABLES `vtf_datasheet_companies` WRITE;
/*!40000 ALTER TABLE `vtf_datasheet_companies` DISABLE KEYS */;
INSERT INTO `vtf_datasheet_companies` VALUES (1,'کنترل صنعت فراگیر','CSF.CO','CSF','2015-08-29 05:05:18','2015-11-01 01:42:14'),(2,'بهینه سازان','energy consumption optimizers','ECO','2015-08-29 05:16:54','2015-11-01 01:42:26'),(3,'پارس ترک سیلو','pars turk silo','PTSILO','2015-08-29 05:17:32','2015-11-01 01:42:43');
/*!40000 ALTER TABLE `vtf_datasheet_companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_datasheet_datasheet_code`
--

DROP TABLE IF EXISTS `vtf_datasheet_datasheet_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_datasheet_datasheet_code` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datasheet_id` int(10) unsigned NOT NULL,
  `datasheet_code_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `datasheet_datasheet_code_datasheet_id_index` (`datasheet_id`),
  KEY `datasheet_datasheet_code_datasheet_code_id_index` (`datasheet_code_id`),
  CONSTRAINT `datasheet_datasheet_code_datasheet_code_id_foreign` FOREIGN KEY (`datasheet_code_id`) REFERENCES `vtf_datasheet_codes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `datasheet_datasheet_code_datasheet_id_foreign` FOREIGN KEY (`datasheet_id`) REFERENCES `vtf_datasheets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_datasheet_datasheet_code`
--

LOCK TABLES `vtf_datasheet_datasheet_code` WRITE;
/*!40000 ALTER TABLE `vtf_datasheet_datasheet_code` DISABLE KEYS */;
INSERT INTO `vtf_datasheet_datasheet_code` VALUES (2,3,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,3,4,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vtf_datasheet_datasheet_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_datasheet_group`
--

DROP TABLE IF EXISTS `vtf_datasheet_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_datasheet_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `datasheet_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `datasheet_group_datasheet_id_index` (`datasheet_id`),
  KEY `datasheet_group_group_id_index` (`group_id`),
  CONSTRAINT `datasheet_group_datasheet_id_foreign` FOREIGN KEY (`datasheet_id`) REFERENCES `vtf_datasheets` (`id`) ON DELETE CASCADE,
  CONSTRAINT `datasheet_group_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `vtf_groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_datasheet_group`
--

LOCK TABLES `vtf_datasheet_group` WRITE;
/*!40000 ALTER TABLE `vtf_datasheet_group` DISABLE KEYS */;
INSERT INTO `vtf_datasheet_group` VALUES (1,1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,2,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,3,4,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vtf_datasheet_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_datasheet_project_types`
--

DROP TABLE IF EXISTS `vtf_datasheet_project_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_datasheet_project_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_name` varchar(40) NOT NULL,
  `en_name` varchar(40) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `short` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_datasheet_project_types`
--

LOCK TABLES `vtf_datasheet_project_types` WRITE;
/*!40000 ALTER TABLE `vtf_datasheet_project_types` DISABLE KEYS */;
INSERT INTO `vtf_datasheet_project_types` VALUES (1,'کنترل سیستم سیلو','Silo Control System','2015-08-29 07:37:02','2015-08-29 07:42:11','SCS'),(2,'اتوماسیون صنعتی','industrial automation ','2015-11-01 04:41:35','2015-11-01 04:41:35','IAU'),(3,'سیستم های پنهان','embedded systems','2015-11-01 04:42:17','2015-11-01 04:42:17','EBSYS');
/*!40000 ALTER TABLE `vtf_datasheet_project_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_datasheets`
--

DROP TABLE IF EXISTS `vtf_datasheets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_datasheets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_name` varchar(50) NOT NULL,
  `file` varchar(500) NOT NULL,
  `fa_description` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_name` varchar(50) DEFAULT NULL,
  `en_description` text,
  PRIMARY KEY (`id`),
  KEY `file` (`file`(255))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_datasheets`
--

LOCK TABLES `vtf_datasheets` WRITE;
/*!40000 ALTER TABLE `vtf_datasheets` DISABLE KEYS */;
INSERT INTO `vtf_datasheets` VALUES (1,'بللالا','backend/assets/images/datasheet/1_376_65_t94-07-15.pdf','<p>لااا</p>\r\n','2015-11-01 02:48:45','2015-11-01 02:48:45','ggg','<p>gggfr545</p>\r\n'),(2,'بللالا','backend/assets/images/datasheet/1_376_65_t94-07-15.pdf','<p>لااا</p>\r\n','2015-11-01 02:58:15','2015-11-01 02:58:15','ggg','<p>gggfr545</p>\r\n'),(3,'fghghgf','backend/assets/images/datasheet/531-643.pdf','<p>hfghfgh</p>\r\n','2015-11-01 04:43:27','2015-11-01 04:43:27','fghfghfg','<p>hfgbvbgfhrtbgb</p>\r\n');
/*!40000 ALTER TABLE `vtf_datasheets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_elearning_categories`
--

DROP TABLE IF EXISTS `vtf_elearning_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_elearning_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_ecat_title` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_ecat_title` varchar(255) DEFAULT NULL,
  `ru_ecat_title` varchar(50) DEFAULT NULL,
  `ru_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_elearning_categories`
--

LOCK TABLES `vtf_elearning_categories` WRITE;
/*!40000 ALTER TABLE `vtf_elearning_categories` DISABLE KEYS */;
INSERT INTO `vtf_elearning_categories` VALUES (1,'برق','2015-07-04 04:56:29','2015-10-03 09:03:57','bargh',NULL,NULL),(2,'اتوماسیون','2015-07-04 08:13:54','2015-10-03 09:04:06','automation',NULL,NULL);
/*!40000 ALTER TABLE `vtf_elearning_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_elearning_elearning_category`
--

DROP TABLE IF EXISTS `vtf_elearning_elearning_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_elearning_elearning_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `elearning_id` int(10) unsigned NOT NULL,
  `elearning_category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `elearning_elearning_category_elearning_id_index` (`elearning_id`),
  KEY `elearning_elearning_category_elearning_category_id_index` (`elearning_category_id`),
  CONSTRAINT `elearning_elearning_category_elearning_category_id_foreign` FOREIGN KEY (`elearning_category_id`) REFERENCES `vtf_elearning_categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `elearning_elearning_category_elearning_id_foreign` FOREIGN KEY (`elearning_id`) REFERENCES `vtf_elearnings` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_elearning_elearning_category`
--

LOCK TABLES `vtf_elearning_elearning_category` WRITE;
/*!40000 ALTER TABLE `vtf_elearning_elearning_category` DISABLE KEYS */;
INSERT INTO `vtf_elearning_elearning_category` VALUES (12,6,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,5,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,4,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,6,1,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vtf_elearning_elearning_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_elearnings`
--

DROP TABLE IF EXISTS `vtf_elearnings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_elearnings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_title` varchar(100) NOT NULL,
  `fa_body` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_title` varchar(100) DEFAULT NULL,
  `en_body` text,
  `ru_title` varchar(100) DEFAULT NULL,
  `ru_body` text,
  `img` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_elearnings`
--

LOCK TABLES `vtf_elearnings` WRITE;
/*!40000 ALTER TABLE `vtf_elearnings` DISABLE KEYS */;
INSERT INTO `vtf_elearnings` VALUES (4,'حضور وستا توان در نمایشگاه صنعت تهران 95','<p style=\"text-align: right;\">هدف اصلی از شرکت در هر نمایشگاه داخلی و خارجی، در کوتاه مدت یا درازمدت افزایش فروش تولیدات یا خدمات است. گاهی نیز اهداف دیگری از قبیل مقاصد سیاسی و فرهنگی برای برگزاری یا شرکت در نمایشگاهی عنوان می شود. هدف هر چه باشد برگزاری یک نمایشگاه بازرگانی موفق و اصولی می تواند زمینه های لازم برای گسترش مناسب سیاسی و فرهنگی و اقتصادی مورد نظر را فراهم آورد.</p>\r\n','2015-10-13 10:06:18','2015-11-04 04:21:22','Vesta presence in the exhibition Tehran 95','<p>The main objective of the company in any domestic and international exhibitions, short-term or long-term increase in sales of products or services. Sometimes also other objectives such as political and cultural objectives for holding or participating in the exhibition title.</p>\r\n','Веста присутствие в выставке в Тегеране 95','<p>Основная цель компании в любых отечественных и международных выставках, краткосрочный или долгосрочный рост продаж продукции или услуг. Иногда также другие цели, такие как политические и культурные цели для проведения или участия в выставке титул. Независимо от цели проведения успешной торговли справедливые и соответствующие принципы могут быть основанием для политического, культурного и экономического развития обеспечены.</p>\r\n','backend/assets/images/news/vinyl-decal-sticker-4356.jpg'),(5,'حضور وستا توان در نمایشگاه صنعت تهران 95','<p>هدف اصلی از شرکت در هر نمایشگاه داخلی و خارجی، در کوتاه مدت یا درازمدت افزایش فروش تولیدات یا خدمات است. گاهی نیز اهداف دیگری از قبیل مقاصد سیاسی و فرهنگی برای برگزاری یا شرکت در نمایشگاهی عنوان می شود. هدف هر چه باشد برگزاری یک نمایشگاه بازرگانی موفق و اصولی می تواند زمینه های لازم برای گسترش مناسب سیاسی و فرهنگی و اقتصادی مورد نظر را فراهم آورد.</p>\r\n','2015-10-13 10:11:37','2015-11-03 10:05:33','Vesta presence in the exhibition Tehran 95','<p>The main objective of the company in any domestic and international exhibitions, short-term or long-term increase in sales of products or services. Sometimes also other objectives such as political and cultural objectives for holding or participating in the exhibition title.</p>\r\n','Веста присутствие в выставке в Тегеране 95','<p>Основная цель компании в любых отечественных и международных выставках, краткосрочный или долгосрочный рост продаж продукции или услуг. Иногда также другие цели, такие как политические и культурные цели для проведения или участия в выставке титул. Независимо от цели проведения успешной торговли справедливые и соответствующие принципы могут быть основанием для политического, культурного и экономического развития обеспечены.</p>\r\n','backend/assets/images/news/vinyl-decal-sticker-4356.jpg'),(6,'حضور وستا توان در نمایشگاه صنعت ','<p>هدف اصلی از شرکت در هر نمایشگاه داخلی و خارجی، در کوتاه مدت یا درازمدت افزایش فروش تولیدات یا خدمات است. گاهی نیز اهداف دیگری از قبیل مقاصد سیاسی و فرهنگی برای برگزاری یا شرکت در نمایشگاهی عنوان می شود. هدف هر چه باشد برگزاری یک نمایشگاه بازرگانی موفق و اصولی می تواند زمینه های لازم برای گسترش مناسب سیاسی و فرهنگی و اقتصادی مورد نظر را فراهم آورد.</p>\r\n','2015-10-13 10:11:48','2015-11-03 10:05:40','Vesta presence in the exhibition Tehran 95','<p>The main objective of the company in any domestic and international exhibitions, short-term or long-term increase in sales of products or services. Sometimes also other objectives such as political and cultural objectives for holding or participating in the exhibition title.</p>\r\n','Веста присутствие в выставке в Тегеране 95','<p>Основная цель компании в любых отечественных и международных выставках, краткосрочный или долгосрочный рост продаж продукции или услуг. Иногда также другие цели, такие как политические и культурные цели для проведения или участия в выставке титул. Независимо от цели проведения успешной торговли справедливые и соответствующие принципы могут быть основанием для политического, культурного и экономического развития обеспечены.</p>\r\n','backend/assets/images/news/refri.png');
/*!40000 ALTER TABLE `vtf_elearnings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_group_user`
--

DROP TABLE IF EXISTS `vtf_group_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_group_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `group_user_group_id_index` (`group_id`),
  KEY `group_user_user_id_index` (`user_id`),
  CONSTRAINT `group_user_group_id_foreign` FOREIGN KEY (`group_id`) REFERENCES `vtf_groups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `group_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `vtf_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_group_user`
--

LOCK TABLES `vtf_group_user` WRITE;
/*!40000 ALTER TABLE `vtf_group_user` DISABLE KEYS */;
INSERT INTO `vtf_group_user` VALUES (4,2,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,3,5,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vtf_group_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_groups`
--

DROP TABLE IF EXISTS `vtf_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_name` varchar(30) NOT NULL,
  `en_name` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_groups`
--

LOCK TABLES `vtf_groups` WRITE;
/*!40000 ALTER TABLE `vtf_groups` DISABLE KEYS */;
INSERT INTO `vtf_groups` VALUES (1,'شرکت گاز','gaz','2015-09-30 05:32:57','2015-09-30 05:32:57'),(2,'مدیر','admin','2015-09-30 05:33:57','2015-09-30 05:33:57'),(3,'بهینه سازان','optimizers','2015-11-01 04:39:26','2015-11-01 04:39:26'),(4,'زر ماکارون','zar makaron','2015-11-01 04:40:06','2015-11-01 04:40:06');
/*!40000 ALTER TABLE `vtf_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_medias`
--

DROP TABLE IF EXISTS `vtf_medias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_medias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` int(11) NOT NULL,
  `address` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `m_title` varchar(35) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_medias`
--

LOCK TABLES `vtf_medias` WRITE;
/*!40000 ALTER TABLE `vtf_medias` DISABLE KEYS */;
INSERT INTO `vtf_medias` VALUES (18,21,'backend/assets/images/medias/pdf//manual511en1.pdf','2015-07-11 08:01:41','2015-07-11 08:01:41','sdcd'),(19,11,'backend/assets/images/medias/image//DSC_0144.jpg','2015-07-11 08:01:52','2015-07-11 08:01:52',''),(20,21,'backend/assets/images/medias/pdf//monitoring.pdf','2015-07-11 08:02:46','2015-07-11 08:02:46',''),(21,11,'backend/assets/images/medias/image/1.jpg','2015-07-14 11:07:03','2015-07-14 11:07:03',''),(22,11,'backend/assets/images/medias/image/5.jpg','2015-07-14 11:15:45','2015-07-14 11:15:45',''),(23,11,'backend/assets/images/medias/image/4.jpg','2015-07-14 11:30:46','2015-07-14 11:30:46',''),(24,11,'backend/assets/images/medias/image/2.jpg','2015-07-14 12:27:42','2015-07-14 12:27:42',''),(25,11,'backend/assets/images/medias/image/3.jpg','2015-07-14 12:28:14','2015-07-14 12:28:14',''),(26,11,'backend/assets/images/medias/image/7eb00d8ce019c1183c648c0ebf98224d.jpg','2015-07-14 12:34:29','2015-07-14 12:34:29','');
/*!40000 ALTER TABLE `vtf_medias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_menus`
--

DROP TABLE IF EXISTS `vtf_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_name` varchar(255) NOT NULL,
  `url` varchar(500) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `menus_parent_id_foreign` (`parent_id`),
  CONSTRAINT `menus_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `vtf_menus` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_menus`
--

LOCK TABLES `vtf_menus` WRITE;
/*!40000 ALTER TABLE `vtf_menus` DISABLE KEYS */;
/*!40000 ALTER TABLE `vtf_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_migrations`
--

DROP TABLE IF EXISTS `vtf_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_migrations`
--

LOCK TABLES `vtf_migrations` WRITE;
/*!40000 ALTER TABLE `vtf_migrations` DISABLE KEYS */;
INSERT INTO `vtf_migrations` VALUES ('2015_05_31_073825_create_menus_table',1),('2015_05_31_080433_menu_parent',1),('2015_06_03_135834_create_team_categories_table',1),('2015_06_03_140405_create_teams_table',1),('2015_06_16_093225_create_team_profession_categories_table',1),('2015_06_17_041015_timestamp',1),('2015_06_18_104110_title_to_other',1),('2015_06_21_105647_create_projects_table',1),('2015_06_21_112806_create_companies_table',1),('2015_06_21_113537_create_project_categories_table',1),('2015_06_21_113818_create_project_project_category_table',1),('2015_06_23_115646_delete_company',1),('2015_06_30_045101_image_size',1),('2015_06_30_110115_create_portfolio_portfolio_category_table',2),('2015_07_01_082017_create_elearnings_table',3),('2015_07_04_061749_create_medias_table',4),('2015_07_04_063648_create_elearning_categories_table',5),('2015_07_04_073909_create_elearning_elearning_category_table',6),('2015_07_04_131315_title_field',7),('2015_07_08_122117_media_title',8),('2015_07_12_140628_create_products_table',9),('2015_07_12_141447_create_product_gategories_table',10),('2015_07_15_071433_create_product_product_category_table',11),('2015_07_21_120317_del_menu',12),('2015_07_21_121323_create_menus_table',13),('2012_12_06_225921_migration_cartalyst_sentry_install_users',14),('2012_12_06_225929_migration_cartalyst_sentry_install_groups',14),('2012_12_06_225945_migration_cartalyst_sentry_install_users_groups_pivot',14),('2012_12_06_225988_migration_cartalyst_sentry_install_throttle',14),('2015_07_22_105550_menu_url',15),('2015_07_22_115037_menu_delete_url',16),('2015_07_22_120455_create_pages_table',16),('2015_07_27_054059_multi_lang',17),('2015_07_27_110939_create_session_table',18),('2015_07_27_110955_create_password_reminders_table',19),('2015_08_02_043135_menu_url',20),('2015_08_25_073409_create_datasheets_table',21),('2015_08_25_122225_create_datasheet_codes_table',22),('2015_08_25_125901_en_datasheet',23),('2015_08_26_042823_create_datasheet_companies_table',24),('2015_08_26_130033_datasheet_project_type',25),('2015_08_26_130625_create_Datasheet_DatasheetCode_table',26),('2015_08_26_134750_datasheet_code_change',27),('2015_08_27_071134_datasheetcode_short',28),('2015_08_31_061632_delete_company_code',29),('2015_08_31_071048_code_time',30),('2015_09_02_085156_create_datasheet_customers_table',31),('2015_09_03_102329_code_user',32),('2015_09_14_081402_create_cache_table',33),('2015_09_30_062516_create_password_reminders_table',34),('2015_09_30_062841_users',35),('2015_09_30_063858_users',36),('2015_09_30_064340_groups',37),('2015_09_30_075722_create_group_user_table',38),('2015_09_30_101107_time_for_user',39),('2015_10_03_092444_group_datasheet',40),('2015_10_04_053548_create_services_table',41),('2015_10_04_073627_create_service_categories_table',42),('2015_10_04_074456_create_service_service_category_table',43),('2015_10_04_081709_service_desc',44),('2015_10_05_055558_russian_lang',45),('2015_10_06_083408_create_sliders_table',46),('2015_10_06_123812_animate_slider',47),('2015_10_07_103939_portfolio_top',48),('2015_10_10_052208_create_settings_table',49),('2015_10_12_074153_product_image',50),('2015_10_12_131323_create_company_categories_table',51),('2015_10_12_131520_ru_c_category',52),('2015_10_12_134529_create_company_company_category_table',53),('2015_10_13_071530_img_elearning',54),('2015_10_13_131032_create_portfolio_customers_table',55),('2015_10_15_073738_thumbnail_portfolio',56),('2015_10_15_083320_create_portfolio_portfolio_customer_table',57),('2015_10_31_132132_create_datasheet_group_table',58),('2015_11_01_062607_create_datasheet_datasheet_code_table',59);
/*!40000 ALTER TABLE `vtf_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_pages`
--

DROP TABLE IF EXISTS `vtf_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_title` varchar(50) NOT NULL,
  `fa_content` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_title` varchar(50) DEFAULT NULL,
  `en_content` text,
  `ru_title` varchar(50) DEFAULT NULL,
  `ru_content` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_pages`
--

LOCK TABLES `vtf_pages` WRITE;
/*!40000 ALTER TABLE `vtf_pages` DISABLE KEYS */;
INSERT INTO `vtf_pages` VALUES (5,'ارزشهای سازمانی','<p dir=\"rtl\">ه طور سنتی، مقوله مدیریت طراحی تنها محدود به مدیریت پروژه های طراحی و دیزاین دیده می شد. اما در طول زمان، به تدریج دیگر جنبه های سازمانی در سطوح کاربردی تا راهبردی را در بر گرفت. در سالهای اخیر نگرشی ایجاد شده است که تلفیق&nbsp;<a href=\"https://fa.wikipedia.org/w/index.php?title=%D8%AA%D9%81%DA%A9%D8%B1_%D8%B7%D8%B1%D8%A7%D8%AD%DB%8C&amp;action=edit&amp;redlink=1&amp;preload=%D8%A7%D9%84%DA%AF%D9%88%3A%D8%A7%DB%8C%D8%AC%D8%A7%D8%AF%2B%D9%85%D9%82%D8%A7%D9%84%D9%87%2F%D8%A7%D8%B3%D8%AA%D8%AE%D9%88%D8%A7%D9%86%E2%80%8C%D8%A8%D9%86%D8%AF%DB%8C&amp;editintro=%D8%A7%D9%84%DA%AF%D9%88%3A%D8%A7%DB%8C%D8%AC%D8%A7%D8%AF%2B%D9%85%D9%82%D8%A7%D9%84%D9%87%2F%D8%A7%D8%AF%DB%8C%D8%AA%E2%80%8C%D9%86%D9%88%D8%AA%DB%8C%D8%B3&amp;summary=%D8%A7%DB%8C%D8%AC%D8%A7%D8%AF%2B%DB%8C%DA%A9%2B%D9%85%D9%82%D8%A7%D9%84%D9%87%2B%D9%86%D9%88%2B%D8%A7%D8%B2%2B%D8%B7%D8%B1%DB%8C%D9%82%2B%D8%A7%DB%8C%D8%AC%D8%A7%D8%AF%DA%AF%D8%B1&amp;nosummary=&amp;prefix=&amp;minor=&amp;create=%D8%AF%D8%B1%D8%B3%D8%AA%2B%DA%A9%D8%B1%D8%AF%D9%86%2B%D9%85%D9%82%D8%A7%D9%84%D9%87%2B%D8%AC%D8%AF%DB%8C%D8%AF\" title=\"تفکر طراحی (صفحه وجود ندارد)\">تفکر طراحی</a>&nbsp;(تفکر دیزاین) با مدیریت استراتژیک (راهبردی) را به عنوان رویکردی میان رشته و انسان محور به دانش مدیریت در نظر می گیرد.</p>\r\n','2015-09-12 06:04:30','2015-10-13 11:01:58','Organizational values','<p>The&nbsp;Statement on the Co-operative Identity, promulgated by the&nbsp;International Co-operative Alliance&nbsp;(ICA),The&nbsp;Statement on the Co-operative Identity, promulgated by the&nbsp;International Co-operative Alliance&nbsp;(ICA),The&nbsp;Statement on the Co-operative Identity,</p>\r\n','Заявление на ','<p>Заявление на Кооперативной идентичности, обнароЗаявление на Кооперативной идентичности, обнароЗаявление на Кооперативной идентичности, обнароЗаявление на Кооперативной идентичности, обнароЗаявление на Кооперативной&nbsp;</p>\r\n'),(6,'????΍','<p dir=\"rtl\">????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?????΍?</p>\r\n','2015-09-12 06:07:29','2015-09-12 06:07:29','History','<p>HistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistoryHistory',NULL,NULL),(7,'?????ϐ? ?? ','<p dir=\"rtl\">?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;?????ϐ? ??&nbsp;</p>\r\n','2015-09-12 06:08:07','2015-09-12 06:08:07','Branches','<p>BranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBranchesBran',NULL,NULL),(8,'?????? ?????','<p dir=\"rtl\">?????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ????????????? ???????</p>\r\n','2015-09-12 06:09:06','2015-09-12 06:09:06','Mission Statement','<p>Mission StatementMission StatementMission StatementMission StatementMission StatementMission StatementMission StatementMission StatementMission StatementMission StatementMission StatementMission StatementMission StatementMission StatementMission Statem',NULL,NULL),(9,'??????','<p dir=\"rtl\">???????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????</p>\r\n','2015-09-12 06:10:05','2015-09-12 06:10:05','testimonials','<p>testimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonialstestimonials',NULL,NULL),(11,'اهداف و آینده نگری','<p dir=\"rtl\">وجود این گونه هماهنگی ها باعث شده شرکت وستا توان یک روند خوب که می توان با سوابق کاری که داشته، در این چند سال اخیر ثابت استوار بماند وبتواند با سامان دهی و ایجاد یک نظم متفکرانه به سمت اهداف خود حرکت کند. در این راستا شرکت همچنان بر مهارت های مناسب در برابر تصمیم گیری و رفتارهای خودتخریبانه پافشاری می کند تا در برابر تکانه ها و هوس های آنی تسلیم نشود.<br />\r\n&nbsp;</p>\r\n','2015-09-12 06:11:36','2015-10-13 11:00:30','Goals and vision','<p>In this regard, the company will continue to decide on the appropriate skills and behaviors persist Khvdtkhrybanh to shocks and do not give immediate desires.<br />\r\nIt is also important to note that we can apply Hayz thinking, introspection plan in important decisions, with a visionary company could well be assigned to projects.</p>\r\n','Цели и видение','<p>В связи с этим, компания будет по-прежнему решать на соответствующих навыков и поведения сохраняются Khvdtkhrybanh к ударам и не дают немедленные желания.<br />\r\nВажно также отметить, что мы можем применить Hayz мышление, план самоанализа в важных решений,</p>\r\n'),(12,'امکانات و زیر ساخت ها','<p dir=\"rtl\">یکی از عوامل رسیدن به هدف انتخاب زیر ساختار مناسب با شرایط موجود می باشد. این عوامل که یک مرحله مهم در پایه ریزی اساس یک شرکت فنی مهندسی برای ارائه خدمات به حساب می آید. خدماتی که با انتخاب یک زیر ساخت مناسب بهره وری صد در صد می رسد.<br />\r\nشایان ذکر است که امکانات مناسب و ایده ال توانست خواسته ها و آرزوهای این شرکت را تحقق بخشد و نتیجه ای قابل توجه در رشد و شکوفایی داشته باشد.</p>\r\n','2015-09-12 06:13:00','2015-10-13 11:01:11','Facilities and infrastructure','<p>One of the purpose of the infrastructure is appropriate to the circumstances. These factors are an important step in establishing a firm basis of Engineering for services considered. Services with an adequate infrastructure is one hundred percent efficiency.<br />\r\n&nbsp;</p>\r\n','Услуги и инфраструктура','<p>Один из целей инфраструктуры, соответствующей обстоятельствам. Эти факторы являются важным шагом в создании прочной основы инженерии услуг рассмотрены. Услуги по адекватной инфраструктуры является эффективность сто процентов.<br />\r\n&nbsp;</p>\r\n'),(13,'درباره ما','این مجموعه فعالیت خود را در سال ۱۳۸۵ با اجرای پروژه های پیمانکاری و تعمیراتی جهت ارائه خدمات برای شرکت های فعال در عرصه اتوماسیون ، کنترل و ماشین سازی آغاز نمود.\r\n\r\nمجموعه با گذشت زمانی نه چندان زیاد در زمینه سیستم های توزین وباسکول های صنعتی به تجربه های خاص و منحصر به فرد دست یافت.\r\n\r\nدر سال ۱۳۸۹ با توجه به دیدگاه های خاص مدیر تیم با ضعف های موجود در زمینه تولید تابلو برق و اجرای سیستم های اتوماسیون در منطقه ، این مجموعه پا به عرصه این فعالیت با نوآوری ها و بهره گیری از علم و تجربه شرکت های اروپایی گذاشت.\r\n\r\nدر سال ۱۳۹۱ و با افزایش فعالیت های اجرایی در کل کشور نیاز به ثبت نام تجاری پیش بینی گردید و برند VESTA  جهت نام گذاری این مجموعه انتخاب شد. شایان ذکر است که فعالیت های اجرایی و تولیدی وستا توان فرتاک با نام آرم تجاری وستا به مشتریان و همکاران ارائه می گردد.\r\n\r\nاین مجموعه در سال ۱۳۹۳  با وجود بازار کار نه چندان چشمگیر در کشور جهت ورود به عرصه های جدید و جهانی مکان خود را از تبریز به تهران انتقال داده و از طریق همکاری با شرکت های معتبر در پیشبرد اهداف خود تلاش نموده است.\r\n','2015-10-03 10:42:21','2015-10-28 08:41:52','About us','About Vesta power\r\nThe complex started its activity in 1385 with the implementation of projects and construction contract to provide services to companies operating in the field of automation, control and machine building began.\r\nWith the passage of time, not so much in the field of industrial weighing systems Vbaskvl experience was special and unique.\r\n\r\nIn 1391 and to increase enforcement activities in the country are required to register their trade was expected to name the collection was VESTA. It is worth noting that administrative and production activities with name brand logo Vesta Vesta Frtak be offered to customers and partners.','О нас','О Веста власти\r\nКомплекс начал свою деятельность в 1385 году с реализации проектов и строительного подряда на оказание услуг для компаний, работающих в области автоматизации, контроля и машиностроение стали.\r\n\r\nС течением времени, не столько в области промышленных систем взвешивания Vbaskvl опыт был особенным и уникальным.\r\n\r\nВ 1391 и увеличить правоприменительной деятельности в стране, должны зарегистрироваться в свою торговлю, как ожидается, назвать коллекцию было ВЕСТА. Стоит отметить, что административные и производственные виды еятельности с логотипом бренда имя Веста Веста Frtak быть предложены клиентами и партнерами.'),(14,'تیم ما','تیم ما متشکل از متخصصان الکترونیک برنامه نویسی و برق قدرت میباشد','2015-11-02 06:06:46','2015-11-02 06:06:46','our team','professionals are in our team','dfdfdf','sdfrtfrefd');
/*!40000 ALTER TABLE `vtf_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_password_reminders`
--

DROP TABLE IF EXISTS `vtf_password_reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_password_reminders` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_reminders_email_index` (`email`),
  KEY `password_reminders_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_password_reminders`
--

LOCK TABLES `vtf_password_reminders` WRITE;
/*!40000 ALTER TABLE `vtf_password_reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `vtf_password_reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_portfolio_categories`
--

DROP TABLE IF EXISTS `vtf_portfolio_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_portfolio_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_pcat_title` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_pcat_title` varchar(30) DEFAULT NULL,
  `ru_pcat_title` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_portfolio_categories`
--

LOCK TABLES `vtf_portfolio_categories` WRITE;
/*!40000 ALTER TABLE `vtf_portfolio_categories` DISABLE KEYS */;
INSERT INTO `vtf_portfolio_categories` VALUES (1,'برنامه نویسی','0000-00-00 00:00:00','2015-10-06 06:43:03','programming','кодирование'),(3,'اتوماسیون','0000-00-00 00:00:00','2015-10-06 06:43:48','automation','автоматизация'),(4,'توسعه و تبدیل','0000-00-00 00:00:00','2015-10-06 06:44:04','extending and transporting','Разработка и преобразование'),(5,'طراحی','0000-00-00 00:00:00','2015-10-06 06:44:34','Designing','проектирование'),(6,'ثبت وقایع','0000-00-00 00:00:00','2015-10-06 06:45:05','Submit events','Разместить события'),(8,'سامانه بیمارستان','2015-06-30 10:35:02','2015-10-06 06:45:25','HIS','Больница системы'),(9,'برون مرزی','2015-10-11 09:48:14','2015-10-11 09:48:14','International','за рубежом'),(10,'داخلی','2015-10-14 03:31:52','2015-10-14 03:31:52','Internal','внутренний');
/*!40000 ALTER TABLE `vtf_portfolio_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_portfolio_customers`
--

DROP TABLE IF EXISTS `vtf_portfolio_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_portfolio_customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_name` varchar(50) NOT NULL,
  `en_name` varchar(50) DEFAULT NULL,
  `ru_name` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_portfolio_customers`
--

LOCK TABLES `vtf_portfolio_customers` WRITE;
/*!40000 ALTER TABLE `vtf_portfolio_customers` DISABLE KEYS */;
INSERT INTO `vtf_portfolio_customers` VALUES (2,'شرکت گاز','gaz','взвешивание','2015-10-14 10:49:17','2015-10-14 10:58:28'),(3,'پارس ترک سیلو','PTSILO',NULL,'2015-10-15 02:47:20','2015-10-15 02:47:20'),(4,'زرماکارون',NULL,NULL,'2015-10-15 02:51:12','2015-10-15 02:51:12'),(5,'بهینه سازان صنعت تاسیسات','Behineh sazan sanat tasisat','Behineh sazan sanat tasisat','2015-11-12 06:17:26','2015-11-12 06:17:26');
/*!40000 ALTER TABLE `vtf_portfolio_customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_portfolio_portfolio_category`
--

DROP TABLE IF EXISTS `vtf_portfolio_portfolio_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_portfolio_portfolio_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(10) unsigned NOT NULL,
  `portfolio_category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `portfolio_portfolio_category_portfolio_id_index` (`portfolio_id`),
  KEY `portfolio_portfolio_category_portfolio_category_id_index` (`portfolio_category_id`),
  CONSTRAINT `portfolio_portfolio_category_portfolio_category_id_foreign` FOREIGN KEY (`portfolio_category_id`) REFERENCES `vtf_portfolio_categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `portfolio_portfolio_category_portfolio_id_foreign` FOREIGN KEY (`portfolio_id`) REFERENCES `vtf_portfolios` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_portfolio_portfolio_category`
--

LOCK TABLES `vtf_portfolio_portfolio_category` WRITE;
/*!40000 ALTER TABLE `vtf_portfolio_portfolio_category` DISABLE KEYS */;
INSERT INTO `vtf_portfolio_portfolio_category` VALUES (31,16,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(32,16,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,16,10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,17,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,17,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,17,10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,18,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,18,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,18,4,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,18,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(41,18,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(42,19,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(43,19,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(44,19,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(45,19,10,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vtf_portfolio_portfolio_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_portfolio_portfolio_customer`
--

DROP TABLE IF EXISTS `vtf_portfolio_portfolio_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_portfolio_portfolio_customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `portfolio_id` int(10) unsigned NOT NULL,
  `portfolio_customer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `portfolio_portfolio_customer_portfolio_id_index` (`portfolio_id`),
  KEY `portfolio_portfolio_customer_portfolio_customer_id_index` (`portfolio_customer_id`),
  CONSTRAINT `portfolio_portfolio_customer_portfolio_customer_id_foreign` FOREIGN KEY (`portfolio_customer_id`) REFERENCES `vtf_portfolio_customers` (`id`) ON DELETE CASCADE,
  CONSTRAINT `portfolio_portfolio_customer_portfolio_id_foreign` FOREIGN KEY (`portfolio_id`) REFERENCES `vtf_portfolios` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_portfolio_portfolio_customer`
--

LOCK TABLES `vtf_portfolio_portfolio_customer` WRITE;
/*!40000 ALTER TABLE `vtf_portfolio_portfolio_customer` DISABLE KEYS */;
INSERT INTO `vtf_portfolio_portfolio_customer` VALUES (10,16,5,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,16,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,17,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,18,3,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,19,3,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vtf_portfolio_portfolio_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_portfolios`
--

DROP TABLE IF EXISTS `vtf_portfolios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_portfolios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_title` varchar(100) NOT NULL,
  `fa_body` text NOT NULL,
  `img1` varchar(300) DEFAULT NULL,
  `img2` varchar(300) DEFAULT NULL,
  `img3` varchar(300) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_title` varchar(100) DEFAULT NULL,
  `en_body` text,
  `ru_title` varchar(100) DEFAULT NULL,
  `ru_body` text,
  `top` tinyint(1) NOT NULL,
  `thumbnail` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_portfolios`
--

LOCK TABLES `vtf_portfolios` WRITE;
/*!40000 ALTER TABLE `vtf_portfolios` DISABLE KEYS */;
INSERT INTO `vtf_portfolios` VALUES (16,'کنترل دمای گاز ورودی به رگلاتورهای گاز در ایستگاههای تقلیل فشار','<p dir=\"rtl\" style=\"text-align: justify;\">این پروژه با همکاری شرکت کنترل صنعت فراگیر انجام گشته است. در این پروژه با دریافت گاز ورودی و خروجی و مقایسه این دو ، مقدار مشعل گرمکن گاز تغییر می نمود . قسمت مونتاژ پانل های کنترل این پروژه به عهده تیم وستا توان و برنامه AVR به عهده تیم شرکت کنترل صنعت فراگیر بود.</p>\r\n','backend/assets/images/portfolio/2.jpg','backend/assets/images/portfolio/1.jpg','backend/assets/images/portfolio/2.jpg','2015-11-12 06:32:37','2015-11-12 07:54:10','Gas inlet temperature regulators control the gas pressure regulating stations','<p>This project has been conducted in collaboration with widespread industry control. In this project the gas inlet and outlet and comparing the two, the flare gas flow was changed. Vesta project team responsible for the assembly of control panels and power to the AVR program team was controlling the overall industry.</p>\r\n','Регуляторы температуры газа на входе управления регулирования давления газа станций','Этот проект был проведен в сотрудничестве с широко распространенной управления промышленности. В этом проекте на входе и выходе газа и сравнения двух, поток газа вспышка была изменена. Команда Веста проект отвечает за сборку панелей управления и власти в программе команды AVR контролировал общий промышленности.',1,'https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=%D8%A8%D8%AF%D9%88%D9%86+%D8%AA%D8%B5%D9%88%DB%8C%D8%B1&w=200&h=200&txttrack=0'),(17,'پروژه کارخانه ستاره کیان بیرجند','<p dir=\"rtl\" style=\"text-align: justify;\">با تکیه بر کادر فنی و تجربه کاری متخصصین تیم شرکت در صورت نیاز با اعزام سوپروایزر فنی به پروژه های بزرگ و ارائه مشاوره فنی جهت اجرای هر چه بهتر پروژه ها تلاش می نماید در همین راستا با اعزام سوپروایزر فنی برای پروژه کارخانه خوراک دام و طیور، آبزیان ستاره کیان بیرجند از سوی شرکت بین المللی پارس ترک سیلو جهت هرچه بهتر شدن پروژه ساخت و اجرای کارخانه، خدمات ارائه داده است. شایان ذکر می باشد شرکت ستاره کیان بزرگترین کارخانه خوراک دام، طیور و آبزیان در سطح کشور با توان تولید ۵۰تن در ساعت می باشد.</p>\r\n','backend/assets/images/portfolio/-_1_~1.JPG','backend/assets/images/portfolio/--2_1_~1.JPG','https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=1920&h=900&txttrack=0','2015-11-12 07:28:39','2015-11-12 07:53:52','Kian Star Factory project Birjand','<p>aquaculture star Kian Pars International Birjand leave the company to better silo plant project development and implementation, services offered. It is the co-star Kian largest animal feed mill, poultry and fish in the country with production capacity of 50 tons per hour.</p>\r\n','Киан Фабрика звезд проект Birjand','аквакультура звезда Киан Pars International Birjand покинуть компанию лучшему развитию проекта элеватора и реализации, предлагаемых услуг.\r\nЭто ролей Киан крупнейшим корма мельница, птицы и рыбы в стране с производственной мощностью 50 тонн в час.',0,'https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=%D8%A8%D8%AF%D9%88%D9%86+%D8%AA%D8%B5%D9%88%DB%8C%D8%B1&w=200&h=200&txttrack=0'),(18,'پروژه سیلو های ذخیره غلات هندوستان','<p dir=\"rtl\" style=\"text-align: justify;\">شرکت وستا توان در زمینه سیلو های گندم با شرکت بین المللی پارس ترک سیلو فعالیت مداوم و همسو داشته ودر پروژه های خارج از کشور کار های خاصی انجام داده، از جمله پروژه های بین المللی، سیلوهای ذخیره غلات هندوستان می باشد که تمامی مراحل طراحی ، تامین قطعات ، اجرا و تحویل از طریق این مجموعه انجام گرفته است. در رابطه با این پروژه می توان گفت یکی از خاص ترین پروژه های اجرایی در زمینه سیلوهای گندم بوده که با توجه به مشکلات اعزام سوپروایزر برقی به کشور هندوستان ، پروژه به صورتی طراحی و اجرا گردید که توسط ساده ترین تکنسین ها قابل اجرا باشد. از همین جهت تمامی نقشه ها و مدارک مهندسی با تکیه بر استانداردهای روز و بهره گیری از مهندسین مجرب انجام یافته و با توجه به اینکه چنین پروژه هایی نیاز به آموزش و بهره برداری درست از سیتم اتوماسیون وکنترل داشته اند به همین دلیل این پروژه به صورت Functional طراحی و اجرا گردید تا نیاز به آموزش از پروسه تحویل در کشور هندوستان حذف گردد.</p>\r\n','backend/assets/images/portfolio/3.jpg','https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=1920&h=900&txttrack=0','https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=1920&h=900&txttrack=0','2015-11-12 07:33:04','2015-11-12 07:53:36','Project grain silos India','<p>Vesta company in the field of wheat silo silo continuity and consistent with international companies to leave Pars have done certain work and projects abroad, including international projects, grain silos India is that the entire process of design, supply, implementation and delivery of this collection has been done. In conjunction with this project will be one of the most unique projects in the field of wheat silos due to problems with electrical supervisor dispatched to India, the project was designed in a way that is easiest technicians applicable.Functional was designed to eliminate the need to teach the process of delivery in India.</p>\r\n','Проект зерна силосы Индию','Компания Веста в области пшеницы непрерывности силоса силос и в соответствии с международными компаниями, чтобы оставить Парс сделали определенную работу и проекты за рубежом, в том числе международных проектов, элеваторы Индия, что весь процесс проектирования, поставку, внедрение и поставка этой коллекции было сделано.\r\nФункциональная был разработан, чтобы устранить необходимость учить процесс доставки в Индии.\r\n',0,'https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=%D8%A8%D8%AF%D9%88%D9%86+%D8%AA%D8%B5%D9%88%DB%8C%D8%B1&w=200&h=200&txttrack=0'),(19,'پروژه سیلوی ذخیره غلات آذران مهر پویای صبا','<p dir=\"rtl\" style=\"text-align: justify;\">سیلوی ۳۰ هزار تنی ذخیره غلات و کارخانه آرد سازی و نان صنعتی این پروژه در قالب ۳ فاز در حال احداث می باشد. در فاز اول که ساخت ۱۵هزار تن سیلوی ذخیره غلات بود. شرکت وستا توان فرتاک اقدام به طراحی وساخت تابلو های توزیع پانل کنترل اپراتوری اجرای سیستم اتوماسیون مبتنی بر PLC و نیز کابل کشی وسینی گذاری نموده است که در دی ماه سال ۱۳۹۱ آغاز گردیده و پایان فروردین ۱۳۹۲ راه اندازی گشته است. با توجه به فاز ۲ پروژه که ساخت ۱۵هزار تن سیلوی ذخیره غلات می باشد در زمان طراحی سیستم برقی و اتوماسیون فاز یک نیاز دیده شد تا سیستم یکپارچه ای طراحی گردد به همین دلیل سیستم اتوماسیون فراتر از خواسته ای خریدار و با امکاناتی بیشتر ارائه شد تا پس از ساخت فاز ۲ کنترل یکپارچه در اختیار اپراتور فراهم آورد. فاز ۳ این پروژه که ساخت کارخانه آرد می باشد به حول قوه الهی در سال ۱۳۹۴ استارت خواهد خورد. نیاز به توضیح است در فاز یک و دو این پروژه علاوه بر پانل کنترل اپراتوری امکان کنترل کل پروسه از طریق SCADA نیز برای اپراتور فراهم گردید که سیستم اعلام خرابی ، ثبت وقایع و کنترل اتوماتیک دمای غلات داخل سیلوها حتی در زمان های حاضر نبودن اپراتور جهت کنترل سیستم فراهم گشته است.</p>\r\n','backend/assets/images/portfolio/4.jpg','backend/assets/images/portfolio/5.jpg','https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=1920&h=900&txttrack=0','2015-11-12 07:44:50','2015-11-12 07:51:27','Grain storage silo projects Azaran dynamic seals Saba (Asia)','<p>30 thousand ton storage silo grain and flour and bread industries The project is being built in 3 phases. The first phase of the construction of 15 thousand tonnes of grain storage silo. Vesta power distribution boards Frtak company to design and implement automation systems based on PLC and the operator control panel cabling Blister investment has started in January 1391 and the end of the Persian date Farvardin 1392 has been launched. According to the Phase 2 project to build 15 thousand tons of silo storage of grains at the time of system design, electrical and automation phase, a need was seen to integrated systems be designed so the automation system beyond the demands of the buyer and the facilities further to After the construction of Phase 2 of the integrated controller provides the operator. Phase 3 of the project is to build a flour mill to start in 1394 will be about the power of God.control system is provided.</p>\r\n','Хранения зерна силоса проекты Azaran динамические уплотнения Саба (Азия)','30 тысяч тонн силоса для хранения зерна и муки и хлеба отрасли\r\nПроект строится в 3 этапа. Первый этап строительства 15 тысяч тонн хранения зерна силоса. Распределения питания Веста платы компании Frtak для проектирования и реализации систем автоматизации на основе ПЛК и управления панель оператора кабельной волдыря инвестиции началось в январе 1391 и в конце персидского даты Фарвардин тысяче триста девяносто два был запущен.\r\nСогласно проекту Фаза 2 для создания 15 тысяч тонн силоса хранения зерна на момент проектирования системы, электрической и автоматизации фазе, необходимость было видно, интегрированных систем быть спроектирована таким образом система автоматизации за требований покупателя и объектов далее После строительства Фазы 2 интегрированного контроллера предоставляет оператору.\r\nЭтап 3 проекта заключается в создании мельницу, чтобы начать в 1394 будет о силе Бога. Нужно объяснить, в фазе первой и второй этого проекта в дополнение к панели управления оператора позволяет оператору контролировать весь процесс до неисправности системы SCADA, журналов и автоматического регулирования температуры зерна внутри оператора бункера нет, даже во времена Система управления обеспечивается.',1,'https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=%D8%A8%D8%AF%D9%88%D9%86+%D8%AA%D8%B5%D9%88%DB%8C%D8%B1&w=200&h=200&txttrack=0');
/*!40000 ALTER TABLE `vtf_portfolios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_product_categories`
--

DROP TABLE IF EXISTS `vtf_product_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_product_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `fa_name` varchar(30) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_name` varchar(30) DEFAULT NULL,
  `ru_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_gategories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `product_gategories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `vtf_product_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_product_categories`
--

LOCK TABLES `vtf_product_categories` WRITE;
/*!40000 ALTER TABLE `vtf_product_categories` DISABLE KEYS */;
INSERT INTO `vtf_product_categories` VALUES (1,NULL,'سیستم های توزین',1,'2015-11-12 09:48:04','2015-11-12 09:48:04','Weighing',NULL);
/*!40000 ALTER TABLE `vtf_product_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_product_product_category`
--

DROP TABLE IF EXISTS `vtf_product_product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_product_product_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `product_category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `product_product_category_product_id_index` (`product_id`),
  KEY `product_product_category_product_category_id_index` (`product_category_id`),
  CONSTRAINT `product_product_category_product_category_id_foreign` FOREIGN KEY (`product_category_id`) REFERENCES `vtf_product_categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `product_product_category_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `vtf_products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_product_product_category`
--

LOCK TABLES `vtf_product_product_category` WRITE;
/*!40000 ALTER TABLE `vtf_product_product_category` DISABLE KEYS */;
INSERT INTO `vtf_product_product_category` VALUES (1,7,1,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vtf_product_product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_products`
--

DROP TABLE IF EXISTS `vtf_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_title` varchar(30) NOT NULL,
  `fa_description` text NOT NULL,
  `availability` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_title` varchar(30) DEFAULT NULL,
  `en_description` text,
  `ru_title` varchar(30) DEFAULT NULL,
  `ru_description` text,
  `img` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_products`
--

LOCK TABLES `vtf_products` WRITE;
/*!40000 ALTER TABLE `vtf_products` DISABLE KEYS */;
INSERT INTO `vtf_products` VALUES (7,'ygty','<p>uuy</p>\r\n',1,1,'2015-11-14 08:18:44','2015-11-14 08:18:44','','','ygty','<p>uuy</p>\r\n','');
/*!40000 ALTER TABLE `vtf_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_service_categories`
--

DROP TABLE IF EXISTS `vtf_service_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_service_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_title` varchar(25) NOT NULL,
  `en_title` varchar(25) DEFAULT NULL,
  `fa_description` text NOT NULL,
  `img` varchar(300) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_description` text,
  `ru_title` varchar(25) DEFAULT NULL,
  `ru_description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_service_categories`
--

LOCK TABLES `vtf_service_categories` WRITE;
/*!40000 ALTER TABLE `vtf_service_categories` DISABLE KEYS */;
INSERT INTO `vtf_service_categories` VALUES (6,'سیلو','silo','سیلو انباری جهت نگهداری از مواد خوراکی مورد استفاده در تغذیه انسان و دام است. سیلو در دامپروری معمولاً به جایی گفته می‌شود که علوفه با رطوبت بالا را در آن نگهداری می‌کنند به طوری که می‌توان تا سال‌ها از آن علوفه استفاده نمود بدون این که ارزش غذایی علوفه تغییر زیادی بدهد. انواع سیلو به دو نوع زیرزمینی و روزمینی تقسیم می‌شوند که در ایران معمولاً سیلو همیشه با ذرت خرد شده تهیه می‌گردد اما در کشورهایی که تولید یونجه در آن‌ها زیاد است از سیلوی یونجه نیز تهیه می‌گردد.','backend/assets/images/service/ser4.png','2015-10-04 10:28:45','2015-10-13 09:02:53','A silo (from the Greek σιρός – siros, \"pit for holding grain\") is a structure for storing bulk materials. Silos are used in agriculture to store grain (see grain elevators) or fermented feed known as silage. Silos are more commonly used for bulk storage of grain, coal, cement, carbon black, woodchips, food products and sawdust.','силосная яма или башня','Силос (от греческого σιρός - Сирос, \"яма для проведения зерно\") представляет собой структуру для хранения сыпучих материалов. Бункеры используются в сельском хозяйстве для хранения зерна (см элеваторы) или ферментированной канал, известный как силос. Бункеры чаще используются для бестарного хранения зерна, угля, цемента, сажи, щепа, продукты питания и опилок.'),(9,'انبار مکانیزه','Warehouse automation','مکانیزه کردن انبار یک کارخانه برای آسان تر و سریع ترشدن سطح دسترسی انبار دار به اطلاعات انبار میباشد .\r\nاین برنامه این امکان را برای انبار دار فراهم آورده تا انبار دار با زدن چند کلید از آخرین موجودی انبار و خروجی و ورودی انبار و همچنین موقعیت یک مرسوله ( از نظر تاریخ ورودی و تاریخ خروجی آن ) با خبر شود .','backend/assets/images/service/ser5.png','2015-10-04 10:48:27','2015-10-13 09:07:22','A warehouse is a commercial building for storage of goods. Warehouses are used by manufacturers, importers, exporters, wholesalers, transport businesses, customs, etc.','автоматизация склада','Склад — помещение (также их комплекс), предназначенное для хранения материальных ценностей и оказания складских услуг. В логистике склад выполняет функцию аккумулирования резервов материальных ресурсов,'),(10,'گلخانه','Greenhouse','تولید محصولات گلخانه ای در مناطق روستایی کشورهای صنعتی و با آب و هوای ملایم و درجه حرارت معتدل حتی مناطق استوایی روز به روز در حال افزایش است. پرورش میوه های مرغوب در زمین های حتی کوچک درآمد های خوبی برای ساکنین فراهم آورده است. گلخانه ها امکان کشت در برخی مناطق خشک و گرمسیری و غیر قابل کشت را به وجود آورده است.','backend/assets/images/service/green_house_logo.png','2015-10-04 16:09:08','2015-10-13 09:09:21','Greenhouse production in rural areas of industrialized countries, with a mild climate and mild temperatures even in the tropics is increasing day by day. Growing fruit quality is good land even a small income for residents is provided. Greenhouse cultivation in some arid and tropical and non-arable created.','Парниковый','Производство парниковых в сельских районах промышленно развитых стран, с мягким климатом и умеренными температурами даже в тропиках растет день ото дня. Рост качества плодов хорошая земля даже небольшой доход для резидентов предоставляется. Тепличное в некоторых засушливых и тропических и не-пашни создан.\r\n'),(11,'سردخانه','Refrigerator','یخچال دستگاهی است که از یک وسیله سردکننده به همراه یک عایق حرارتی و سازوکاری برای انتقال گرما از داخل این محفظه به بیرون تشکیل می‌شود. از یخچال برای خنک نگهداشتن مواد غذایی، داروها و دیگر چیزهای فاسدشدنی استفاده می‌شود.','backend/assets/images/service/1111.png','2015-10-04 16:11:16','2015-10-13 10:30:11','Refrigerator (commonly fridge) is a common household appliance, which consists of a thermally insulated chamber and the heat pump (mechanical, electronic, or chemical), which transfers heat from the inside of the refrigerator to its external environment, so that inside of the refrigerator is cooled to a temperature below ambient temperature in the room.','холодильник','Холодильник (в просторечии холодильник) является общим бытовой прибор, который состоит из теплоизолированной камеры и теплового насоса (механической, электронной или химической), который передает тепло от внутренней части холодильника, чтобы ее внешней среды таким образом, что внутри холодильника является охлаждают до температуры ниже температуры окружающего воздуха в комнате.'),(12,'مرغداری','Aviculture','صنعت مرغداری بعد از نفت مهمترین صنعت در کشور به حساب می آید و با توجه به اهمیت این صنعت در سبد غذایی مردم تجربیات در ضمینه این صنعت و یا فته های علمی جدید می تواند ره آوردی برای پیشرفت این صنعت و حرکت رو به جلو در این صنعت شود تا شاید مقداری بتوانیم سرعت نزدیک شدن به کشورهای پیشرفته را بیشتر کنیم','backend/assets/images/service_category/ser6.png','2015-10-13 09:53:18','2015-10-13 09:56:10','We should not forget that under such a situation, the carcasses of the birds, marketability and low demand. A practical recommendations in this regard is that the average moisture content of the substrate must always be kept in the range of 25 to 35 percent. Appropriate and free of disease, it can be used again during the next training contract','птицеводство','Мы не должны забывать, что при таком положении, трупы птиц, товарности и низкого спроса. Практической рекомендации в этом отношении является то, что среднее содержание влаги в подложке должна быть всегда в диапазоне от 25 до 35 процентов. Соответствующее и свободным от болезни, он может быть использован еще раз при следующем контракта учебного\r\n'),(13,'gygh','','hgghgh','https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=125&h=125&txttrack=0','2015-10-19 03:48:24','2015-10-19 03:48:24','','','');
/*!40000 ALTER TABLE `vtf_service_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_service_service_category`
--

DROP TABLE IF EXISTS `vtf_service_service_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_service_service_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `service_id` int(10) unsigned DEFAULT NULL,
  `service_category_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `service_service_category_service_id_index` (`service_id`),
  KEY `service_service_category_service_category_id_index` (`service_category_id`),
  CONSTRAINT `service_service_category_service_category_id_foreign` FOREIGN KEY (`service_category_id`) REFERENCES `vtf_service_categories` (`id`) ON DELETE CASCADE,
  CONSTRAINT `service_service_category_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `vtf_services` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_service_service_category`
--

LOCK TABLES `vtf_service_service_category` WRITE;
/*!40000 ALTER TABLE `vtf_service_service_category` DISABLE KEYS */;
INSERT INTO `vtf_service_service_category` VALUES (2,2,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,2,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,2,10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,2,11,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,2,12,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,3,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,3,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(9,3,10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(10,3,11,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,3,12,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,1,6,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,1,9,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,1,10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,1,11,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,1,12,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,5,6,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vtf_service_service_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_services`
--

DROP TABLE IF EXISTS `vtf_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_services` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_title` varchar(25) NOT NULL,
  `en_title` varchar(25) DEFAULT NULL,
  `fa_body` text NOT NULL,
  `en_body` text,
  `img` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ru_title` varchar(25) DEFAULT NULL,
  `ru_body` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_services`
--

LOCK TABLES `vtf_services` WRITE;
/*!40000 ALTER TABLE `vtf_services` DISABLE KEYS */;
INSERT INTO `vtf_services` VALUES (1,'اتوماسیون','automation','امروزه‌ تقريبايك‌ قرن‌ از آن‌ كه‌ گــرگــور مندل‌ رموز دورگــه‌سازي‌ (پرورش‌ متقاطع‌) را تشريح‌كرد، مي‌گــذرد، و ژنتيسين‌هاي‌ عصر حاضر هنوز از يافته‌هاي‌ او براي‌ اصلاحات‌ ژنتيكي‌ درگــياهان‌ و حيوانات‌ استفاده‌ مي‌نمايند. اگــرچه‌ فن‌آوري‌هاي‌ جديد آنها را در پيش‌بيني‌ بهترنتيجه‌ آميزش‌ گــونه‌ها جهت‌ تكامل‌ صفات‌ و پرورش‌ كارآمدتر و سريع‌تر ياري‌ مي‌دهد\r\n','About a century now that Gregor Mendel password Dvrg·hsazy (cross-breeding) to Tshryhkrd, passes, and Zhntysynhay present his findings to genetically modified plants and animals still exist. Although new technologies to predict the evolution of traits and breeding more efficient and faster Bhtrntyjh mixing species helps.\r\n','backend/assets/images/service/ser6.png','2015-10-13 09:50:35','2015-10-14 08:06:43','',''),(2,'کنترل دما','temprture controlling','سیلو\r\n','temprture temprture temprture temprture temprture ','backend/assets/images/service/1.jpg','2015-10-13 09:58:36','2015-10-14 09:16:31','',''),(3,'ثبت فرایند','datalogger','بیبلیبل','datalogger datalogger datalogger datalogger datalogger ','backend/assets/images/service/ghjh.jpg','2015-10-14 08:02:10','2015-10-14 09:16:03','',''),(5,'gffh','ghgh','ghghhg','ghgh','https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=125&h=125&txttrack=0','2015-10-19 03:40:06','2015-10-19 03:40:06','','');
/*!40000 ALTER TABLE `vtf_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_sessions`
--

DROP TABLE IF EXISTS `vtf_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_sessions` (
  `id` varchar(255) NOT NULL,
  `payload` text NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_sessions`
--

LOCK TABLES `vtf_sessions` WRITE;
/*!40000 ALTER TABLE `vtf_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `vtf_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_settings`
--

DROP TABLE IF EXISTS `vtf_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(50) DEFAULT NULL,
  `value` text,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_settings`
--

LOCK TABLES `vtf_settings` WRITE;
/*!40000 ALTER TABLE `vtf_settings` DISABLE KEYS */;
INSERT INTO `vtf_settings` VALUES (10,'fa_description','jkojk','0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,'fa_keywords','اتوماسیون','0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,'en_keywords','ghjghj','0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,'en_description','yghjh','0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,'ru_description','sdf','0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,'ru_keywords','dfdf','0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,'fa_slug','کسب و کار شما علاقه ما است','0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,'en_slug','Your Businees is Our Passion','0000-00-00 00:00:00','0000-00-00 00:00:00'),(22,'ru_slug','Ваш бизнес это наша страсть','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `vtf_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_sliders`
--

DROP TABLE IF EXISTS `vtf_sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `img` varchar(400) NOT NULL,
  `fa_slug1` varchar(200) NOT NULL,
  `fa_slug2` varchar(200) NOT NULL,
  `en_slug1` varchar(200) DEFAULT NULL,
  `en_slug2` varchar(200) DEFAULT NULL,
  `ru_slug1` varchar(200) DEFAULT NULL,
  `ru_slug2` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `animate` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_sliders`
--

LOCK TABLES `vtf_sliders` WRITE;
/*!40000 ALTER TABLE `vtf_sliders` DISABLE KEYS */;
INSERT INTO `vtf_sliders` VALUES (2,'backend/assets/images/slider/silo3.jpg','همراه شما در نوآوری','نو آوری مزیتیست برای تعریف اینکه پیشرو باشیم نه دنباله رو','your innovation partner','innovation distinguishes between a leader and a follower','Ваш партнер инновации','Инновация отличает лидера между и последователь','2015-10-07 01:18:35','2015-10-07 01:18:35','papercut'),(6,'backend/assets/images/slider/csfco_Corn_field.jpg','اسلاید 2','اسلاید 2','','','','','2015-10-12 11:52:22','2015-10-12 11:52:32','3dcurtain-horizontal'),(7,'backend/assets/images/slider/silo2.jpg','اسلاید 3','اسلاید 3','','','','','2015-10-12 11:54:07','2015-10-12 11:54:07','papercut'),(8,'backend/assets/images/slider/csfco-grainsilo.jpg','اسلاید 4','اسلاید 4','','','','','2015-10-12 11:56:53','2015-10-12 11:56:53','scaledownfromleft');
/*!40000 ALTER TABLE `vtf_sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_team_categories`
--

DROP TABLE IF EXISTS `vtf_team_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_team_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_cat_title` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_cat_title` varchar(30) DEFAULT NULL,
  `ru_cat_title` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_team_categories`
--

LOCK TABLES `vtf_team_categories` WRITE;
/*!40000 ALTER TABLE `vtf_team_categories` DISABLE KEYS */;
INSERT INTO `vtf_team_categories` VALUES (1,'مدیر عامل','2015-08-02 12:23:20','2015-11-12 05:19:53','Direct Manager','непосредственным руководителем'),(2,'مدیریت فناوری اطلاعت','2015-08-02 12:24:12','2015-10-06 06:41:07','IT  Manager','Управление информационных техн'),(3,'مدیریت','2015-09-12 07:39:24','2015-10-06 06:41:24','Manager','управление'),(4,'مشاور','2015-09-12 07:40:20','2015-10-06 06:41:57','Advisor','консультант'),(5,'مسئول کارگاه','2015-09-14 02:57:47','2015-10-06 06:42:27','Responsible for workshop','Ответственный за мастерской'),(6,'مدیر دپارتمان نساجی','2015-11-02 06:32:40','2015-11-12 05:17:00','textile department manager','dsfdsfd'),(7,'مدیر مالی','2015-11-12 05:18:41','2015-11-12 05:18:41','Financial Manager','Финансовый менеджер');
/*!40000 ALTER TABLE `vtf_team_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_team_profession_categories`
--

DROP TABLE IF EXISTS `vtf_team_profession_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_team_profession_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_prof_title` varchar(30) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_prof_title` varchar(30) DEFAULT NULL,
  `ru_prof_title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_team_profession_categories`
--

LOCK TABLES `vtf_team_profession_categories` WRITE;
/*!40000 ALTER TABLE `vtf_team_profession_categories` DISABLE KEYS */;
INSERT INTO `vtf_team_profession_categories` VALUES (1,'کارشناس نرم افزار','2015-08-02 12:13:48','2015-10-06 06:38:29','Software expert','Эксперт Программное обеспечени'),(2,'کارشناس برق','2015-08-02 12:22:35','2015-10-06 06:38:55','Electrical expert','Электрические эксперт'),(3,'کارشناس الکترونیک','2015-09-12 07:42:41','2015-10-06 06:39:23','Electronic expert','эксперт Электроника'),(4,'کارشناس کنترل','2015-11-02 06:35:49','2015-11-02 06:35:49','control expert','rfgrewer'),(5,'کارشناس ابزار دقیق','2015-11-12 05:26:30','2015-11-12 05:26:30','Expert Instrumentation','Эксперт приборы');
/*!40000 ALTER TABLE `vtf_team_profession_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_teams`
--

DROP TABLE IF EXISTS `vtf_teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fa_name` varchar(30) NOT NULL,
  `fa_family` varchar(30) NOT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `linkedin` varchar(100) DEFAULT NULL,
  `google` varchar(100) DEFAULT NULL,
  `image` varchar(300) DEFAULT NULL,
  `category` int(11) NOT NULL,
  `profession` int(11) NOT NULL,
  `fa_description` text,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `en_name` varchar(255) DEFAULT NULL,
  `en_family` varchar(255) DEFAULT NULL,
  `en_description` varchar(255) DEFAULT NULL,
  `ru_name` varchar(30) DEFAULT NULL,
  `ru_family` varchar(30) DEFAULT NULL,
  `ru_description` text,
  PRIMARY KEY (`id`),
  KEY `profession` (`profession`),
  KEY `category` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_teams`
--

LOCK TABLES `vtf_teams` WRITE;
/*!40000 ALTER TABLE `vtf_teams` DISABLE KEYS */;
INSERT INTO `vtf_teams` VALUES (1,'بهنام','نظمی','hjgh','jghj','ghjghjg','backend/assets/images/team/1446730838photo_2015-11-02_13-18-37.jpg',1,2,'<p>بهنام نظمی مدیر عامل شرکت</p>\r\n','2015-10-19 04:18:45','2015-11-12 05:43:23','Behnam','Nazmi','<p>Apart from my studies, I am involved in political studies and international relations at university. I am very interested in international relations and have been a part of the Harvard International Review, a magazine, since my first year.</p>\r\n\r\n<p>I ','Behnam','Nazmi','Помимо учебы, я участвую в политических исследований и международных отношений в университете. Я очень заинтересован в международных отношениях и были частью Гарвардского International Review, журнал, так как мой первый год. Я провел большую часть своей жизни в Соединенных Штатах, но посетил Иран и планируют вернуться, когда я получаю шанс.'),(2,'سجاد','میرعبدالی','trfhgtgh','fghfghfg','hghfgh','backend/assets/images/team/photo_2015-11-02_13-17-51.jpg',6,4,'<p dir=\"rtl\">به غیر از مطالعات من، من موجود در مطالعات سیاسی و روابط بین الملل در دانشگاه هستم. من بسیار علاقه مند در روابط بین المللی هستم و بخشی از بررسی بین المللی هاروارد، یک مجله است، از سال سال اول من.</p>\r\n\r\n<p dir=\"rtl\">من از زندگی من در ایالات متحده کرده اند، اما ایران سفر کرده اند و برنامه ریزی برای بازگشت به زمانی که من شانس.</p>\r\n','2015-11-02 06:36:31','2015-11-12 05:42:17','Sajad','Mirabdaly','<p>Apart from my studies, I am involved in political studies and international relations at university. I am very interested in international relations and have been a part of the Harvard International Review, a magazine, since my first year.</p>\r\n\r\n<p>I ','Sajad ','Mirabdaly','Помимо учебы, я участвую в политических исследований и международных отношений в университете. Я очень заинтересован в международных отношениях и были частью Гарвардского International Review, журнал, так как мой первый год. Я провел большую часть своей жизни в Соединенных Штатах, но посетил Иран и планируют вернуться, когда я получаю шанс.'),(3,'توحید','صباغی','tfhftg','hfghfgh','gfhfgh','backend/assets/images/team/1446730848photo_2015-11-02_13-17-29.jpg',3,2,'<p dir=\"rtl\">به غیر از مطالعات من، من موجود در مطالعات سیاسی و روابط بین الملل در دانشگاه هستم. من بسیار علاقه مند در روابط بین المللی هستم و بخشی از بررسی بین المللی هاروارد، یک مجله است، از سال سال اول من.</p>\r\n\r\n<p dir=\"rtl\">من از زندگی من در ایالات متحده کرده اند، اما ایران سفر کرده اند و برنامه ریزی برای بازگشت به زمانی که من شانس.</p>\r\n','2015-11-02 06:37:37','2015-11-12 06:07:03','Towhid','Sabbaghi','<p>Apart from my studies, I am involved in political studies and international relations at university. I am very interested in international relations and have been a part of the Harvard International Review, a magazine, since my first year.</p>\r\n\r\n<p>I<','','','Помимо учебы, я участвую в политических исследований и международных отношений в университете. Я очень заинтересован в международных отношениях и были частью Гарвардского International Review, журнал, так как мой первый год. Я провел большую часть своей жизни в Соединенных Штатах, но посетил Иран и планируют вернуться, когда я получаю шанс.'),(4,'امید','زرین مهد','بلاب','ابلالبا','بلابلا','backend/assets/images/team/photo_2015-11-05_17-05-15.jpg',2,1,'بلاب','2015-11-05 10:10:26','2015-11-05 10:10:26','omid','zarinmahd','','','','');
/*!40000 ALTER TABLE `vtf_teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vtf_users`
--

DROP TABLE IF EXISTS `vtf_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vtf_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `family` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `password` varchar(100) NOT NULL,
  `remember_token` varchar(500) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vtf_users`
--

LOCK TABLES `vtf_users` WRITE;
/*!40000 ALTER TABLE `vtf_users` DISABLE KEYS */;
INSERT INTO `vtf_users` VALUES (4,'omid','zarin','zarinpy@gmail.com',1,'$2y$10$/K.ceUQijuT3VRJXDsusAu02QWGDEz0vfro/ZMlfFgJCimJjAEn6C','2OyJKTxZipZ98YyWDPKATVc6zB4GoIWq7Ty92va6g5Y4nuT6VX79DUfWWP9Q','2015-10-01 05:45:12','2015-11-24 04:57:31'),(5,'senator1','zarin','zarin@yahoo.com',1,'$2y$10$98DRZcc8tgj.ZV5HQLEPROzAn/Rqdb9R4g/1bmZNVbKZha3DjZMuG','Ytwso5JlsAl5m4xY3A1P3lmnFm2cGBtGQ0Chn9Q7JXrsZiAJ1Awg2eunn1Ss','2015-11-10 03:32:51','2015-11-24 05:15:14');
/*!40000 ALTER TABLE `vtf_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-27 13:59:36
