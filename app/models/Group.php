<?php

class Group extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'fa_name' => 'required',
		 //'en_name' => 'required'
		 //'ru_name' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['ru_name','fa_name','en_name'];
	public function user(){
		return $this->belongsToMany('User');
	}
    public function datasheet(){
        return $this->belongsToMany('Datasheet');
    }
}
