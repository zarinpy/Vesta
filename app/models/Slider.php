<?php

class Slider extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'fa_slug1' => 'required|max:200',
		'fa_slug2' => 'required|max:200',
	];

	// Don't forget to fill this array
	protected $fillable = ['fa_slug2','fa_slug1','en_slug2','en_slug1','ru_slug2','ru_slug1'];

}