<?php

class PortfolioCategory extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'fa_pcat_title' => 'required|max:35',
		 //'en_pcat_title' => 'required|max:35',
		 //'ru_pcat_title' => 'required|max:35',
	];

	// Don't forget to fill this array
	protected $fillable = ['ru_pcat_title','fa_pcat_title','en_pcat_title'];

	public function portfolio(){
		return $this->belongsToMany('Portfolio');
	}
}