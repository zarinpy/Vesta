<?php

class PortfolioCustomer extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		//'fa_title' => 'required|max:50'
	];

	// Don't forget to fill this array
	protected $fillable = ['fa_name','ru_name','en_name'];

	public function portfolio(){
		return $this->belongsToMany('Portfolio');
	}
}