<?php

class Service extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'fa_title' => 'required',
		//'en_title' => 'required',
		//'ru_title' => 'required',
		'fa_body' => 'required',
		//'en_body' => 'required',
		//'ru_body' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = ['ru_body','ru_title','fa_title','en_title','fa_body','en_body'];
	public function category(){
		return $this->belongsToMany('ServiceCategory');
	}
}