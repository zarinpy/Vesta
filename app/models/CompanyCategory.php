<?php

class CompanyCategory extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'fa_name' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['fa_name','en_name','ru_name'];

	public function company(){
		return $this->belongsToMany('Company');
	}
}