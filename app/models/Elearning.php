<?php

class Elearning extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'fa_title' => 'required|max:50',
		 //'ru_title' => 'required|max:50',
		 //'en_title' => 'required|max:50',
		 'fa_body' => 'required',
		 //'en_body' => 'required',
		 //'ru_body' => 'required',

	];

	// Don't forget to fill this array
	protected $fillable = ['ru_body','ru_title','fa_title','en_title','fa_body','en_body'];

	public function category(){
		return $this->belongsToMany('ElearningCategory')->withPivot('elearning_category_id');
	}

}