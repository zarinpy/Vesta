<?php

class Team extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'fa_name' => 'required|max:30',
		//'en_name' => 'required|max:30',
		//'ru_name' => 'required|max:30',
		'fa_family' => 'required|max:30',
		//'en_family' => 'required|max:30',
		//'ru_family' => 'required|max:30',
		'profession' => 'required',
		'category' => 'required',
		//'ru_description' => 'required',
		//'en_description' => 'required',
		'fa_description' => 'required',
		'image'=>'image'
	];

	// Don't forget to fill this array
	protected $fillable = ['ru_name','ru_family','fa_name','en_name','fa_family','en_family','profession','category','google','facebook','linkedin','fa_description','en_description'];
	public function categories(){
		return $this->belongsTo('TeamCategory');
	}
	public function professioncategory(){
		return  $this->belongsTo('TeamProfessionCategory');
	}
}