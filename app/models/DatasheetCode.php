<?php

class DatasheetCode extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'short' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = ['short'];

	public function datasheet(){
		return $this->belongsToMany('Datasheet');
	}
}