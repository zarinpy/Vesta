<?php

class Menu extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'name' => 'required',
		 'en_name' => 'required',
		'url'	=>	'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['name','url','en_name'];

}