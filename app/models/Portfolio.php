<?php

class Portfolio extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'fa_title' =>  'required|max:100',

		//'en_title' =>  'required|max:25',
		//'ru_title' =>  'required|max:25',
		'fa_body' => 'required',
		//'en_body' => 'required',
		//'ru_body' => 'required',
	];
	// Don't forget to fill this array
	protected $fillable = ['top','ru_title','ru_body','fa_title','en_title','img1','img2','img3','en_body','fa_body','thumbnail'];
	public function category(){
		return $this->belongsToMany('PortfolioCategory');
	}
	public function customer(){
		return $this->belongsToMany('PortfolioCustomer');
	}
}