<?php

class Company extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'fa_name' => 'required|max:30',
		//'en_name' => 'required|max:30',
		//'ru_name' => 'required|max:30',
		//'logo' => 'required|max:100|image',
		'website' => 'required|max:100'
	];

	// Don't forget to fill this array
	protected $fillable = [
		'ru_name',
		'fa_name',
		'website',
		'en_name',
	];
	public function category(){
		return $this->belongsToMany('CompanyCategory');
	}

}