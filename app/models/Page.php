<?php

class Page extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'fa_title' => 'required|max:50',
		 //'en_title' => 'required|max:50',
		 //'ru_title' => 'required|max:50',
		 'fa_content' => 'required',
		 //'ru_content' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = ['ru_content','ru_title','fa_title','fa_content','en_title','en_content'];

}