<?php

class TeamProfessionCategory extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'fa_prof_title' => 'required|max:30',
		 //'en_prof_title' => 'required|max:30',
		 //'ru_prof_title' => 'required|max:30',
	];
	// Don't forget to fill this array
	protected $fillable = ['ru_prof_title','fa_prof_title','en_prof_title'];
	function teams(){
		return $this->belongsTo('teams');
	}
}