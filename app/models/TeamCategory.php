<?php

class TeamCategory extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'fa_cat_title' => 'required|max:30',
		//'en_cat_title' => 'required|max:30',
		//'ru_cat_title' => 'required|max:30',
	];

	// Don't forget to fill this array
	protected $fillable = ['ru_cat_title','fa_cat_title','en_cat_title'];

	function teams(){
		return $this->belongsTo('teams');
	}

}