<?php

class ServiceCategory extends \Eloquent {
	// Add your validation rules here
	public static $rules = [
		'fa_title' => 'required',
		//'en_title' => 'required',
		//'ru_title' => 'required',
		//'en_description' => 'required',
		//'ru_description' => 'required',
		'fa_description' => 'required',
	];
	// Don't forget to fill this array
	protected $fillable = ['ru_description','ru_title','fa_title','en_title','en_description','fa_description'];
	public function service(){
		return $this->belongsToMany('Service');
	}
}