<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	public static $rules = [
		'name' => 'required|max:30',
		'password' => 'required|max:30',
		'family' => 'required|max:30',
		'email' => 'required|max:100|email',
	];
	protected $fillable = ['name','family','email','active','password'];
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $guarded = array('id');
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	public function group(){
		return $this->belongsToMany('Group');
	}
}
