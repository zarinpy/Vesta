<?php

class DatasheetCompany extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'fa_name' => 'required',
		//'en_name' => 'required',
		'short' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = ['fa_name','en_name','short'];

}