<?php

class Product extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'fa_title' => 'required',
		 //'en_title' => 'required',
		 //'ru_title' => 'required',
		'fa_description'=>'required',
		//'en_description'=>'required',
		//'ru_description'=>'required',

	];

	// Don't forget to fill this array
	protected $fillable = ['ru_description','ru_title','fa_title','fa_description','en_title','en_description'];
	public function category(){
		return $this->belongsToMany('ProductCategory');
	}
}