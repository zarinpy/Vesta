<?php

class ProductCategory extends \Eloquent {
	// Add your validation rules here
	public static $rules = [
		 'fa_name' => 'required',
		 //'en_name' => 'required',
		 //'ru_name' => 'required',
	];
	// Don't forget to fill this array
	protected $fillable = ['ru_name','fa_name','active','parent_id','en_name'];
	public function product(){
		return $this->belongsToMany('Product');
	}
}