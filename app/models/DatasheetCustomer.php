<?php

class DatasheetCustomer extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		//'en_name' => 'required',
		'fa_name' => 'required',

	];

	// Don't forget to fill this array
	protected $fillable = ['fa_name','en_name'];

}