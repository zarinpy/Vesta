<?php

class Datasheet extends \Eloquent {
	// Add your validation rules here
	public static $rules = [
		 'fa_name' => 'required',
		 //'en_name' => 'required',
		 'file' => 'required',
	];
	// Don't forget to fill this array
	protected $fillable = ['fa_name','en_name','file','fa_description','en_description'];

	public function code(){
		return $this->belongsToMany('DatasheetCode');
	}
	public function group(){
		return $this->belongsToMany('Group');
	}
}