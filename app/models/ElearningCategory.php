<?php

class ElearningCategory extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		 'fa_ecat_title' => 'required',
		 //'en_ecat_title' => 'required',
		 //'ru_ecat_title' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = ['ru_ecat_title','fa_ecat_title','en_ecat_title'];
	public function elearning(){
		return $this->belongsToMany('Elearning');
	}
}