<?php
return array(
    'error4'=>'enter your name',
    'email'=>'Email',
    'contact_us'=>'Contact us',
    'name'=>'Name',
    'body'=>'Tell Us Everything:',
    'message'=>'Message',
    'send'=>'Send',
    'text'=>'We are waiting for you',
    'error1'=>'Please enter message',
    'error2'=>'Have problem in sending message',
    'success'=>'Message sent correctly',
    'error3'=>'e-mail is not a valid format',
    'info'=>'Contact information',
);