<?php
return array(
    "team"=>'تیم',
    "learning"=>'آموزش',
    "contact"=>'تماس-با-ما',
    "customer_portal"=>'سامانه-مشتریان',
    "product"=>'محصولات',
    "news"=>'اخبار',
    "about"=>"درباره-ما",
    "service"=>"خدمات",
    "home"=>"خانه",
    "history"=>"تاریخچه",
    "projects"=>"پروژه-ها",
    "certificate"=>"مدارک",
    "customers"=>"مشتریان",
    "mission_statement"=>"بیانیه-ماموریت",
    "organizational_value"=>"ارزشهای-سازمانی",
    "vision"=>"چشم-انداز",
    "glance"=>"شرکت-در-یک-نگاه",
);