<footer class="main">
    <div class="container">
        <div class="row">
            <div class="span2">
                <a class="logo" href="#">
                    <img src="{{url("front")}}/img/logo-footer.png" alt="logo" title="logo">
                </a>
                <div class="gap gap-small"></div>
                <div class="span2 emails">
                    it at vestatavan.com <br>
                    info at vestatavan.com <br>
                    manager at vestatavan.com <br>
                </div>
            </div>
            <div class="span3 offset1">
                <h5>@lang('menu.about')</h5>
                <p>{{ str_limit(Page::find(13)->{$la.'_content'},350,'' )}}</p>
            </div>
            <div class="span3">
                <h5>Subscribe</h5>
                <p>Ultrices magnis euismod tempus mi nunc convallis sem dis duis</p>
                <form class="sign-up">
                    <input type="text" placeholder="E-mail" class="span3"><span class="help-block">*We never send spam</span>
                    <input type="submit" value="Subscribe" class="btn btn-primary">
                </form>
            </div>
            <div class="span3">
                <h5>Keep in Touch</h5>
                <p>Curabitur congue molestie vitae porttitor enim nulla consequat non donec arcu himenaeos eget lectus vehicula vel inceptos ultrices taciti nec</p>
                <ul class="list list-inline">
                    <li>
                        <a class="icon-facebook box-icon-inverse box-icon-to-normal round animate-icon-left-to-right" href="#"></a>
                    </li>
                    <li>
                        <a class="icon-twitter box-icon-inverse box-icon-to-normal round animate-icon-left-to-right" href="#"></a>
                    </li>
                    <li>
                        <a class="icon-dribbble box-icon-inverse box-icon-to-normal round animate-icon-left-to-right" href="#"></a>
                    </li>
                    <li>
                        <a class="icon-flickr box-icon-inverse box-icon-to-normal round animate-icon-left-to-right" href="#"></a>
                    </li>
                    <li>
                        <a class="icon-google-plus box-icon-inverse box-icon-to-normal round animate-icon-left-to-right" href="#"></a>
                    </li>
                    <li>
                        <a class="icon-rss box-icon-inverse box-icon-to-normal round animate-icon-left-to-right" href="#"></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>