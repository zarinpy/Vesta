<?php $la=App::getLocale();?>
<header class="main">
    <div class="container">
        <div class="row">
            <div class="span3">
                <a class="logo" href="/{{$la}}"><img src="{{url('front')}}/img/Untitled-1.png" alt="logo" title="logo"></a>
            </div>
            <div class="span8">
                <div class="flexnav-menu-button" id="flexnav-menu-button">Menu<span class="touch-button"></span></div>
                <nav>
                    <ul class="nav nav-pills flexnav lg-screen" id="flexnav" data-breakpoint="800">
                        @if($la == 'fa')
                            <li class="item-with-ul"><a href="/{{$la}}/@lang('routes.about')">@lang('menu.about')</a>
                                <ul class="" style="display: none;">
                                    <li><a href="/{{$la}}/@lang('routes.certificate')">@lang('menu.certificate')</a></li>
                                    <li><a href="/{{$la}}/@lang('routes.mission_statement')">@lang('menu.mission_statement')</a></li>
                                    <li><a href="/{{$la}}/@lang('routes.organizational_value')">@lang('menu.organizational_value')</a></li>
                                </ul>
                                <span class="touch-button"></span></li>
                            <li class="item-with-ul"><a href="/{{$la}}/@lang('routes.contact')">@lang('menu.contact')</a>
                                <span class="touch-button"></span></li>
                            <li class="item-with-ul"><a href="/{{$la}}/@lang('routes.customer_portal')">@lang('menu.portal')</a>
                                <span class="touch-button"></span></li>
                            <li class=" item-with-ul"><a href="/{{$la}}/@lang('routes.learning')">@lang('menu.learning')</a>
                                <span class="touch-button"></span></li>
                            <li class="item-with-ul"><a href="/{{$la}}/@lang('routes.product')">@lang('menu.products')</a>
                                <span class="touch-button"></span></li>
                            <li class="item-with-ul"><a href="/{{$la}}/@lang('routes.projects')">@lang('menu.project')</a>
                                <span class="touch-button"></span></li>
                            <li class="item-with-ul"><a href="/{{$la}}">@lang('menu.home')</a>
                                <span class="touch-button"></span></li>
                        @else
                            <li class="item-with-ul"><a href="/{{$la}}">@lang('menu.home')</a>
                                <span class="touch-button"></span></li>
                            <li class="item-with-ul"><a href="/{{$la}}/@lang('routes.projects')">@lang('menu.project')</a>
                                <span class="touch-button"></span></li>
                            <li class="item-with-ul"><a href="/{{$la}}/@lang('routes.product')">@lang('menu.products')</a>
                                <span class="touch-button"></span></li>
                            <li class=" item-with-ul"><a href="/{{$la}}/@lang('routes.learning')">@lang('menu.learning')</a>
                                <span class="touch-button"></span></li>
                            <li class="item-with-ul"><a href="/{{$la}}/@lang('routes.customer_portal')">@lang('menu.portal')</a>
                                <span class="touch-button"></span></li>
                            <li class="item-with-ul"><a href="/{{$la}}/@lang('routes.about')">@lang('menu.about')</a>
                                <ul class="" style="display: none;">
                                    <li><a href="/{{$la}}/@lang('routes.certificate')">@lang('menu.certificate')</a></li>
                                    <li><a href="/{{$la}}/@lang('routes.mission_statement')">@lang('menu.mission_statement')</a></li>
                                    <li><a href="/{{$la}}/@lang('routes.organizational_value')">@lang('menu.organizational_value')</a></li>
                                </ul>
                                <span class="touch-button"></span></li>
                            <li class="item-with-ul"><a href="/{{$la}}/@lang('routes.contact')">@lang('menu.contact')</a>
                                <span class="touch-button"></span></li>
                        @endif
                    </ul>
                </nav>
            </div>
            <div class="span1">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @lang('messages.language') <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <ul class="list stacked-4">
                        <li><a href="/en"><img class="flags" src="{{url("front")}}/img/flags/us.png"></a></li>
                        <li><a href="/fa"><img class="flags" src="{{url("front")}}/img/flags/ir.png"></a></li>
                        <li><a href="/ru"><img class="flags" src="{{url("front")}}/img/flags/ru.png"></a></li>
                        </ul>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>