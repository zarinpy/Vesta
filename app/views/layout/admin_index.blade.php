<!DOCTYPE html>
<!-- Template Name: Clip-One - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.4 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    <title>vestatavan</title>
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <meta content="" name="author" />
    <!-- end: META -->
    <!-- start: MAIN CSS -->
    @section('style')
        {{HTML::style('backend/assets/plugins/bootstrap/css/bootstrap.min.css')}}
        {{HTML::style('backend/assets/plugins/font-awesome/css/font-awesome.min.css')}}
        {{HTML::style('backend/assets/fonts/style.css')}}
        {{HTML::style('backend/assets/css/main.css')}}
        {{HTML::style('backend/assets/css/main-responsive.css')}}
        {{HTML::style('backend/assets/plugins/iCheck/skins/all.css')}}
        {{HTML::style('backend/assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css')}}
        {{HTML::style('backend/assets/plugins/perfect-scrollbar/src/perfect-scrollbar-rtl.css')}}
        {{HTML::style('backend/assets/css/theme_light.css',array('type'=>'text/css','id'=>'skin-color'))}}
        {{HTML::style('backend/assets/css/print.css',array('type'=>'text/css','media'=>'print'))}}
        {{HTML::style('backend/assets/css/rtl-version.css')}}
        <!--[if IE 7]>
        {{HTML::style('backend/assets/plugins/font-awesome/css/font-awesome-ie7.min.css')}}
        <!-- <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
         <![endif]-->
        <!-- end: MAIN CSS -->
    @show()
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body class="rtl">
<!-- start: HEADER -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <!-- start: TOP NAVIGATION CONTAINER -->
    <div class="container">
        <div class="navbar-header">
            <!-- start: RESPONSIVE MENU TOGGLER -->
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="clip-list-2"></span>
            </button>
            <!-- end: RESPONSIVE MENU TOGGLER -->
            <!-- start: LOGO -->
            <a class="navbar-brand" href="{{URL::to('admin/dashboard')}}">
                {{HTML::image('backend/assets/images/logo.png',null,array('width'=>120,'height'=>40))}}
            </a>
            <!-- end: LOGO -->
        </div>
        <div class="navbar-tools">
            <!-- start: TOP NAVIGATION MENU -->
            <ul class="nav navbar-right">
                <!-- start: TO-DO DROPDOWN -->
                <li class="dropdown">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                        <i class="clip-list-5"></i>
                        <span class="badge"> 12</span>
                    </a>
                    <ul class="dropdown-menu todo">
                        <li>
                            <span class="dropdown-menu-title"> You have 12 pending tasks</span>
                        </li>
                        <li>
                            <div class="drop-down-wrapper">
                                <ul>
                                    <li>
                                        <a class="todo-actions" href="javascript:void(0)">
                                            <i class="fa fa-square-o"></i>
                                            <span class="desc" style="opacity: 1; text-decoration: none;">Staff Meeting</span>
                                            <span class="label label-danger" style="opacity: 1;"> today</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="todo-actions" href="javascript:void(0)">
                                            <i class="fa fa-square-o"></i>
                                            <span class="desc" style="opacity: 1; text-decoration: none;"> New frontend layout</span>
                                            <span class="label label-danger" style="opacity: 1;"> today</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="todo-actions" href="javascript:void(0)">
                                            <i class="fa fa-square-o"></i>
                                            <span class="desc"> Hire developers</span>
                                            <span class="label label-warning"> tommorow</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="todo-actions" href="javascript:void(0)">
                                            <i class="fa fa-square-o"></i>
                                            <span class="desc">Staff Meeting</span>
                                            <span class="label label-warning"> tommorow</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="todo-actions" href="javascript:void(0)">
                                            <i class="fa fa-square-o"></i>
                                            <span class="desc"> New frontend layout</span>
                                            <span class="label label-success"> this week</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="todo-actions" href="javascript:void(0)">
                                            <i class="fa fa-square-o"></i>
                                            <span class="desc"> Hire developers</span>
                                            <span class="label label-success"> this week</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="todo-actions" href="javascript:void(0)">
                                            <i class="fa fa-square-o"></i>
                                            <span class="desc"> New frontend layout</span>
                                            <span class="label label-info"> this month</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="todo-actions" href="javascript:void(0)">
                                            <i class="fa fa-square-o"></i>
                                            <span class="desc"> Hire developers</span>
                                            <span class="label label-info"> this month</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="todo-actions" href="javascript:void(0)">
                                            <i class="fa fa-square-o"></i>
                                            <span class="desc" style="opacity: 1; text-decoration: none;">Staff Meeting</span>
                                            <span class="label label-danger" style="opacity: 1;"> today</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="todo-actions" href="javascript:void(0)">
                                            <i class="fa fa-square-o"></i>
                                            <span class="desc" style="opacity: 1; text-decoration: none;"> New frontend layout</span>
                                            <span class="label label-danger" style="opacity: 1;"> today</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="todo-actions" href="javascript:void(0)">
                                            <i class="fa fa-square-o"></i>
                                            <span class="desc"> Hire developers</span>
                                            <span class="label label-warning"> tommorow</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="view-all">
                            <a href="javascript:void(0)">
                                See all tasks <i class="fa fa-arrow-circle-o-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- end: TO-DO DROPDOWN-->
                <!-- start: NOTIFICATION DROPDOWN -->
                <li class="dropdown">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                        <i class="clip-notification-2"></i>
                        <span class="badge"> 11</span>
                    </a>
                    <ul class="dropdown-menu notifications">
                        <li>
                            <span class="dropdown-menu-title"> You have 11 notifications</span>
                        </li>
                        <li>
                            <div class="drop-down-wrapper">
                                <ul>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-primary"><i class="fa fa-user"></i></span>
                                            <span class="message"> New user registration</span>
                                            <span class="time"> 1 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-success"><i class="fa fa-comment"></i></span>
                                            <span class="message"> New comment</span>
                                            <span class="time"> 7 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-success"><i class="fa fa-comment"></i></span>
                                            <span class="message"> New comment</span>
                                            <span class="time"> 8 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-success"><i class="fa fa-comment"></i></span>
                                            <span class="message"> New comment</span>
                                            <span class="time"> 16 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-primary"><i class="fa fa-user"></i></span>
                                            <span class="message"> New user registration</span>
                                            <span class="time"> 36 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-warning"><i class="fa fa-shopping-cart"></i></span>
                                            <span class="message"> 2 items sold</span>
                                            <span class="time"> 1 hour</span>
                                        </a>
                                    </li>
                                    <li class="warning">
                                        <a href="javascript:void(0)">
                                            <span class="label label-danger"><i class="fa fa-user"></i></span>
                                            <span class="message"> User deleted account</span>
                                            <span class="time"> 2 hour</span>
                                        </a>
                                    </li>
                                    <li class="warning">
                                        <a href="javascript:void(0)">
                                            <span class="label label-danger"><i class="fa fa-shopping-cart"></i></span>
                                            <span class="message"> Transaction was canceled</span>
                                            <span class="time"> 6 hour</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-success"><i class="fa fa-comment"></i></span>
                                            <span class="message"> New comment</span>
                                            <span class="time"> yesterday</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-primary"><i class="fa fa-user"></i></span>
                                            <span class="message"> New user registration</span>
                                            <span class="time"> yesterday</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-primary"><i class="fa fa-user"></i></span>
                                            <span class="message"> New user registration</span>
                                            <span class="time"> yesterday</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-success"><i class="fa fa-comment"></i></span>
                                            <span class="message"> New comment</span>
                                            <span class="time"> yesterday</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-success"><i class="fa fa-comment"></i></span>
                                            <span class="message"> New comment</span>
                                            <span class="time"> yesterday</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="view-all">
                            <a href="javascript:void(0)">
                                See all notifications <i class="fa fa-arrow-circle-o-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- end: NOTIFICATION DROPDOWN -->
                <!-- start: MESSAGE DROPDOWN -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-close-others="true" data-hover="dropdown" data-toggle="dropdown" href="#">
                        <i class="clip-bubble-3"></i>
                        <span class="badge"> 9</span>
                    </a>
                    <ul class="dropdown-menu posts">
                        <li>
                            <span class="dropdown-menu-title"> You have 9 messages</span>
                        </li>
                        <li>
                            <div class="drop-down-wrapper">
                                <ul>
                                    <li>
                                        <a href="javascript:;">
                                            <div class="clearfix">
                                                <div class="thread-image">
                                                    <img alt="" src="./assets/images/avatar-2.jpg">
                                                </div>
                                                <div class="thread-content">
                                                    <span class="author">Nicole Bell</span>
                                                    <span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</span>
                                                    <span class="time"> Just Now</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <div class="clearfix">
                                                <div class="thread-image">
                                                    <img alt="" src="./assets/images/avatar-1.jpg">
                                                </div>
                                                <div class="thread-content">
                                                    <span class="author">Peter Clark</span>
                                                    <span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</span>
                                                    <span class="time">2 mins</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <div class="clearfix">
                                                <div class="thread-image">
                                                    <img alt="" src="./assets/images/avatar-3.jpg">
                                                </div>
                                                <div class="thread-content">
                                                    <span class="author">Steven Thompson</span>
                                                    <span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</span>
                                                    <span class="time">8 hrs</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <div class="clearfix">
                                                <div class="thread-image">
                                                    <img alt="" src="./assets/images/avatar-1.jpg">
                                                </div>
                                                <div class="thread-content">
                                                    <span class="author">Peter Clark</span>
                                                    <span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</span>
                                                    <span class="time">9 hrs</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            <div class="clearfix">
                                                <div class="thread-image">
                                                    <img alt="" src="./assets/images/avatar-5.jpg">
                                                </div>
                                                <div class="thread-content">
                                                    <span class="author">Kenneth Ross</span>
                                                    <span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</span>
                                                    <span class="time">14 hrs</span>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="view-all">
                            <a href="pages_messages.html">
                                See all messages <i class="fa fa-arrow-circle-o-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- end: MESSAGE DROPDOWN -->
                <!-- start: USER DROPDOWN -->
                <li class="dropdown current-user">
                    <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                        <span class="username">{{ Auth::user()->email }}</span><i class="clip-chevron-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#">
                                <i class="clip-user-2"></i>
                                &nbsp;My Profile
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="clip-calendar"></i>
                                &nbsp;My Calendar
                            </a>
                        <li>
                            <a href="#">
                                <i class="clip-bubble-4"></i>
                                &nbsp;My Messages (3)
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="clip-locked"></i>
                                &nbsp;Lock Screen </a>
                        </li>
                        <li>
                            <a href="{{URL::route('logout')}}">
                                <i class="clip-exit"></i>
                                &nbsp;خروج
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- end: USER DROPDOWN -->
                <!-- start: PAGE SIDEBAR TOGGLE -->
                <!-- end: PAGE SIDEBAR TOGGLE -->
            </ul>
            <!-- end: TOP NAVIGATION MENU -->
        </div>
    </div>
    <!-- end: TOP NAVIGATION CONTAINER -->
</div>
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
        <div class="main-navigation navbar-collapse collapse">
            <!-- start: MAIN MENU TOGGLER BUTTON -->
            <div class="navigation-toggler">
                <i class="clip-chevron-left"></i>
                <i class="clip-chevron-right"></i>
            </div>
            <!-- end: MAIN MENU TOGGLER BUTTON -->
            <!-- start: MAIN NAVIGATION MENU -->
            <ul class="main-navigation-menu">
                <li {{$title or ""}}>
                    <a href="{{URL::to('admin/dashboard')}}"><i class="clip-home-3"></i>
                        <span class="title"> Dashboard </span><span class="selected"></span>
                    </a>
                </li>
                <li {{$class_menu or ""}}>
                    <a href="javascript:void(0)"><i class="clip-screen"></i>
                        <span class="title"> فهرست </span><i class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{URL::route('admin.menu.create')}}">
                                <span class="title">ایجاد فهرست جدید </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.menu.index')}}">
                                <span class="title">لیست فهرست ها</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li {{$class_team or ""}}>
                    <a href="javascript:void(0)"><i class="clip-cog-2"></i>
                        <span class="title"> اعضای تیم </span><i class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{URL::route('admin.teams.create')}}">
                                <span class="title"> ایجاد عضو جدید </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.teams.index')}}">
                                <span class="title"> لیست اعضای تیم</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.team_categories.index')}}">
                                <span class="title">لیست سمت ها</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.team_categories.create')}}">
                                <span class="title">ایجاد سمت جدید</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.team_profession_categories.index')}}">
                                <span class="title">لیست حرفه ها</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.team_profession_categories.create')}}">
                                <span class="title">ایجاد حرفه جدید</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li {{$class_portfolio or ""}}>
                    <a href="javascript:void(0)"><i class="clip-grid-6"></i>
                        <span class="title"> نمونه کارها</span><i class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li >
                            <a href="javascript:;">پروژه ها <i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="{{URL::route('admin.portfolios.create')}}">ایجاد پروژه جدید</a></li>
                                <li><a href="{{URL::route('admin.portfolios.index')}}">لیست پروژه ها</a></li>
                                <li><a href="{{URL::route('admin.portfolio_categories.create')}}">ایجاد دسته بندی</a></li>
                                <li><a href="{{URL::route('admin.portfolio_categories.index')}}">لیست دسته بندی</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;">مشتریان<i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="{{URL::route('admin.portfolio_customers.create')}}">ایجاد مشتری</a></li>
                                <li><a href="{{URL::route('admin.portfolio_customers.index')}}">لیست مشتریان </a></li>
                            </ul>
                        </li>
                        <li >
                            <a href="javascript:;">همکاران<i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li><a href="{{URL::route('admin.companies.create')}}">ایجاد همکار جدید</a></li>
                                <li><a href="{{URL::route('admin.companies.index')}}">لیست همکاران</a></li>
                                <li><a href="{{URL::route('admin.company_categories.create')}}">ایجاد دسته بندی همکاران </a></li>
                                <li><a href="{{URL::route('admin.company_categories.index')}}">لیست همکاران</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li {{$class_elearning or ''}}>
                    <a href="javascript:void(0)"><i class="clip-pencil"></i>
                        <span class="title"> آموزش</span><i class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{URL::route('admin.elearnings.create')}}">
                                <span class="title">ایجاد مطلب جدید</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.elearnings.index')}}">
                                <span class="title">لیست مطالب</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.elearning_categories.create')}}">
                                <span class="title">ایجاد دسته بندی</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.elearning_categories.index')}}">
                                <span class="title">لیست دسته بندی</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li {{$class_media or ""}}>
                    <a href="javascript:void(0)"><i class="clip-user-2"></i>
                        <span class="title">رسانه</span><i class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{URL::route('admin.medias.create')}}">
                                <span class="title">بارگزاری رسانه</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.medias.index')}}">
                                <span class="title">لیست رسانه ها</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li {{$class_page or ""}}>
                    <a href="javascript:void(0)">
                        <i class="clip-file"></i>
                        <span class="title">صفحات</span><i class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{URL::route('admin.pages.create')}}">
                                <span class="selected">ایجاد صفحه جدید</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.pages.index')}}">
                                <span class="selected">لیست صفحات</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li {{$class_product or ""}}>
                    <a href="javascript:void(0)"><i class="clip-file"></i>
                        <span class="title">محصولات</span><i class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{URL::route('admin.products.create')}}">
                                <span class="title">ایجاد محصول</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.products.index')}}">
                                <span class="title">لیست محصولات</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.product_categories.create')}}">
                                <span class="title">ایجاد دسته بندی</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.product_categories.index')}}">
                                <span class="title">لیست دسته بندی</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li {{$class_datasheets or ""}}>
                    <a href="javascript:void(0)"><i class="clip-attachment-2"></i>
                        <span class="title"> اطلاعات مشتریان و پیمانکاران</span><i class="icon-arrow"></i>
                        <span class="selected"></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="javascript:void(0)">
                            <span class="title">داده ها</span><i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="{{URL::route('admin.datasheets.create')}}"><span class="title">ایجاد داده</span></a>
                                </li>
                                <li>
                                    <a href="{{URL::route('admin.datasheets.index')}}"><span class="title">لیست داده ها </span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        <span class="title">نوع داده</span><i class="icon-arrow"></i>
                                    </a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="{{URL::route('admin.datasheet_project_types.index')}}"><span class="title">لیست  نوع داده ها</span></a>
                                        </li>
                                        <li>
                                            <a href="{{URL::route('admin.datasheet_project_types.create')}}"><span class="title">ایجاد  نوع داده </span></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <span class="title">سریال پروه ها</span><i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="{{URL::route('admin.datasheet_codes.create')}}"><span class="title">ایجاد سریال</span></a>
                                </li>
                                <li>
                                    <a href="{{URL::route('admin.datasheet_codes.index')}}"><span class="title">لیست   سریال ها</span></a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <span class="title">پیمان کاران</span><i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="{{URL::route('admin.datasheet_companies.create')}}"><span class="title">ایجاد پیمانکار</span></a>
                                </li>
                                <li>
                                    <a href="{{URL::route('admin.datasheet_companies.index')}}"><span class="title">لیست پیمانکارها</span></a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <span class="title">گروه ها</span><i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="{{URL::route('admin.groups.create')}}"><span class="title">ایجاد گروه</span></a>
                                </li>
                                <li>
                                    <a href="{{URL::route('admin.groups.index')}}"><span class="title">لیست گروه ها</span></a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <span class="title">کاربران</span><i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="{{URL::route('admin.users.create')}}"><span class="title">ایجاد کاربر</span></a>
                                </li>
                                <li>
                                    <a href="{{URL::route('admin.users.index')}}"><span class="title">لیست کاربران</span></a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li {{$class_service or "" }}>
                    <a href="javascript:void(0)" ><i class="clip-folder"></i><span class="title"> خدمات </span>
                        <i class="icon-arrow"></i>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{URL::route('admin.services.create')}}">
                                ایجاد خدمات
                            </a>
                        </li>
                        <li>
                            <a href="{{URL::route('admin.services.index')}}">
                               لیست خدمات
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <span class="title">دسته بندی</span><i class="icon-arrow"></i>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="{{URL::route('admin.service_categories.create')}}">ایجاد دسته بندی</a>
                                </li>
                                <li>
                                    <a href="{{URL::route('admin.service_categories.index')}}">لیست دسته بندی</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li {{$class_slider or ''}}>
                    <a href="javascript:(0)">
                        <i class="clip-folder-open"></i>
                        <span class="title"> اسلایدر </span><i class="icon-arrow"></i>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{URL::route('admin.sliders.index')}}">لیست تصاویر</a></li>
                        <li><a href="{{URL::route('admin.sliders.create')}}">ایجاد اسلاید</a></li>
                    </ul>
                </li>
                <li {{$class_setting or ''}}>
                    <a href="javascript:(0)">
                        <i class="clip-folder-open"></i>
                        <span class="title">تنظیمات</span><i class="icon-arrow"></i>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="{{URL::route('admin.settings.create')}}">تغییر تنظیمات</a></li>

                    </ul>
                </li>
            </ul>
            <!-- end: MAIN NAVIGATION MENU -->
        </div>
        <!-- end: SIDEBAR -->
    </div>
    <!-- start: PAGE -->
    <div class="main-content">
        <!-- start: PANEL CONFIGURATION MODAL FORM -->
        <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">Panel Configuration</h4>
                    </div>
                    <div class="modal-body">
                        Here will be a configuration form
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Close
                        </button>
                        <button type="button" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- end: SPANEL CONFIGURATION MODAL FORM -->
        <div class="container">
            <!-- start: PAGE HEADER -->
            <div class="row">
                <div class="col-sm-12">

                    <!-- start: PAGE TITLE & BREADCRUMB -->
                    <ol class="breadcrumb">
                        <li>
                            <i class="clip-home-3"></i>
                            <a href="#">
                                Home
                            </a>
                        </li>
                        <li class="active">
                            Dashboard
                        </li>
                        <li class="search-box">
                            <form class="sidebar-search">
                                <div class="form-group">
                                    <input type="text" placeholder="Start Searching...">
                                    <button class="submit">
                                        <i class="clip-search-3"></i>
                                    </button>
                                </div>
                            </form>
                        </li>
                    </ol>
                    <div class="page-header">
                        <h1>{{$title or "داشبورد"}} <small>وستا توان فرتاک</small></h1>
                        {{$info_product or "" }}
                        {{$info_product_edit or ""}}
                    </div>
                    <!-- end: PAGE TITLE & BREADCRUMB -->
                </div>
            </div>
            <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
            @yield('content')
            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->
<div class="footer clearfix">
    <div class="footer-inner">
        2014 &copy; clip-one by cliptheme.
    </div>
    <div class="footer-items">
        <span class="go-top"><i class="clip-chevron-up"></i></span>
    </div>
</div>
<!-- end: FOOTER -->
<!-- start: LEFT SIDEBAR -->
<!-- end: LEFT SIDEBAR -->
<div id="event-management" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">Event Management</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-light-grey">
                    Close
                </button>
                <button type="button" class="btn btn-danger remove-event no-display">
                    <i class='fa fa-trash-o'></i> Delete Event
                </button>
                <button type='submit' class='btn btn-success save-event'>
                    <i class='fa fa-check'></i> Save
                </button>
            </div>
        </div>
    </div>
</div>
@section('script')
    <!-- start: MAIN JAVASCRIPTS
     <script src="assets/plugins/respond.min.js"></script>
     <script src="assets/plugins/excanvas.min.js"></script>
     <script type="text/javascript" src="assets/plugins/jQuery-lib/1.10.2/jquery.min.js"></script>
    -->
    <!--[if lt IE 9]>
    {{HTML::script('backend/assets/plugins/respond.min.js')}}
    {{HTML::script('backend/assets/plugins/jQuery-lib/1.10.2/jquery.min.js')}}
    <![endif]--
    <!--[if gte IE 9]><!-->
    {{HTML::script('backend/assets/plugins/jQuery-lib/2.0.3/jquery.min.js')}}
    <!--<![endif]-->
    {{HTML::script('backend/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js')}}
    {{HTML::script('backend/assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js')}}
    {{HTML::script('backend/assets/plugins/autosize/jquery.autosize.min.js')}}
    {{HTML::script('backend/assets/plugins/bootstrap/js/bootstrap.min.js')}}
    {{HTML::script('backend/assets/plugins/blockUI/jquery.blockUI.js')}}
    {{HTML::script('backend/assets/plugins/iCheck/jquery.icheck.min.js')}}
    {{HTML::script('backend/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js')}}
    {{HTML::script('backend/assets/plugins/perfect-scrollbar/src/perfect-scrollbar-rtl.js')}}
    {{HTML::script('backend/assets/plugins/less/less-1.5.0.min.js')}}
    {{HTML::script('backend/assets/plugins/jquery-cookie/jquery.cookie.js')}}
    {{HTML::script('backend/assets/js/main.js')}}
    {{HTML::script('backend/assets/js/form-elements.js')}}
    <!-- end: MAIN JAVASCRIPTS -->
@show()
@section('index_script')
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
{{HTML::script('backend/assets/plugins/flot/jquery.flot.js')}}
{{HTML::script('backend/assets/plugins/flot/jquery.flot.pie.js')}}
{{HTML::script('backend/assets/plugins/flot/jquery.flot.resize.min.js')}}
{{HTML::script('backend/assets/js/index.js')}}
{{HTML::script('backend/assets/js/form-elements.js')}}
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script>
    jQuery(document).ready(function() {
        Main.init();
        Index.init();
        FormElements.init();
        TableExport.init();
        Dropzone.init();
    });
</script>
@show()
</body>
<!-- end: BODY -->
</html>