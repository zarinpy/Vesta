<?php $la = App::getLocale();?>
<!DOCTYPE HTML>
<html prefix="og: http://ogp.me/ns#" lang="{{$la}}">
<head>
    @section("head")
    @show
    <link rel="apple-touch-icon" sizes="57x57" href="{{url(('front/favicons'))}}/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{{url(('front/favicons'))}}/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{url(('front/favicons'))}}/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{url(('front/favicons'))}}/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{url(('front/favicons'))}}/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{url(('front/favicons'))}}/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{url(('front/favicons'))}}/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{url(('front/favicons'))}}/apple-touch-icon-152x152.png">
    <link rel="icon" type="image/png" href="{{url(('front/favicons'))}}/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="{{url(('front/favicons'))}}/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="{{url(('front/favicons'))}}/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="{{url(('front/favicons'))}}/manifest.json">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{url('front')}}/css/boostrap.css">
    <link rel="stylesheet" href="{{url('front')}}/css/boostrap_responsive.css">
    <link rel="stylesheet" href="{{url('front')}}/css/font_awesome.css">
    @if($la=='fa')
        <link rel="stylesheet" href="{{url('front')}}/css/styles_fa.css">
    @else()
        <link rel="stylesheet" href="{{url('front')}}/css/styles_en.css">
   @endif()
    <link rel="stylesheet" href="{{url('front')}}/css/mystyles.css">
    <!-- IE 8 Fallback -->
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="{{url('front')}}/css/ie8.css" />
    <![endif]-->
    <!-- Modernizr script -->
    <script src="{{url('front')}}/js/modernizr.custom.26633.js"></script>
</head>
<body class="width1200 sticky-header">
<div class="container-global">
    @include('layout.front.header')
    @yield("content")
    @include('layout.front.footer')
    <script src="{{url('front')}}/js/jquery.js"></script>
    <script src="{{url('front')}}/js/easing.min.js"></script>
    <script src="{{url('front')}}/js/touch-swipe.js"></script>
    <script src="{{url('front')}}/js/boostrap.min.js"></script>
    <script src="{{url('front')}}/js/flexnav.min.js"></script>
    <script src="{{url('front')}}/js/countdown.min.js"></script>
    <script src="{{url('front')}}/js/magnific.min.js"></script>
    <script src="{{url('front')}}/js/mediaelement.min.js"></script>
    <script src="{{url('front')}}/js/fitvids.min.js"></script>
    <script src="{{url('front')}}/js/gridrotator.min.js"></script>
    <script src="{{url('front')}}/js/fredsel.min.js"></script>
    <script src="{{url('front')}}/js/backgroundsize.min.js"></script>
    <script src="{{url('front')}}/js/superslides.min.js"></script>
    <script src="{{url('front')}}/js/one-page-nav.min.js"></script>
    <script src="{{url('front')}}/js/scroll-to.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script src="{{url('front')}}/js/gmap3.min.js"></script>
    <script src="{{url('front')}}/js/tweet.min.js"></script>
    <script src="{{url('front')}}/js/mixitup.min.js"></script>
    <script src="{{url('front')}}/js/mail.min.js"></script>
    <script src="{{url('front')}}/js/transit-modified.js"></script>
    <script src="{{url('front')}}/js/layerslider-transitions.min.js"></script>
    <script src="{{url('front')}}/js/layerslider.js"></script>
    <script src="{{url('front')}}/js/custom.js"></script>
</div>
</body>
</html>