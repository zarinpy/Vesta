@extends('layout.admin_index')

@section('style')
    @parent
@stop()

@section('content')
    <div class="col-sm-12 page-error">
        <div class="error-number teal">
            404
        </div>
        <div class="error-details col-sm-6 col-sm-offset-3">
            <h3>کاربر گرامی آدرسی که وارد کرده اید اشتباه است  </h3>
            <p>
                متاسفانه صفحه ای که در این سیستم به دنبال آن هستید وجود ندارد
                <br>
                یا ممکن است به طور موقت حذف یا غیرفعال شده باشد
                <br>
                ادرسی که وارد کرده اید را دوباره بررسی کنید و تلاش کنید
                <br>
                {{HTML::link('admin/dashboard','برگشت به خانه',array('class'=>'btn btn-teal btn-return'))}}
                <br>
            </p>
        </div>
    </div>
@stop()
@section('script')
    @parent
@stop()