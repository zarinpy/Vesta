<li>
    <?php

    $disable ="";
    if(!$menu['active']){
        $disable .= "<span class='label label-warning'>غیر فعال</span>";
    }
    ?>
    {{ HTML::link('admin/product_categories/' . $menu['id'].'/edit/', $menu['fa_name'],array('style'=>'font-size: 20px')) .' '.$disable}}
</li>
@if(count($menu['child']) > 0)
    <ul>
        @foreach($menu['child'] as $menu)
            @include('backend.product_categories.list_edit', $menu)
        @endforeach
    </ul>
@endif