@extends("layout.admin_index")
@section('style')
    @parent
@stop()
@section('content')
    <div class="col-sm-9">
        @if(Session::has('success'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('success')}}
            </div>
        @endif()
        @if(Session::has('update'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('update')}}
            </div>
        @endif()
    </div>
    <div class="portlet">
        <div class="portlet-content">
            <div class="row">
                <div class="col-sm-12">
                    @if(count($menus) > 0)
                        <ul>
                            @foreach($menus as $menu)
                                @include('backend.product_categories.list_index', array('menu'=>$menu))
                            @endforeach
                        </ul>
                    @else
                        {{ "دسته بندی یافت نشد" }}
                    @endif
                </div> <!-- /.col -->
            </div> <!-- /.row -->
        </div> <!-- /.portlet-content -->
    </div>
    <div class="alert alert-block alert-warning fade in">
        <button data-dismiss="alert" class="close" type="button">
            ×
        </button>
        <h4 class="alert-heading"><i class="fa fa-exclamation-triangle"></i> Warning!</h4>
        <p><h4>کاربر گرامی توجه داشته باشید که برای حذف دسته بندی به ترتیب از دسته های زیرین شروع کنیددر غیر این صورت برنامه دچار مشکل خواهد شد</h4></h5></p>

    </div>
@stop
@section('script')
    @parent()
@stop()
