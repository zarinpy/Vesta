@extends("layout.admin_index")
@section('style')
    @parent
@stop()
@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
    </div>
    <div class="col-sm-9">
        @if(Session::has('success'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('success')}}
            </div>
        @endif()
        @if(Session::has('warning'))
            <div class="alert alert-warning">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('warning')}}
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{Form::open(array('route'=>'admin.product_categories.store','method'=>'POST','class'=>'form-horizontal','role'=>'form'))}}
                    {{Form::token()}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام دسته بندی محصول</label>
                        <div class="col-sm-9">
                            {{Form::text('fa_name','',array( "placeholder"=>"نام دسته بندی محصول", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام انگلیسی دسته بندی</label>
                        <div class="col-sm-9">
                            {{Form::text('en_name','',array( "placeholder"=>"نام انگلیسی دسته بندی", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">دسته پدر</label>
                        <div class="col-sm-9">
                           <select name="parent_id" class="form-control">
                               <option value="null">هیچکدام</option>
                               <?php $dash='';?>
                               @if(count($menus) > 0)
                                   @foreach($menus as $menu)
                                       @include('backend.product_categories.list',array('menu'=>$menu,'dash'=>$dash))
                                   @endforeach
                               @else
                                   {{ "دسته بندی یافت نشد" }}
                               @endif
                           </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">فعال</label>
                        <div class="col-sm-9">
                            {{Form::checkbox('active',null,null)}}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop
@section('script')
    @parent()
@stop()