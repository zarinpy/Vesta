@extends("layout.admin_index")
@section('style')
    @parent
    {{HTML::style('backend/assets/plugins/DataTables/media/css/DT_bootstrap.css')}}
@stop()
@section('content')
    <div class="col-sm-9">
        @if(Session::has('success'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('success')}}
            </div>
        @endif()
            @if(Session::has('update'))
                <div class="alert alert-success ">
                    <button data-dismiss="alert" class="close">×</button>
                    <i class="fa fa-check-circle"></i>
                    {{Session::get('update')}}
                </div>
        @endif()
    </div>
    <div class="table-responsive">
        <table class="table table-hover" id="sample-table-1">
            <thead>
            <tr>
                <th class="center"> تصویر</th>
                <th class="center">نام</th>
                <th class="center">نام خانوادگی</th>
                <th class="center">سمت</th>
                <th class="center">حرفه</th>
                <th class="center">عملیات</th>
                <th class="center">عملیات</th>
            </tr>
            </thead>
            <tbody>
                @foreach($teams as $t)
            <tr>
                <td class="center"><a href="{{URL::route('admin.teams.edit',array($t->id))}}">{{HTML::image($t->image,$t->fa_name.'--'.$t->fa_family,array('width'=>'50','height'=>'50'))}}</a></td>
                <td class="center">{{$t->fa_name}}</td>
                <td class="center">{{$t->fa_family}}</td>
                <td class="center">{{$t->fa_cat_title}}</td>
                <td class="center">{{$t->fa_prof_title}}</td>
                <td class="center">
                        <a href="{{URL::route('admin.teams.edit',array($t->id))}}" class="btn btn-xs btn-blue" data-placement="top" data-original-title="ویرایش">ویرایش</a>
                </td>
                <td class="center"> {{Form::open(array('url'=>'admin/teams/'.$t->id))}}
                    {{Form::token()}}
                    {{Form::hidden('_method','DELETE')}}
                    {{Form::submit(' حذف ',array('class'=>'btn btn-xs btn-bricky ','style'=>'font-family:b nazanin; ','onclick'=>"if(!confirm('آیا مایل به تغییر وضعیت این ردیف هستید؟')){return false;};"))}}
                    {{Form::close()}}</td>
            </tr>
                @endforeach()
            </tbody>
        </table>
    </div>
@stop
@section('script')
    @parent()
    {{HTML::script('backend/assets/js/table-export.js')}}
    {{HTML::script('backend/assets/plugins/bootbox/bootbox.min.js')}}
    {{HTML::script('backend/assets/plugins/select2/select2.min.js')}}
    {{HTML::script('backend/assets/plugins/jquery-mockjax/jquery.mockjax.js')}}
    {{HTML::script('backend/assets/plugins/DataTables/media/js/jquery.dataTables.min.js')}}
    {{HTML::script('backend/assets/plugins/DataTables/media/js/DT_bootstrap.js')}}
    {{HTML::script('backend/assets/js/table-data.js')}}
@stop()
