@extends("layout.admin_index")
@section('style')
    @parent
    {{HTML::style('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}
    {{HTML::style('//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css')}}
@stop()

@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
        @if(Session::has('img'))
            <div class="alert alert-warning ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                    {{Session::get('img')}}
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{Form::open(array('route'=>'admin.teams.store','method'=>'POST','class'=>'form-horizontal','role'=>'form','enctype'=>"multipart/form-data"))}}
                    <div class="tabbable">
                        <ul id="myTab" class="nav nav-tabs tab-bricky">
                            <li class="active">
                                <a href="#panel_tab2_example1" data-toggle="tab">
                                    <i class="green fa"></i> اطلاعات فارسی
                                </a>
                            </li>
                            <li class="">
                                <a href="#panel_tab2_example2" data-toggle="tab">اطلاعات انگلیسی</a>
                            </li>
                            <li class="">
                                <a href="#panel_tab3_example2" data-toggle="tab">اطلاعات روسی</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="panel_tab2_example1">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">نام فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('fa_name','',array( "placeholder"=>"نام فارسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-2">نام خانوادگی فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('fa_family','',array( "placeholder"=>"نام خانوادگی فارسی", "id"=>"form-field-2", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-7">توضیحات فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('fa_description','',array('placeholder'=>'توضیحات فارسی','id'=>'ee','class'=>'form-control'))}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="panel_tab2_example2">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">نام انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('en_name','',array( "placeholder"=>"نام انگلیسی ", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-2">نام خانوادگی انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('en_family','',array( "placeholder"=>"نام خانوادگی انگلیسی", "id"=>"form-field-2", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-7">توضیحات انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('en_description','',array('placeholder'=>'توضیحات انگلیسی','id'=>'rr','class'=>'form-control'))}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="panel_tab3_example2">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">نام روسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('ru_name','',array( "placeholder"=>"نام روسی ", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-2">نام خانوادگی روسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('ru_family','',array( "placeholder"=>"نام خانوادگی روسی", "id"=>"form-field-2", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-7">توضیحات روسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('ru_description','',array('placeholder'=>'توضیحات روسی','id'=>'aa','class'=>'form-control'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-3">سمت</label>
                        <div class="col-sm-9">
                            {{Form::select('category',$category,null,array('class'=>'select2-container form-control search-select','id'=>'s2id_form-field-select-3'))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-4">حرفه</label>
                        <div class="col-sm-9">
                            {{Form::select('profession',$profession,null,array('class'=>'select2-container form-control search-select','id'=>'s2id_form-field-select-4'))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-5">فیس بوک</label>
                        <div class="col-sm-9">
                            {{Form::text('facebook','',array('placeholder'=>'حساب فیس بوک','id'=>'form-field-4','class'=>'form-control'))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-5">لینکداین</label>
                        <div class="col-sm-9">
                            {{Form::text('linkedin','',array('placeholder'=>'لینکد این','id'=>'form-field-4','class'=>'form-control'))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-6">گوگل پلاس</label>
                        <div class="col-sm-9">
                            {{Form::text('google','',array('placeholder'=>'حساب گوگل پلاس','id'=>'form-field-5','class'=>'form-control'))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> تصویر عضو </label>
                        <div class="col-sm-9">
                            <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden" value="" name="">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt="">
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                                <div>
														<span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> انتخاب تصویر</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> ویرایش تصویر</span>
															<input type="file" name="image" title="image">
														</span>
                                    <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> حذف
                                    </a>
                                </div>
                            </div>
                            <div class="alert alert-warning">
                                <span class="label label-warning">NOTE!</span>
                                <span> Image preview only works in IE10+, FF3.6+, Chrome6.0+ and Opera11.1+. In older browsers and Safari, the filename is shown instead. </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop

@section('script')
    @parent()
    {{HTML::script('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js')}}
    <script type="text/javascript">
        CKEDITOR.replace('ee');
        CKEDITOR.replace('rr');
        CKEDITOR.replace('aa');
    </script>
@stop()
