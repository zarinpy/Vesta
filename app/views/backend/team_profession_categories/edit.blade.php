@extends("layout.admin_index")
@section('style')
    @parent
@stop()
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{Form::model($teamprofessioncategory,array('route'=>array('admin.team_profession_categories.update','id'=>$teamprofessioncategory->id),'method'=>'PUT','class'=>'form-horizontal','role'=>'form')) }}
                    {{Form::token()}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام حرفه</label>
                        <div class="col-sm-9">
                            {{Form::text('fa_prof_title',null,array( "placeholder"=>"نام حرفه", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام انگلیسی حرفه</label>
                        <div class="col-sm-9">
                            {{Form::text('en_prof_title',null,array( "placeholder"=>"نام انگلیسی حرفه", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام روسی حرفه</label>
                        <div class="col-sm-9">
                            {{Form::text('ru_prof_title',null,array( "placeholder"=>"نام روسی حرفه", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                        <div/>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop
@section('script')
    @parent()
@stop()
