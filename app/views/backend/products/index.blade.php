@extends("layout.admin_index")
@section('style')
    @parent
    {{HTML::style('//cdn.datatables.net/1.10.7/css/jquery.dataTables.css')}}
@stop()
@section('content')
    <table class="table table-striped table-bordered table-hover table-full-width" id="sample_1">
        <thead>
        <tr>
            <th class="center">نام محصول</th>
            <th  class="center">دسته بندی</th>
            <th  class="center">توضیحات</th>
            <th  class="center">ویرایش</th>
            <th  class="center">حذف</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $p)
        <tr>
            <td>{{$p->fa_title}}</td>
            <td class="hidden-xs">@foreach($p->category as $cat){{$cat->fa_name}} - @endforeach()</td>
            <td>{{str_limit($p->fa_description,20,'...')}}</td>
            <td class="center">
                <a href="{{URL::route('admin.products.edit',array($p->id))}}" class="btn btn-xs btn-teal tooltips" data-placement="top" >ویرایش</a>
            </td>
            <td>
                {{Form::open(array('url'=>'admin/products/'.$p->id))}}
                {{Form::token()}}
                {{Form::hidden('_method','DELETE')}}
                {{Form::submit(' حذف ',array('class'=>'btn btn-xs btn-bricky ','style'=>'font-family:b nazanin; ','onclick'=>"if(!confirm('آیا مایل به تغییر وضعیت این ردیف هستید؟')){return false;};"))}}
                {{Form::close()}}
            </td>
        </tr>
            @endforeach()
        </tbody>
    </table>
@stop
@section('script')
    @parent()
        {{HTML::script('//cdn.datatables.net/1.10.7/js/jquery.dataTables.js')}}
        {{HTML::script('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}
    <script>
        $(document).ready(function() {
            $('#sample_1').DataTable();
        } );
    </script>
@stop()