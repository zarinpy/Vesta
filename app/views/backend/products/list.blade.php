    <?php
        $class='';
        if(!$menu['active']){
        $class = "style='color: red'";
        }else{
            $class = "style='color: blue'";
        }
    ?>
   <li style="list-style:none">
       <div class="checkbox">
           <label>
               <input type="checkbox" value="{{$menu['id']}}" name="category[]" class="square-grey">
               {{$menu['fa_name']}}
           </label>
       </div>
   </li>
       @if(count($menu['child']) > 0)
    <?php  $dash.=' ';?>
    <ul>
        @foreach($menu['child'] as $menu)
                @include('backend.products.list', $menu)
        @endforeach
    </ul>
       @endif