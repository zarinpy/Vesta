@extends("layout.admin_index")
@section('style')
    @parent
    {{HTML::style('//cdn.datatables.net/1.10.7/css/jquery.dataTables.css')}}
@stop()
@section('content')
    <div class="col-sm-9">
        @if(Session::has('success'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('success')}}
            </div>
        @endif()
        @if(Session::has('update'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('update')}}
            </div>
        @endif()
    </div>
    <div class="table-responsive">
        <table id="sample-table-1" class="table table-hover" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th class="center">عنوان</th>
                <th class="center">دسته بندی</th>
                <th class="center">وبرایش</th>
                <th class="center">حذف</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th class="center">عنوان</th>
                <th class="center">دسته بندی</th>
                <th class="center">وبرایش</th>
                <th class="center">حذف</th>
            </tr>
            </tfoot>
            <tbody>
            @foreach($services as $e)
                <tr>
                    <td class="center "><b>{{$e->fa_title}}</b></td>
                    <td class="center ">
                        @foreach ($e->category as $cat)
                            {{$cat->fa_title}} - {{$cat->en_title}}
                        @endforeach()</td>
                    <td class="center">
                        <a href="{{URL::route('admin.services.edit',array($e->id))}}" class="btn btn-xs btn-teal tooltips" data-placement="top" >ویرایش</a>
                    </td>
                    <td>
                        {{Form::open(array('url'=>'admin/services/'.$e->id))}}
                        {{Form::token()}}
                        {{Form::hidden('_method','DELETE')}}
                        {{Form::submit(' حذف ',array('class'=>'btn btn-xs btn-bricky ','style'=>'font-family:b nazanin; ','onclick'=>"if(!confirm('آیا مایل به تغییر وضعیت این ردیف هستید؟')){return false;};"))}}
                        {{Form::close()}}
                    </td>
                </tr>
            @endforeach()
            </tbody>
        </table>
    </div>
@stop
@section('script')
    @parent()
    {{HTML::script('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}
    <script>
        $(document).ready(function() {
            $('#example').dataTable( {
                "scrollY":        "800px",
                "scrollCollapse": true
            } );
        } );
    </script>
@stop()
