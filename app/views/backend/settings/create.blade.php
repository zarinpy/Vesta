@extends("layout.admin_index")
@section('style')
    @parent
    {{HTML::style('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}
    {{HTML::style('//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css')}}
@stop()
@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
        @if(Session::has('success'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('success')}}
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{Form::open(array('route'=>'admin.settings.store','method'=>'POST','class'=>'form-horizontal','role'=>'form','enctype'=>"multipart/form-data"))}}
                    {{Form::token()}}
                    <div class="tabbable">
                        <ul id="myTab" class="nav nav-tabs tab-bricky">
                            <li class="active">
                                <a href="#panel_tab2_example1" data-toggle="tab">فارسی</a>
                            </li>
                            <li class="">
                                <a href="#panel_tab2_example2" data-toggle="tab">انگلیسی</a>
                            </li>
                            <li class="">
                                <a href="#panel_tab3_example2" data-toggle="tab">روسی</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="panel_tab2_example1">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">توضیحات فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('fa_description',Setting::find(10)->value,array( "placeholder"=>"توضیحات فارسی", "id"=>"form-field-1","dir"=>"ltr", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">کلیدهای فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('fa_keywords',Setting::find(11)->value,array( "placeholder"=>"کلیدهای فارسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">شعار فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('fa_slug',Setting::find(20)->value,array( "placeholder"=>"شعار فارسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="panel_tab2_example2">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">توضیحات انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('en_description',Setting::find(13)->value,array( "placeholder"=>"توضیحات انگلیسی","dir"=>"ltr", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">کلیدهای انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('en_keywords',Setting::find(12)->value,array( "placeholder"=>"کلیدهای انگلیسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">شعار انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('en_slug',Setting::find(21)->value,array( "placeholder"=>"شعار انگلیسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="panel_tab3_example2">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">توضیحات روسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('ru_description',Setting::find(14)->value,array( "placeholder"=>"توضیحات روسی","dir"=>"ltr", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">کلیدهای روسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('ru_keywords',Setting::find(15)->value,array( "placeholder"=>"کلیدهای روسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">شعار روسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('ru_slug',Setting::find(22)->value,array( "placeholder"=>"شعار روسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop
@section('script')
    @parent()
    {{HTML::script('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js')}}
    {{HTML::script('backend/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js')}}
    {{HTML::script('backend/assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js')}}
    <script type="text/javascript">
        $('.js-example-basic-multiple').select2();
        CKEDITOR.replace('ee');
        CKEDITOR.replace('rr');
    </script>
@stop()
