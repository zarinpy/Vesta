@extends("layout.admin_index")
@section('style')
    @parent
@stop()
@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()

    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{Form::open(array('route'=>'admin.portfolio_categories.store','method'=>'POST','class'=>'form-horizontal','role'=>'form'))}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام دسته بندی فارسی</label>
                        <div class="col-sm-9">
                            {{Form::text('fa_pcat_title','',array( "placeholder"=>"نام دسته بندی فارسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام انگلیسی دسته بندی</label>
                        <div class="col-sm-9">
                            {{Form::text('en_pcat_title','',array( "placeholder"=>"نام انگلیسی دسته بندی", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام روسی دسته بندی</label>
                        <div class="col-sm-9">
                            {{Form::text('ru_pcat_title','',array( "placeholder"=>"نام روسی دسته بندی", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop
@section('script')
    @parent()
@stop()