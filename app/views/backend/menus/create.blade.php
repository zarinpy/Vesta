@extends("layout.admin_index")
@section('style')
    @parent
@stop()
@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
        @if(Session::has('success'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('success')}}
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{Form::open(array('route'=>'admin.menus.store','method'=>'POST','class'=>'form-horizontal','role'=>'form'))}}
                    {{Form::token()}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                            نام منو
                        </label>
                        <div class="col-sm-9">
                            {{Form::text('name','',array('placeholder'=>'نام منو ذا وترد کنید','class'=>'form-control','id'=>'form-field-1'))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام انگلیسی منو</label>
                        <div class="col-sm-9">
                            {{Form::text('en_name','',array('placeholder'=>'نام انگلیسی منو','class'=>'form-control','id'=>'form-field-1'))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-2">
                            آدرس
                        </label>
                        <div class="col-sm-9">
                            {{Form::url('url','',array('class'=>'form-control','id'=>"form-field-2"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-2">وضعیت
                        </label>
                        <div class="col-sm-2">
                            <div class="checkbox">
                                <label class="checkbox-inline">
                                    <input type="checkbox" class="square-grey" name="active" value="" checked="checked">
                                    فعال
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-2">
                            پدر
                        </label>
                        <div class="col-sm-9">
                            <select name="parent_id" class="form-control">
                                <option value="null">هیچکدام</option>
                                <?php $dash='';?>
                                @if(count($menu) > 0)
                                    @foreach($menu as $menu)
                                        @include('backend.menus.list',array('menu'=>$menu,'dash'=>$dash))
                                    @endforeach
                                @else
                                    {{ "دسته بندی یافت نشد" }}
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                        <div/>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop
@section('script')
    @parent()
@stop()
