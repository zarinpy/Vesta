@extends("layout.admin_index")
@section('style')
    @parent
@stop()

@section('content')
{{$page->content}}
@stop

@section('script')
    @parent()
@stop()
