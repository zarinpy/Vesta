@extends("layout.admin_index")
@section('style')
    @parent
    {{HTML::style('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}
    {{HTML::script('backend/assets/plugins/ckeditor/ckeditor.js')}}
@stop()
@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{ Form::model($page,array('route'=>array('admin.pages.update','id'=>$page->id),'method'=>'PUT','files'=>true,'class'=>'form-horizontal','role'=>'form')) }}
                    <div class="tabbable">
                        <ul id="myTab" class="nav nav-tabs tab-bricky">
                            <li class="active">
                                <a href="#panel_tab2_example1" data-toggle="tab">
                                    <i class="green fa"></i> اطلاعات فارسی
                                </a>
                            </li>
                            <li class="">
                                <a href="#panel_tab2_example2" data-toggle="tab">اطلاعات انگلیسی</a>
                            </li>
                            <li class="">
                                <a href="#panel_tab3_example2" data-toggle="tab">اطلاعات روسی</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="panel_tab2_example1">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">عنوان فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('fa_title',null,array( "placeholder"=>"عنوان فارسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">متن فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('fa_content',null,array( "placeholder"=>"متن فارسی", "id"=>"ee", "class"=>" autosize  form-control","style"=>"overflow: hidden; word-wrap: break-word; resize: horizontal; height: 69px;"))}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="panel_tab2_example2">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">عنوان انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('en_title',null,array( "placeholder"=>"عنوان انگلیسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">متن انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('en_content',null,array( "placeholder"=>"متن انگلیسی", "id"=>"rr", "class"=>" autosize  form-control","style"=>"overflow: hidden; word-wrap: break-word; resize: horizontal; height: 69px;"))}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="panel_tab3_example2">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">عنوان روسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('ru_title',null,array( "placeholder"=>"عنوان روسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">متن روسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('ru_content',null,array( "placeholder"=>"متن روسی", "id"=>"aa", "class"=>" autosize  form-control","style"=>"overflow: hidden; word-wrap: break-word; resize: horizontal; height: 69px;"))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop
@section('script')
    @parent()
    {{HTML::script('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js')}}
    {{HTML::script('backend/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js')}}
    {{HTML::script('backend/assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js')}}
    <script type="text/javascript">
        $('.js-example-basic-multiple').select2();

    </script>
@stop()
