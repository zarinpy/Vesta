@extends("layout.admin_index")
@section('style')
    @parent
    {{HTML::style('//cdn.datatables.net/1.10.7/css/jquery.dataTables.css')}}
@stop()

@section('content')
    <div class="col-sm-9">
        @if(Session::has('success'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('success')}}
            </div>
        @endif()
        @if(Session::has('update'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('update')}}
            </div>
        @endif()
    </div>
    <table id="sample-table-1" class="table table-hover" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th class="center">نام</th>
            <th class="center">نام انگلیسی</th>
            <th class="center">نام فارسی</th>
            <th class="center">ویرایش</th>
            <th class="center">حذف</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th class="center">#</th>
            <th class="center">نام</th>
            <th class="center">نام انگلیسی</th>
            <th class="center">نام فارسی</th>
            <th class="center">ویرایش</th>
            <th class="center">حذف</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($pages as $m)
            <tr>
                <td class="center">{{$m->id}}</td>
                <td class="center">{{$m->fa_title}}</td>
                <td class="center ">{{$m->en_title}}</td>
                <td class="center "><a href="{{URL::route('admin.pages.edit',array($m->id))}}" class="btn btn-xs btn-blue" data-placement="top" data-original-title="ویرایش">ویرایش</a></td>
                <td class="center ">{{Form::open(array('url'=>'admin/pages/'.$m->id))}}
                    {{Form::token()}}
                    {{Form::hidden('_method','DELETE')}}
                    {{Form::submit(' حذف ',array('class'=>'btn btn-xs btn-bricky ','style'=>'font-family:b nazanin; ','onclick'=>"if(!confirm('آیا مایل به تغییر وضعیت این ردیف هستید؟')){return false;};"))}}
                    {{Form::close()}}</td>
            </tr>
        @endforeach()
        </tbody>
    </table>
@stop
@section('script')
    @parent()
    {{HTML::script('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}
    <script>
        $(document).ready(function() {
            $('#example').dataTable( {
                "scrollY": "800px",
                "scrollCollapse": true
            } );
        } );
    </script>
@stop()
