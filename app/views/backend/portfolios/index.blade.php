@extends("layout.admin_index")
@section('style')
    @parent
@stop()

@section('content')
    <div class="col-sm-9">
        @if(Session::has('success'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('success')}}
            </div>
        @endif()
        @if(Session::has('update'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('update')}}
            </div>
        @endif()
    </div>
    <table class="table table-hover" id="sample-table-1">
        <thead>
        <tr>
            <th class="center">#</th>
            <th class="center">تصویر</th>
            <th class="hidden-xs center">عنوان پروژه</th>
            <th class="center">توضیحات</th>
            <th class="hidden-xs center">عملیات</th>
            <th class="hidden-xs center">عملیات</th>
        </tr>
        </thead>
        <tbody>
        <?php $i=1 ?>
        @foreach($portfolios as $p)
        <tr>
            <td class="hidden-xs">{{$i}}</td>
            <td class="center">{{HTML::image($p->img1,$p->fa_title,array('width'=>'100','height'=>'50'))}}</td>
            <td class="center">{{$p->fa_title}}</td>
            <td class="center">{{str_limit($p->fa_body,50,'. . .')}}</td>
            <td class="center">
                <a href="{{URL::route('admin.portfolios.edit',array($p->id))}}" class="btn btn-xs btn-blue" data-placement="top" data-original-title="ویرایش">ویرایش</a>
            </td>
            <td class="center">
                {{Form::open(array('url'=>'admin/portfolios/'.$p->id))}}
                {{Form::token()}}
                {{Form::hidden('_method','DELETE')}}
                {{Form::submit(' حذف ',array('class'=>'btn btn-xs btn-bricky ','style'=>'font-family:b nazanin; ','onclick'=>"if(!confirm('آیا مایل به تغییر وضعیت این ردیف هستید؟')){return false;};"))}}
                {{Form::close()}}</td>
        </tr><?php $i++ ?>
            @endforeach
        </tbody>
    </table>
@stop

@section('script')
    @parent()
@stop()
