@extends("layout.admin_index")
@section('style')
    @parent
@stop()

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{ Form::model($teamcategory,array('route'=>array('admin.team_categories.update','id'=>$teamcategory->id),'method'=>'PUT','class'=>'form-horizontal','role'=>'form')) }}
                    {{Form::token()}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام سمت</label>
                        <div class="col-sm-9">
                            {{Form::text('fa_cat_title',null,array( "placeholder"=>"نام سمت", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام انگلیسی سمت</label>
                        <div class="col-sm-9">
                            {{Form::text('en_cat_title',null,array( "placeholder"=>"نام انگلیسی سمت", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام روسی سمت</label>
                        <div class="col-sm-9">
                            {{Form::text('ru_cat_title',null,array( "placeholder"=>"نام روسی سمت", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}

                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop

@section('script')
    @parent()
@stop()
