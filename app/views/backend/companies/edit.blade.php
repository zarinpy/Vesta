@extends("layout.admin_index")

@section('style')
    @parent
    {{HTML::style('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}
@stop()
@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{ Form::model($company,array('route'=>array('admin.companies.update','id'=>$company->id),'method'=>'PUT','class'=>'form-horizontal','role'=>'form','enctype'=>'multipart/form-data')) }}
                    {{Form::token()}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام همکار فارسی</label>
                        <div class="col-sm-9">
                            {{FOrm::text('fa_name',null,array( "placeholder"=>"ننام همکار فارسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام انگلیسی همکار</label>
                        <div class="col-sm-9">
                            {{FOrm::text('en_name',null,array( "placeholder"=>"نام انگلیسی همکار", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام همکار روسی</label>
                        <div class="col-sm-9">
                            {{FOrm::text('ru_name',null,array( "placeholder"=>"نام همکار روسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-2">آدرس وبسایت</label>
                        <div class="col-sm-9">
                            {{Form::text('website',null,array( "placeholder"=>"آدرس وبسایت", "id"=>"form-field-2", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-select-4">دسته بندی</label>
                        <div class="col-sm-9">
                            @foreach($category as $cat)
                                @if(in_array($cat->fa_name,$result_pcat))
                                    <input type="checkbox" value="{{$cat->id}}" {{"checked='checked'"}} name="category[]" title="دسته بندی" />
                                    {{Form::label($cat->fa_name,$cat->fa_name,array('class'=>'col-sm-2 control-label','for'=>'form-field-select-4'))}}
                                @else()
                                    <input type="checkbox" value="{{$cat->id}}" name="category[]" title="دسته بندی" />
                                    {{Form::label($cat->fa_name,$cat->fa_name,array('class'=>'col-sm-2 control-label','for'=>'form-field-select-4'))}}
                                @endif()
                            @endforeach()
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> تصویر عضو </label>
                        <div class="col-sm-9">
                            <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden" value="" name="">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt="">
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                                <div>
                                    <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> انتخاب تصویر</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> ویرایش تصویر</span>
                                        <input type="file" name="logo" title="logo" >
                                    </span>
                                    <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> حذف
                                    </a>
                                </div>
                            </div>
                            <div class="alert alert-warning">
                                <span class="label label-warning">توجه!</span>
                                <span> کاربر گرامی چنانچه تصویری انتخاب کنید تصویر قبلی حذف خواهد شد. </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop
@section('script')
    @parent()
    {{HTML::script('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js')}}
@stop()
