@extends("layout.admin_index")
@section('style')
    @parent
    {{HTML::style('backend/assets/plugins/DataTables/media/css/DT_bootstrap.css')}}
@stop()
@section('content')
    <div class="col-sm-9">
        @if(Session::has('success'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('success')}}
            </div>
        @endif()
            @if(Session::has('update'))
                <div class="alert alert-success ">
                    <button data-dismiss="alert" class="close">×</button>
                    <i class="fa fa-check-circle"></i>
                    {{Session::get('update')}}
                </div>
        @endif()
    </div>
    <div class="table-responsive">
        <table class="table table-hover" id="sample-table-1">
            <thead>
            <tr>
                <th class="center">نام</th>
                <th class="center">نام خانوادگی</th>
                <th class="center">پست الکترونیکی</th>
                <th class="center">گروه</th>
                <th class="center">فعال</th>
                <th class="center">عملیات</th>
                <th class="center">عملیات</th>
            </tr>
            </thead>
            <tbody>
                @foreach($users as $t)
            <tr>
                <td class="center">{{$t->name}}</td>
                <td class="center">{{$t->family}}</td>
                <td class="center">{{$t->email}}</td>
                <td class="center">
                    @foreach ($t->group as $gp)
                        {{$gp->fa_name}}
                    @endforeach()</td>
                <td class="center">@if($t->active == 1)
                    {{HTML::image('backend/assets/images/ok.png',null,array('width'=>25,'height'=>25));}}
                @else()
                        {{HTML::image('backend/assets/images/not.png',null,array('width'=>25,'height'=>25));}}
               @endif()
                </td>
                <td class="center">
                    <a href="{{URL::route('admin.users.edit',array($t->id))}}" class="btn btn-xs btn-blue" data-placement="top" data-original-title="ویرایش">ویرایش</a>
                </td>
                <td class="center">
                    {{Form::open(array('url'=>'admin/users/'.$t->id))}}
                    {{Form::token()}}
                    {{Form::hidden('_method','DELETE')}}
                    {{Form::submit(' حذف ',array('class'=>'btn btn-xs btn-bricky ','style'=>'font-family:b nazanin; ','onclick'=>"if(!confirm('آیا مایل به تغییر وضعیت این ردیف هستید؟')){return false;};"))}}
                    {{Form::close()}}</td>
            </tr>
                @endforeach()
            </tbody>
        </table>
    </div>
@stop
@section('script')
    @parent()
    {{HTML::script('backend/assets/js/table-export.js')}}
    {{HTML::script('backend/assets/plugins/bootbox/bootbox.min.js')}}
    {{HTML::script('backend/assets/plugins/select2/select2.min.js')}}
    {{HTML::script('backend/assets/plugins/jquery-mockjax/jquery.mockjax.js')}}
    {{HTML::script('backend/assets/plugins/DataTables/media/js/jquery.dataTables.min.js')}}
    {{HTML::script('backend/assets/plugins/DataTables/media/js/DT_bootstrap.js')}}
    {{HTML::script('backend/assets/js/table-data.js')}}
@stop()
