@extends("layout.admin_index")
@section('style')
    @parent

@stop()

@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
        @if(Session::has('email'))
            <div class="alert alert-warning ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                    {{Session::get('email')}}
            </div>
        @endif()
        @if(Session::has('password'))
            <div class="alert alert-warning ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('password')}}
            </div>
        @endif()
            @if(Session::has('user_exist'))
                <div class="alert alert-warning ">
                    <button data-dismiss="alert" class="close">×</button>
                    <i class="fa fa-check-circle"></i>
                    {{Session::get('user_exist')}}
                </div>
            @endif()
            @if(Session::has('group_not_found'))
                <div class="alert alert-warning ">
                    <button data-dismiss="alert" class="close">×</button>
                    <i class="fa fa-check-circle"></i>
                    {{Session::get('group_not_found')}}
                </div>
            @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{Form::open(array('route'=>'admin.users.store','method'=>'POST','class'=>'form-horizontal','role'=>'form','enctype'=>"multipart/form-data"))}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام و نام خانوادگی</label>
                        <div class="col-sm-3">
                            {{Form::input('text','name','',array( "placeholder"=>"نام ", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                        <div class="col-sm-3">
                            {{Form::input('text','family','',array( "placeholder"=>"نام خانوادگی", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-7">پست الکترونیکی</label>
                        <div class="col-sm-3">
                            {{Form::email('email','',array('placeholder'=>'آدرس پست الکترونیکی','id'=>'form-field-6','class'=>'form-control'))}}
                        </div>
                        <div class="col-sm-3">
                            <div class="checkbox">
                                <label class="checkbox-inline">فعال
                                    {{Form::checkbox('active',1,true,array('class'=>' form-control square-grey','checked'=>'checked'))}}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-7">گروه مشتریان</label>
                        <div class="col-sm-6">
                            <select name="group" class="form-control">
                                @foreach($group as $g)
                                    <option value="{{$g->id}}">{{$g->fa_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-7">رمز عبور</label>
                        <div class="col-sm-6">
                            {{Form::password('password',array('class'=>'form-control'))}}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop

@section('script')
    @parent()
@stop()
