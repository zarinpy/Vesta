@extends("layout.admin_index")
@section('style')
    @parent
@stop()
@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{ Form::model($user,array('route'=>array('admin.users.update','id'=>$user->id),'method'=>'PUT','class'=>'form-horizontal','role'=>'form')) }}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام</label>
                        <div class="col-sm-9">
                            {{FOrm::text('name',null,array( "placeholder"=>"نام ", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام خانوادگی</label>
                        <div class="col-sm-9">
                            {{FOrm::text('family',null,array( "placeholder"=>"نام خانوادگی", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">ایمیل</label>
                        <div class="col-sm-9">
                            {{FOrm::text('email',null,array( "placeholder"=>"ایمیل", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">فعال و غیرفعال </label>
                        <div class="col-sm-9">
                            <label class="checkbox-inline">فعال<?php if($user->active == 1){ $active = "checked='checked'" ;}
                                else{
                                    $active = "" ;
                                }?>
                                    <input type="checkbox" class=" form-control square-grey" name="active" value="1" {{$active}}>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop
@section('script')
    @parent()
@stop()