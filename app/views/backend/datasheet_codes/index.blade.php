@extends("layout.admin_index")
@section('style')
    @parent
@stop()
@section('content')
    <div class="col-sm-9">
        @if(Session::has('success'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">�</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('success')}}
            </div>
        @endif()
        @if(Session::has('update'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">�</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('update')}}
            </div>
        @endif()
    </div>
    <table class="table table-hover" id="sample-table-1">
        <thead>
        <tr>
            <th class="center">#</th>
            <th class="center">��</th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 1 ?>
        @foreach($datasheetcodes as $c)
            <tr>
                <td class="center">{{$i}}</td>
                <td class="center">{{$c->short}}</td>
                <td class="center">
                    {{Form::open(array('url'=>'admin/datasheet_codes/'.$c->id))}}
                    {{Form::token()}}
                    {{Form::hidden('_method','DELETE')}}
                    {{Form::submit(' ��� ',array('class'=>'btn btn-xs btn-bricky ','style'=>'font-family:b nazanin; ','onclick'=>"if(!confirm('�?� ��?� �� ��??� ���?� �?� ��?� ���?Ͽ')){return false;};"))}}
                    {{Form::close()}}
                </td>
                <td><a href="{{URL::route('admin.datasheet_codes.edit',array($c->id))}}" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit">�?��?�</a></td>
            </tr><?php $i++ ?>
        @endforeach()
        </tbody>
    </table>
@stop
@section('script')
    @parent()
@stop()