@extends("layout.admin_index")

@section('style')
    @parent
    {{HTML::style('backend/assets/plugins/datepicker/css/datepicker.css')}}
@stop()
@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
        @if(Session::has('waring'))
            <div class="alert alert-warning ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('waring')}}
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{Form::open(array('route'=>'admin.datasheet_codes.store','method'=>'POST','class'=>'form-horizontal','role'=>'form'))}}
                    {{Form::token()}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">کد</label>
                        <div class="col-sm-1">
                                {{Form::text('extra',null,array('class'=>'form-control','placeholder'=>' اضافی'))}}
                        </div>
                        <div class="col-sm-2">
                            <select name="type[]" class="form-control">
                                <option>نوع داده</option>
                                @foreach($type as $c)
                                    <option value="{{$c->id }}">{{$c->fa_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-sm-1">
                            <select name="day[]" class="form-control">
                                <option value="0">روز</option>
                                @foreach($day as $key=>$value)
                                    @if($key<10)
                                        <option value="0{{$key }}">0{{$value}}</option>
                                    @else
                                    <option value="{{$key }}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <select name="month[]" class="form-control">
                                <option value="0">ماه</option>
                                @foreach($month as $key=>$value)
                                    @if($key<10)
                                        <option value="0{{$key }}">0{{$value}}</option>
                                    @else
                                        <option value="{{$key }}">{{$value}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-1">
                            <select name="year[]" class="form-control">
                                <option value="0">سال</option>
                                @foreach($year as $key=>$value)
                                    <option value="{{$key }}">{{$value}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <select name="company[]" class="form-control">

                                <option value="0"> شرکت</option>
                                @foreach($company as $c)
                                    <option value="{{$c->id }}">{{$c->fa_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <br>
                        <br>
                        <br>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                        </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    @parent()
    <script type="javascript">
        $('.form-group input[type=text]').datepicker({
            format: "ddyyyymm"
        });
    </script>
@stop()