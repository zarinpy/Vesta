@extends("layout.admin_index")
@section('style')
    @parent
    {{HTML::style('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}
@stop()
@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
        @if(Session::has('img'))
            <div class="alert alert-warning ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('img')}}
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{ Form::model($slider,array('route'=>array('admin.sliders.update','id'=>$slider->id),'method'=>'PUT','files'=>true,'class'=>'form-horizontal','role'=>'form','enctype'=>'multipart/form-data')) }}
                    <div class="tabbable">
                        <ul id="myTab" class="nav nav-tabs tab-bricky">
                            <li class="active">
                                <a href="#panel_tab2_example1" data-toggle="tab">
                                    <i class="green fa"></i> اطلاعات فارسی
                                </a>
                            </li>
                            <li class="">
                                <a href="#panel_tab2_example2" data-toggle="tab">اطلاعات انگلیسی</a>
                            </li>
                            <li class="">
                                <a href="#panel_tab3_example2" data-toggle="tab">اطلاعات روسی</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="panel_tab2_example1">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">متن فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('fa_slug1',null,array( "placeholder"=>"متن فارسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">متن دوم فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('fa_slug2',null,array( "placeholder"=>"متن دوم فارسی", "id"=>"rr", "class"=>"autosize  form-control","style"=>"overflow: hidden; word-wrap: break-word; resize: horizontal; height: 69px;"))}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="panel_tab2_example2">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">متن انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('en_slug1',null,array( "placeholder"=>"متن انگلیسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">متن دوم انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('en_slug2',null,array( "placeholder"=>"متن دوم انگلیسی", "id"=>"ee", "class"=>"autosize  form-control","style"=>"overflow: hidden; word-wrap: break-word; resize: horizontal; height: 69px;"))}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="panel_tab3_example2">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">متن روسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('ru_slug1',null,array( "placeholder"=>"متن روسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">متن دوم روسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('ru_slug2',null,array( "placeholder"=>"متن دوم روسی", "id"=>"aa", "class"=>"autosize  form-control","style"=>"overflow: hidden; word-wrap: break-word; resize: horizontal; height: 69px;"))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">انیمیشن</label>
                        <div class="col-sm-9">
                            {{Form::select('animate',array('papercut'=>'papercut','3dcurtain-horizontal'=>'3dcurtain-horizontal','scaledownfromleft'=>'scaledownfromleft'),$slider->animate,array('class'=>'form-control','id'=>''))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> تصویر عضو </label>
                        <div class="col-sm-3">
                            <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden" value="" name="">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image" alt="">
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                                <div>
                                        <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> انتخاب تصویر</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> ویرایش تصویر</span>
                                            <input type="file" name="file" title="file">
                                        </span>
                                    <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> حذف
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop
@section('script')
    @parent()
    {{HTML::script('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js')}}
@stop()
