@extends("layout.admin_index")

@section('style')
    @parent
    {{HTML::style('//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css')}}
    {{HTML::script('backend/assets/plugins/ckeditor/ckeditor.js')}}
@stop()
@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
        @if(Session::has('file'))
            <div class="alert alert-danger ">
            <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-bug"></i>
                {{Session::get('file')}}<br/>
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{Form::open(array('route'=>'admin.datasheets.store','method'=>'POST','class'=>'form-horizontal','enctype'=>"multipart/form-data",'role'=>'form'))}}
                    <div class="tabbable">
                        <ul id="myTab" class="nav nav-tabs tab-bricky">
                            <li class="active">
                                <a href="#panel_tab2_example1" data-toggle="tab">
                                    <i class="green fa"></i> اطلاعات فارسی
                                </a>
                            </li>
                            <li class="">
                                <a href="#panel_tab2_example2" data-toggle="tab">اطلاعات انگلیسی</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="panel_tab2_example1">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">نام فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('fa_name','',array( "placeholder"=>"نام فارسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">متن فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('fa_description','',array( "placeholder"=>"متن فارسی", "id"=>"ee", "class"=>" form-control"))}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="panel_tab2_example2">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">نام انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('en_name','',array( "placeholder"=>"نام انگلیسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">متن انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('en_description','',array( "placeholder"=>"متن انگلیسی", "id"=>"dd", "class"=>" form-control"))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-select-4">کد مورد نظر</label>
                        <div class="col-sm-9">
                            <select name="code[]" class="form-control js-example-basic-multiple" id="form-field-select-4" multiple="multiple">
                                @foreach($code as $cat)
                                    <option value="{{$cat->id}}">{{$cat->short}}</option>
                                @endforeach()
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-select-4">گروه</label>
                        <div class="col-sm-9">
                            <select name="group[]" class="form-control js-example-basic-multiple" id="form-field-select-4" multiple="multiple">
                                @foreach($gp as $g)
                                    <option value="{{$g->id}}">{{$g->fa_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">فایل</label>
                        {{Form::file('file')}}
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop
@section('script')
    @parent()
    {{HTML::script('backend/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}
    {{HTML::script('//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js')}}
    {{HTML::script('backend/assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js')}}
    <script type="text/javascript">
        $('.js-example-basic-multiple').select2();
        CKEDITOR.replace('ee');
        CKEDITOR.replace('dd');
    </script>
@stop()
