@extends("layout.admin_index")
@section('style')
    @parent
    {{HTML::style('//cdn.datatables.net/1.10.7/css/jquery.dataTables.css')}}
@stop()

@section('content')
    <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th class="center">Name</th>
            <th class="center">Position</th>
            <th class="center">Office</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th class="center">Name</th>
            <th class="center">Position</th>
            <th class="center">Office</th>
        </tr>
        </tfoot>

        <tbody>
            @foreach($medias as $m)
        <tr>
            <td class="center"><?php
            switch($m->category){
                case '11':
                   echo HTML::image($m->address,$m->m_title,array('width'=>'80','height'=>'80'));
                    break;
                case '21':
                    echo HTML::image('/backend/assets/images/pdf.png',$m->m_title,array('width'=>'80','height'=>'80'));
                    break;
                case '31':
                    echo HTML::image('/backend/assets/images/rar.png',$m->m_title,array('width'=>'80','height'=>'80'));
                    break;
                case '41':
                    echo  HTML::image('/backend/assets/images/word.png',$m->m_title,array('width'=>'80','height'=>'80'));
                    break;
                case '51':
                    echo HTML::image('/backend/assets/images/excel.png',$m->m_title,array('width'=>'80','height'=>'80'));
                    break;
                case '61':
                    echo HTML::image('/backend/assets/images/epan.png',$m->m_title,array('width'=>'80','height'=>'80'));
                    break;
                 case '71':
                     echo HTML::image('/backend/assets/images/other.png',$m->m_title,array('width'=>'80','height'=>'80'));
                    break;

                    default:
                        echo "";

            }
            ?></td>
            <td class="center "><b>{{$m->m_title}}</b></td>
            <td class="center "><b>{{HTML::link($m->address,$m->address)}}</b></td>
        </tr>
            @endforeach()
        </tbody>
    </table>
@stop
@section('script')
    @parent()
    {{HTML::script('//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js')}}
    <script>
        $(document).ready(function() {
            $('#example').dataTable( {
                "scrollY":        "800px",
                "scrollCollapse": true
            } );
        } );
    </script>
@stop()
