@extends("layout.admin_index")
@section('style')
    @parent
@stop()
@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
    </div>
    <div class="col-sm-9">
        @if(Session::has('success'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('success')}}
            </div>
        @endif()
            @if(Session::has('warning'))
            <div class="alert alert-warning">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('warning')}}
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{Form::open(array('route'=>'admin.medias.store','method'=>'POST','id'=>'my-awesome-dropzone','class'=>'dropzone form-horizontal','role'=>'form','enctype'=>"multipart/form-data"))}}
                    {{Form::token()}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-2">دسته بندی</label>
                        <div class="col-sm-9">
                            {{Form::select('category',array(11=>'Image',21=>'PDF',31=>'ZIP-RAR',41=>'DOC',51=>'EXCEL',61=>'EPLAN',71=>'Other'),null,array('class'=>'form-control'))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-2">فایل شماره یک</label>
                        <div class="col-sm-3">
                            {{Form::file('file1')}}
                        </div>
                        <div class="col-sm-3">
                            {{Form::text('title',null,array('placeholder'=>'عنوان فایل یک'))}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop

@section('script')
    @parent()

@stop()
