@extends("layout.admin_index")
@section('style')
    @parent
    {{HTML::style('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}
@stop()

@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{ Form::model($portfoliocustomer,array('route'=>array('admin.portfolio_customers.update','id'=>$portfoliocustomer->id),'method'=>'PUT','class'=>'form-horizontal','role'=>'form','enctype'=>'multipart/form-data')) }}
                    {{Form::token()}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام مشتری فارسی</label>
                        <div class="col-sm-9">
                            {{FOrm::text('fa_name',null,array( "placeholder"=>"نام مشتری فارسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام انگلیسی مشتری</label>
                        <div class="col-sm-9">
                            {{FOrm::text('en_name',null,array( "placeholder"=>"نام انگلیسی همکار", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">نام مشتری روسی</label>
                        <div class="col-sm-9">
                            {{FOrm::text('ru_name',null,array( "placeholder"=>"نام مشتری روسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop

@section('script')
    @parent()
    {{HTML::script('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js')}}
@stop()
