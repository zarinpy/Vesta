@extends("layout.admin_index")
@section('style')
    @parent
@stop()
@section('content')
    <div class="col-sm-9">
        @if(Session::has('success'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('success')}}
            </div>
        @endif()
        @if(Session::has('update'))
            <div class="alert alert-success ">
                <button data-dismiss="alert" class="close">×</button>
                <i class="fa fa-check-circle"></i>
                {{Session::get('update')}}
            </div>
        @endif()
    </div>
    <table class="table table-hover" id="sample-table-1">
        <thead>
        <tr>
            <th class="center">#</th>
            <th class="center">نام همکار</th>
            <th class="center">نام همکار</th>
            <th class="hidden-xs center">عملیات</th>
            <th class="hidden-xs center">عملیات</th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 1 ?>
        @foreach($portfoliocustomers as $c)
            <tr>
                <td class="center">{{$i}}</td>
                <td class="center">
                    <a href="http://{{$c->fa_name}}" rel="nofollow" target="_blank">{{$c->website}}</a>
                </td>
                <td class="hidden-xs center">{{$c->fa_name}}</td>
                <td class="hidden-xs center">{{$c->en_name}}</td>
                <td class="center">
                    {{Form::open(array('url'=>'admin/portfolio_customers/'.$c->id))}}
                    {{Form::token()}}
                    {{Form::hidden('_method','DELETE')}}
                    {{Form::submit(' حذف ',array('class'=>'btn btn-xs btn-bricky ','style'=>'font-family:b nazanin; ','onclick'=>"if(!confirm('آیا مایل به تغییر وضعیت این ردیف هستید؟')){return false;};"))}}
                    {{Form::close()}}
                </td>
                <td class="center"><a href="{{URL::route('admin.portfolio_customers.edit',array($c->id))}}" class="btn btn-xs btn-teal tooltips" data-placement="top" data-original-title="Edit">ویرایش</a></td>
            </tr><?php $i++ ?>
        @endforeach()
        </tbody>
    </table>
@stop
@section('script')
    @parent()
@stop()
