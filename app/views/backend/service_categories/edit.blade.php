@extends("layout.admin_index")
@section('style')
    @parent
    {{HTML::style('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}@stop()
@section('content')
    <div class="col-sm-9">
        @if($errors->has())
            <div class="alert alert-danger ">
                <button data-dismiss="alert" class="close">×</button>
                @foreach($errors->all() as $errors)
                    <i class="fa fa-bug"></i>
                    {{$errors}}<br/>
                @endforeach()
            </div>
        @endif()
    </div>
    <div class="row">
        <div class="col-sm-12">
            <!-- start: TEXT FIELDS PANEL -->
            <div class="panel panel-default">
                <div class="panel-body">
                    {{ Form::model($servicecategory,array('route'=>array('admin.service_categories.update','id'=>$servicecategory->id),'method'=>'PUT','class'=>'form-horizontal','role'=>'form','enctype'=>"multipart/form-data")) }}
                    {{Form::token()}}
                    <div class="tabbable">
                        <ul id="myTab" class="nav nav-tabs tab-bricky">
                            <li class="active">
                                <a href="#panel_tab2_example1" data-toggle="tab">
                                    <i class="green fa"></i> اطلاعات فارسی
                                </a>
                            </li>
                            <li class="">
                                <a href="#panel_tab2_example2" data-toggle="tab">اطلاعات انگلیسی</a>
                            </li>
                            <li class="">
                                <a href="#panel_tab3_example2" data-toggle="tab">اطلاعات روسی</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="panel_tab2_example1">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">عنوان فارسی دسته بندی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('fa_title',null,array( "placeholder"=>"عنوان فارسی دسته بندی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">توضیحات فارسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('fa_description',null,array( "placeholder"=>"توضیحات فارسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="panel_tab2_example2">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">عنوان انگلیسی دسته بندی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('en_title',null,array( "placeholder"=>"عنوان انگلیسی دسته بندی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">توضیحات انگلیسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('en_description',null,array( "placeholder"=>"توضیحات انگلیسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="panel_tab3_example2">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">عنوان روسی دسته بندی</label>
                                    <div class="col-sm-9">
                                        {{Form::text('ru_title',null,array( "placeholder"=>"عنوان روسی دسته بندی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="form-field-1">توضیحات روسی</label>
                                    <div class="col-sm-9">
                                        {{Form::textarea('ru_description',null,array( "placeholder"=>"توضیحات روسی", "id"=>"form-field-1", "class"=>"form-control"))}}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"> تصویر عضو </label>
                        <div class="col-sm-9">
                            <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden" value="" name="">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;"><img src="{{url($servicecategory->img)}}" alt="">
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                                <div>
                                    <span class="btn btn-light-grey btn-file"><span class="fileupload-new"><i class="fa fa-picture-o"></i> انتخاب تصویر</span><span class="fileupload-exists"><i class="fa fa-picture-o"></i> ویرایش تصویر</span>
                                        <input type="file" name="file" title="file">
                                    </span>
                                    <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                        <i class="fa fa-times"></i> حذف
                                    </a>
                                </div>
                            </div>
                            <div class="alert alert-warning">
                                <span class="label label-warning">تذکر</span>
                                <span>اندازه تصویر را 150x150 انتخاب کنید</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label class="col-sm-2 control-label"></label>
                        {{Form::submit('ذخیره اطلاعات',array('class'=>'btn btn-primary'))}}
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>
    </div>
@stop
@section('script')
    @parent()
    {{HTML::script('backend/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js')}}
@stop()