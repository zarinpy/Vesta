<?php $la = App::getLocale();?>
@extends('layout.site_layout')
@section('head')
    <title>@lang('routes.service')::{{$cat->{$la.'_title'} }}</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="{{$cat->{$la.'_title'} }}" />
    <meta name="description" content="{{$cat->{$la.'_title'} }}">
    <meta name="author" content="omid zarinmahd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="@lang('messages.company_name')">
    <meta property="og:site_name" content="@lang('messages.company_name')">
    <meta property="og:locale" content="{{$la}}" />
    @foreach(Config::get('app.alt_langs') as $c)
        <meta property="og:locale:alternate" content="{{$c}}" />
    @endforeach
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:description" content="">
    <meta property="fb:app_id" content="">
    <meta property="og:type" content="article">
@stop
@section('content')
    <div class="top-title-area bg-img">
        <div class="container">
            <h1 class="title-page">{{$cat->{$la.'_title'} }}</h1>
            <div class="text" style="color: #fff8f8;">
                {{$cat->{$la.'_description'} }}
            </div>
        </div>
    </div>
    <div class="gap"></div>
    <div class="container">
    <ul class="list list-inline">
        @foreach($all_cat as $ac)
        <li>
            <a style="@if($ac->{$la.'_title'} == $cat->{$la.'_title'}) color:#000033;  @endif()" href="/{{$la}}/@lang('routes.service')/{{$ac->{$la.'_title'} }}">{{$ac->{$la.'_title'} }}</a>
        </li>
        @endforeach
    </ul>
    </div>
    <div class="gap"></div>
    <div class="container">
            <div>
            @foreach($service as $s)
            <div class="row">
                <li class=" list list-block">
                    <div class="thumb">
                        @if($la == 'fa')
                            <div class="span9">
                                <div class="thumb-caption">
                                    <h5 class="thumb-title"><a href="/{{$la}}/@lang('routes.service')/{{$cat->{$la.'_title'} }}/{{$s->{$la.'_title'} }}">{{$s->{$la.'_title'} }}</a></h5>
                                    <p class="thumb-desc">{{str_limit($s->{$la.'_body'},250,'...') }}</p>
                                </div>
                            </div>
                            <div class="span3">
                                <div class="thumb-header-right">
                                    <a href="/{{$la}}/@lang('routes.service')/{{$cat->{$la.'_title'} }}/{{$s->{$la.'_title'} }}">
                                        <img src="{{url($s->img)}}" alt="Image Alternative text" title="{{$s->{$la.'_title'} }}">
                                    </a>
                                </div>
                            </div>
                        @else()
                            <div class="span3">
                                <div class="thumb-header-right">
                                    <a href="#">
                                        <img src="{{url($s->img)}}" alt="Image Alternative text" title="{{$s->{$la.'_title'} }}">
                                    </a>
                                </div>
                            </div>
                            <div class="span9">
                                <div class="thumb-caption">
                                    <h5 class="thumb-title"><a href="/{{$la}}/@lang('routes.service')/{{$cat->{$la.'_title'} }}/{{$s->{$la.'_title'} }}">{{$s->{$la.'_title'} }}</a></h5>
                                    <p class="thumb-desc">{{$s->{$la.'_body'} }}</p>
                                </div>
                            </div>
                        @endif()
                    </div>
                </li>
            </div>
            <div class="gap-small"></div>
            <hr>
            <div class="gap-small"></div>
            @endforeach
            </div>
        <div class="gap"></div>
    </div>
@stop