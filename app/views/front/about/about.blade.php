<?php
$la = App::getLocale(); $team = Page::find(14);
$description = Setting::where('key','=',$la.'_description')->get()->toArray();
$keywords = Setting::where('key','=',$la.'_keywords')->get()->toArray();
?>
@extends('layout.site_layout')
@section('head')
    <title>@lang('messages.company_name')::@lang('routes.about')</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="omid zarinmahd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="@lang('messages.company_name')">
    <meta property="og:site_name" content="@lang('messages.company_name')">
    <meta property="og:locale" content="{{$la}}" />
    @foreach(Config::get('app.alt_langs') as $c)
        <meta property="og:locale:alternate" content="{{$c}}" />
    @endforeach
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:description" content="{{Page::find(13)->{$la.'_content'} }}">
    <meta property="fb:app_id" content="">
    <meta property="og:type" content="article">
@stop
@section('content')
    <div class="top-title-area bg-img">
        <div class="container">
            <h1 class="title-page">@lang('menu.about')</h1>
        </div>
    </div>
    <div class="gap"></div>
    <div class="container">
        @if($la=='fa')
            <div class="row">
                <div class="span9">
                    <!-- START BOOTSTRAP CAROUSEL -->
                    <div id="my-carousel" class="carousel slide">
                        <div class="carousel-inner">
                            <div class="active item">
                                <img src="https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=800&h=400&txttrack=0" alt="Image Alternative text" title="Workspace">
                            </div>
                            <div class="item">
                                <img src="https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=800&h=400&txttrack=0" alt="Image Alternative text" title="Street">
                            </div>
                            <div class="item">
                                <img src="https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=800&h=400&txttrack=0" alt="Image Alternative text" title="Branding  iPad Interactive Design">
                            </div>
                        </div>
                        <a class="carousel-control left" href="#my-carousel" data-slide="prev"></a>
                        <a class="carousel-control right" href="#my-carousel" data-slide="next"></a>
                    </div>
                    <!-- END BOOTSTRAP CAROUSEL -->
                </div>
                <div class="span3">
                    <h2>@lang('messages.the_company')</h2>
                    <p>{{Page::find(13)->{$la.'_content'} }}</p>
                </div>
            </div>
            <div class="gap"></div>
            <div class="row">
                <div class="span9">
                    <div class="fluid-width-video-wrapper" style="padding-top: 50%;"><iframe src="//www.youtube.com/embed/dmyrCLJGTtE" frameborder="0" allowfullscreen="" id="fitvid118232"></iframe></div>
                </div>
                <div class="span3">
                    <h2>@lang('menu.vision')</h2>
                    <p>{{Page::find(11)->{$la.'_content'} }}</p>
                </div>
            </div>
            <div class="gap"></div>
            <div class="row">
                <div class="span9 ">
                    <div class="fredsel-wrapper ">
                        <div class="row ">
                            <div class="caroufredsel_wrapper" style="display: block; text-align: right; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 900px; height: 259px; margin: 0px; overflow: hidden;"><ul class="list fredsel" id="fredsel" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 5700px; height: 259px;">
                                    @foreach($teams as $t)
                                        <li class="span3" style="">
                                            <div class="thumb center">
                                                <div class="thumb-header">
                                                    <div class="hover-img">
                                                        <img src="{{url($t->image)}}" alt="{{$t->{$la.'_name'} }} {{$t->{$la.'_family'} }}" title="{{$t->{$la.'_name'} }}">
                                                        <ul class="list list-inline hover-icon-group">
                                                            <li><a class="icon-facebook box-icon" href="http://{{$team->facebook}}" target="_blank"></a></li>
                                                            <li><a class="icon-linkedin box-icon" href="http://{{$team->linkedin}} " target="_blank"></a></li>
                                                            <li><a class="icon-google-plus box-icon" href="http://{{$team->google}}" target="_blank"></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="thumb-caption">
                                                    <h5 class="thumb-title"><a href="/{{$la}}/@lang('routes.team')/{{ $t->{$la.'_family'} }}">{{ $t->{$la.'_name'} }} {{$t->{$la.'_family'} }} </a></h5>
                                                    <p class="thumb-meta">{{$t->{$la.'_prof_title'} }}</p>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach()
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <h3>{{Page::find(14)->{$la.'_title'} }}</h3>
                    <p>{{Page::find(14)->{$la.'_content'} }}</p>
                    <a id="fredsel-prev" class="fredsel-prev box-icon icon-chevron-left" href="#" style="display: inline-block;"></a>
                    <a id="fredsel-next" class="fredsel-next box-icon icon-chevron-right" href="#" style="display: inline-block;"></a>
                </div>
            </div>
        @else()
            <div class="row">
                <div class="span3">
                    <h2>@lang('messages.the_company')</h2>
                    <p>{{Page::find(13)->{$la.'_content'} }}</p>
                </div>
                <div class="span9">
                    <!-- START BOOTSTRAP CAROUSEL -->
                    <div id="my-carousel" class="carousel slide">
                        <div class="carousel-inner">
                            <div class="active item">
                                <img src="https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=800&h=400&txttrack=0" alt="Image Alternative text" title="Workspace">
                            </div>
                            <div class="item">
                                <img src="https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=800&h=400&txttrack=0" alt="Image Alternative text" title="Street">
                            </div>
                            <div class="item">
                                <img src="https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=800&h=400&txttrack=0" alt="Image Alternative text" title="Branding  iPad Interactive Design">
                            </div>
                        </div>
                        <a class="carousel-control left" href="#my-carousel" data-slide="prev"></a>
                        <a class="carousel-control right" href="#my-carousel" data-slide="next"></a>
                    </div>
                    <!-- END BOOTSTRAP CAROUSEL -->
                </div>
            </div>
            <div class="gap"></div>
            <div class="row">
                <div class="span3">
                    <h2>@lang('menu.vision')</h2>
                    <p>{{Page::find(11)->{$la.'_content'} }}</p>
                </div>
                <div class="span9">
                    <div class="fluid-width-video-wrapper" style="padding-top: 50%;"><iframe src="//www.youtube.com/embed/dmyrCLJGTtE" frameborder="0" allowfullscreen="" id="fitvid118232"></iframe></div>
                </div>
            </div>
            <div class="gap"></div>
            <div class="row">
                <div class="span3">
                    <h3>{{$team->{$la.'_title'} }}</h3>
                    <p>{{$team->{$la.'_content'} }}</p>
                    <a id="fredsel-prev" class="fredsel-prev box-icon icon-chevron-left" href="#" style="display: inline-block;"></a>
                    <a id="fredsel-next" class="fredsel-next box-icon icon-chevron-right" href="#" style="display: inline-block;"></a>
                </div>
                <div class="span9 ">
                    <div class="fredsel-wrapper ">
                        <div class="row ">
                            <div class="caroufredsel_wrapper" style="display: block; text-align: right; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 900px; height: 259px; margin: 0px; overflow: hidden;"><ul class="list fredsel" id="fredsel" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 5700px; height: 259px;">
                                    @foreach($teams as $t)
                                        <li class="span3" style="">
                                            <div class="thumb center">
                                                <div class="thumb-header">
                                                    <div class="hover-img">
                                                        <img src="{{url($t->image)}}" alt="{{$t->{$la.'_name'} }} {{$t->{$la.'_family'} }}" title="{{$t->{$la.'_name'} }}">
                                                        <ul class="list list-inline hover-icon-group">
                                                            <li>
                                                                <a class="icon-facebook box-icon" href="http://{{$team->facebook}}" target="_blank"></a>
                                                            </li>
                                                            <li>
                                                                <a class="icon-linkedin box-icon" href="http://{{$team->linkedin}} " target="_blank"></a>
                                                            </li>
                                                            <li>
                                                                <a class="icon-google-plus box-icon" href="http://{{$team->google}}" target="_blank"></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="thumb-caption">
                                                    <h5 class="thumb-title">
                                                        <a href="/{{$la}}/@lang('routes.team')/{{ $t->{$la.'_family'} }}">{{ $t->{$la.'_name'} }} {{$t->{$la.'_family'} }} </a>
                                                    </h5>
                                                    <p class="thumb-meta">{{$t->{$la.'_prof_title'} }}</p>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach()
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gap"></div>
            </div>
        @endif()
        <div class="gap"></div>
    </div>
@stop()