<?php $la = App::getLocale();?>
@extends('layout.site_layout')
@section('head')
    <title>@lang('routes.product')</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="" />
    <meta name="description" content="{{$product->{$la.'_title'} }}">
    <meta name="author" content="omid zarinmahd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="@lang('messages.company_name')">
    <meta property="og:site_name" content="@lang('messages.company_name')">
    <meta property="og:locale" content="{{$la}}" />
    @foreach(Config::get('app.alt_langs') as $c)
        <meta property="og:locale:alternate" content="{{$c}}" />
    @endforeach
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:description" content="{{$product->{$la.'_title'} }}">
    <meta property="fb:app_id" content="">
    <meta property="og:type" content="my_namespace:business.industrial_automation">
@stop
@section('content')
    <div class="top-title-area bg-img">
        <div class="container">
            <h1 class="title-page">{{$product->{$la.'_title'} }}</h1>
        </div>
    </div>
    <div class="gap"></div>
    <div class="container">
        <div class="thumb">
            <div class="thumb-header">
                <!-- START BOOTSTRAP CAROUSEL -->
                <div id="blog-slider" class="carousel slide carousel-controls-right">
                    <div class="carousel-inner">
                        <div class="active item">
                            <img src="{{url($product->img)}}" alt="Image Alternative text" title="{{$product->{$la.'_title'} }}">
                        </div>
                    </div>
                    {{--<a class="carousel-control left" href="#blog-slider" data-slide="prev"></a>
                    <a class="carousel-control right" href="#blog-slider" data-slide="next"></a>--}}
                </div>
                <!-- END BOOTSTRAP CAROUSEL -->
            </div>
            <div class="thumb-caption">
                <h4 class="thumb-title"><a href="#">{{$product->{$la.'_title'} }}</a></h4>
            </div>
        </div>
        <div class="gap gap-mini"></div>
        {{$product->{$la.'_description'} }}
        <div class="gap"></div>

    </div>
@stop