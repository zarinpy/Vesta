<?php $la = App::getLocale();?>
@extends('layout.site_layout')
@section('head')
    <title>@lang('routes.product')</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="omid zarinmahd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="@lang('messages.company_name')">
    <meta property="og:site_name" content="@lang('messages.company_name')">
    <meta property="og:locale" content="{{$la}}" />
    @foreach(Config::get('app.alt_langs') as $c)
        <meta property="og:locale:alternate" content="{{$c}}" />
    @endforeach
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:description" content="">
    <meta property="fb:app_id" content="">
    <meta property="og:type" content="my_namespace:business.industrial_automation">
@stop()
@section('content')
    <div class="top-title-area bg-img">
        <div class="container">
            <h1 class="title-page">@lang('menu.product')</h1>
        </div>
    </div>
    <div class="gap"></div>
    <div class="container">
        <ul class="nav nav-pills @if($la == 'fa') pull-right @endif()">
            @if($la == 'fa')
                @foreach($p_cat as $pc)
                    <li class="mix-filter " data-filter="mix-cat-{{$pc->id}}"><a>{{ $pc->{$la.'_title'} }}</a></li>
                @endforeach()
                <li class="mix-filter active" data-filter="all"><a>@lang('messages.all')</a></li>
            @else()
                <li class="mix-filter active" data-filter="all"><a>@lang('messages.all')</a></li>
                @foreach($p_cat as $pc)
                    <li class="mix-filter" data-filter="mix-cat-{{$pc->id}}"><a>{{ $pc->{$la.'_title'} }}</a></li>
                @endforeach()
            @endif()
        </ul>
        <div class="row"></div>
        <!-- START MIXIUP FILTER -->
        <div id="mixitup-grid" class="mixitup-grid row" style="  transform:   ">
            @foreach($p_list as $p)
                <div class="mix @foreach($p->category as $c) mix-cat-{{$c->id}} @endforeach() span3 mix_all" style=" ">
                    <div class="thumb center">
                        <div class="thumb-header">
                            <div class="hover-img">
                                <img src="{{url($p->img)}}" alt="Image Alternative text" title="{{$p->{$la.'_title'} }}">
                                <ul class="list list-inline hover-icon-group">
                                    <li>
                                        <a class="popup-image icon-search box-icon-gray box-icon-to-inverse round animate-icon-top-to-bottom" href="{{url($p->img1) }}" data-effect="zoom-out"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="thumb-caption">
                            <h5 class="thumb-title"><a href="/{{$la}}/@lang('routes.product')/{{$p->{$la.'_title'} }}">{{$p->{$la.'_title'} }}</a></h5>
                            <p class="thumb-meta">@foreach($p->category as $c) {{$c->{$la.'_title'} }}/ @endforeach</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <!-- END MIXIUP FILTER -->
        <div class="gap"></div>
    </div>
@stop()