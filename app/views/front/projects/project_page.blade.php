<?php $la = App::getLocale();
?>
@extends('layout.site_layout')
@section('head')
    <title>@lang('menu.project')</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="" />
    <meta name="description" content="{{ $project->{$la.'_title'} }}">
    <meta name="author" content="omid zarinmahd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="@lang('messages.company_name')">
    <meta property="og:locale" content="{{$la}}" />
    @foreach(Config::get('app.alt_langs') as $c)
        <meta property="og:locale:alternate" content="{{$c}}" />
    @endforeach
    <meta property="og:site_name" content="@lang('messages.company_name')">
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:description" content="@lang('menu.project')">
    <meta property="fb:app_id" content="">
    <meta property="og:type" content="my_namespace:business.industrial_automation">
@stop()
@section('content')
    <div class="top-title-area bg-img">
        <div class="container">
            <h1 class="title-page">{{ $project->{$la.'_title'} }}</h1>
        </div>
    </div>
    <div class="gap"></div>
    <div class="container">
        <div class="row">
            <div class="span8">
                <!-- START BOOTSTRAP CAROUSEL -->
                <div id="my-carousel" class="carousel slide">
                    <div class="carousel-inner">
                        <div class="active item">
                            <img src="{{url($project->img1)}}" alt="Image Alternative text" title="{{ $project->{$la.'_title'} }}">
                        </div>
                        <div class="item">
                            <img src="{{url($project->img2)}}" alt="Image Alternative text" title="{{ $project->{$la.'_title'} }}">
                        </div>
                        <div class="item">
                            <img src="{{url($project->img3)}}" alt="Image Alternative text" title="{{ $project->{$la.'_title'} }}">
                        </div>
                    </div>
                    <a class="carousel-control left" href="#my-carousel" data-slide="prev"></a>
                    <a class="carousel-control right" href="#my-carousel" data-slide="next"></a>
                </div>
                <!-- END BOOTSTRAP CAROUSEL -->
            </div>
            <div class="span4">
                <h3>{{ $project->{$la.'_title'} }}</h3>
                <h5>@lang('messages.description')</h5>
                {{$project->{$la.'_body'} }}
                <div class="gap-small"></div>
                <ul class="list">
                    <li>@lang('messages.customers') : @foreach($project->customer as $c)  {{$c->{$la.'_name'} }}@endforeach <i class="icon-user icon-2x"></i></li>
                    <div class="gap-small"></div>
                    <li>@lang('messages.date') : {{date('Y-m-d',strtotime($project->created_at))}} <i class="icon-time icon-2x"></i></li>
                </ul>
            </div>
        </div>
        <div class="gap"></div>
    </div>
@stop()
