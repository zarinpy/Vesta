<?php
$la = App::getLocale();
$description = Setting::where('key','=',$la.'_description')->get()->toArray();
$keywords = Setting::where('key','=',$la.'_keywords')->get()->toArray();
$slug = Setting::where('key','=',$la.'_slug')->get()->toArray();
?>
@extends('layout.site_layout')
@section('head')
    <title>@lang('messages.company_name')</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="{{$keywords[0]['value']}}" />
    <meta name="description" content="{{$description[0]['value']}}">
    <meta name="author" content="omid zarinmahd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="@lang('messages.company_name')">
    <meta property="og:locale" content="{{$la}}" />
    @foreach(Config::get('app.alt_langs') as $c)
    <meta property="og:locale:alternate" content="{{$c}}" />
    @endforeach
    <meta property="og:site_name" content="vestatavan">
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:description" content="{{$description[0]['value']}}">
    <meta property="fb:app_id" content="">
    <meta property="og:type" content="business.business">
@stop
@section("content")
    <div class="top-area shadow-inner show-onload">
        <div class="top-bg-area bg-mask" data-bg-img="{{url('front')}}/img/bg/business-people-blur.jpg">
            <div class="earth-top to-bottom mask-front">
                <div class="earth-rotate flare">
                    <img src='{{url('front')}}/img/earth-models/render/earth-{{$la}}.png' alt="Image Alternative text" title="Image Title" />
                </div>
            </div>
            <div class="bg-mask-front bg-mask-darken full">
                <div class="vertical-center mask-front white">
                    <div class="container">
                        <div class="row">
                            <div class="span8 offset2 center">
                                <h1 class="super-hero-title">{{$slug[0]['value']}}</h1>
                                <p class="big-text">Aliquam ultricies adipiscing parturient class tempor suspendisse facilisis molestie gravida phasellus tincidunt vel aliquet sociosqu tempor laoreet dignissim diam elementum neque massa aliquam leo maecenas duis enim litora pulvinar aliquam</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gap"></div>
    <div class="container">
        <div class="row">
            <h2 class="center">{{Lang::get('messages.services',array(),$la)}}</h2>
            <div class="gap gap-small"></div>
            <div class="span3">
                <p>Netus dictum venenatis fusce dictum eleifend etiam nam rhoncus augue imperdiet lacinia sit scelerisque massa risus felis a hendrerit vestibulum</p>
                <a id="fredsel-prev" class="fredsel-prev box-icon icon-chevron-left" href="#" style="display: inline-block;"></a>
                <a id="fredsel-next" class="fredsel-next box-icon icon-chevron-right" href="#" style="display: inline-block;"></a>
            </div>
            <div class="span9">
                <div class="fredsel-wrapper">
                    <div class="row">
                        <div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 900px; height: 259px; margin: 0px; overflow: hidden;"><ul class="list fredsel" id="fredsel" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 5700px; height: 259px;">
                                @foreach( ServiceCategory::all() as $s)
                                    <div class="span2">
                                        <div class="thumb">
                                            <div class="thumb-header">
                                                <a href="/{{$la}}/@lang('routes.service')/{{$s->{$la.'_title'} }}">
                                                    <img src="{{$s->img}}" alt="{{$s->{$la.'_description'} }}" title="{{$s->{$la.'_title'} }}">
                                                </a>
                                            </div>
                                            <div class="thumb-caption">
                                                <h5 class="thumb-title"><a href="/{{$la}}/@lang('routes.service')/{{$s->{$la.'_title'} }}">{{$s->{$la.'_title'} }}</a></h5>
                                                <p class="thumb-desc">{{ str_limit( $s->{$la.'_description'},35,'...') }}</p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                    <div class="gap"></div>

                </div>
            </div>
        </div>
    </div>
    <div class="gap"></div>
    <div class="bg-mask-darken" data-bg-img="{{url('front')}}/img/bg/street.jpg">
        <div class="container mask-front">
            <div class="gap gap-big">
                <h1 class="white">{{Lang::get("messages.the_company",array(),$la)}}</h1>
                <p class="white">{{ $about->{$la.'_content'} }}</p>
            </div>
        </div>
    </div>
    <div class="gap"></div>
    <div class="container">
    <div class="row">
        <h1 class="center">{{Lang::get('messages.last_project',array(),$la)}}</h1>
        <div class="gap"></div>
        @foreach(Portfolio::limit(4)->orderBy('created_at')->get() as $p)
            <div class="span3">
                <div class="thumb">
                    <div class="thumb-header">
                        <a href="/{{$la}}/@lang("routes.projects")">
                            <img src="{{$p->img1}}" alt="{{$p->{$la.'_title'} }}" title="{{$p->{$la.'_title'} }}" />
                        </a>
                    </div>
                    <div class="thumb-caption">
                        <h5 class="thumb-title"><a href="/{{$la}}/@lang("routes.projects")">{{$p->{$la.'_title'} }}</a></h5>
                        <p class="thumb-desc">{{ str_limit($p->{$la.'_body'},100,'...') }}</p><a class="btn btn-primary btn-small" href="#">{{Lang::get('messages.read_more',array(),$la)}}</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    </div>
    <div class="gap gap-small"></div>
    <div class="container">
        <h2 class="center">{{Lang::get("messages.partner",array(),$la)}}</h2>
        <div class="gap gap-small"></div>
        <div class="row row-wrap">
            @foreach(Company::all() as $c)
            <div class="span2 ">
                <a href="http://{{$c->website}}" target="_blank"><img src="{{$c->logo}}" alt="Image Alternative text" title="{{$c->{$la.'_name'} }}"></a>
            </div>
            @endforeach()
        </div>
        <div class="gap gap-small"></div>

    </div>
@stop