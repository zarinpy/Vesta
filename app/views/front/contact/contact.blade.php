<?php $la = App::getLocale(); ?>
@extends('layout.site_layout')
@section('head')
    <title>@lang('messages.company_name')::@lang('routes.contact')</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="omid zarinmahd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="@lang('messages.company_name')">
    <meta property="og:locale" content="{{$la}}" />
    @foreach(Config::get('app.alt_langs') as $c)
        <meta property="og:locale:alternate" content="{{$c}}" />
    @endforeach
    <meta property="og:site_name" content="vestatavan">
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:description" content="">
    <meta property="fb:app_id" content="">
    <meta property="og:type" content="business.business">
@stop
@section('content')
    <div class="top-title-area bg-img">
        <div class="container">
            <h1 class="title-page">@lang('menu.contact')</h1>
        </div>
    </div>
    <div class="gap"></div>
    <div class="container">
        <div class="row">
            @if(App::getLocale()=='fa')
                <div class="span6">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d25923.118561553296!2d51.15810551330638!3d35.69202369781667!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8df063abdfc6b7%3A0xd6b3cb688d4ea97!2z2YjYs9iq2Kcg2KrZiNin2YYg2YHYsdiq2KfaqQ!5e0!3m2!1sen!2sir!4v1446463894194" width="570" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="span3">
                    <form name="contactForm" id="contact-form" class="contact-form" method="post" action="includes/mail/index.html">
                        {{Form::token()}}
                        <fieldset>
                            <div class="row-fluid">
                                <label>@lang('contact.name')</label>
                                <div class="alert form-alert" id="form-alert-name">@lang('contact.error4')</div>
                                <input class="span12" id="name" type="text" placeholder="">
                            </div>
                            <div class="row-fluid">
                                <label>@lang('contact.email')</label>
                                <div class="alert form-alert" id="form-alert-email">@lang('contact.error3')</div>
                                <input class="span12" id="email" type="text" placeholder="">
                            </div>
                            <div class="row-fluid">
                                <label>@lang('contact.message')</label>
                                <div class="alert form-alert" id="form-alert-message">@lang('contact.error1')</div>
                                <textarea class="span12" id="message" placeholder="Your message"></textarea>
                            </div>
                            <div class="alert alert-success form-alert" id="form-success">@lang('contact.success')</div>
                            <div class="alert alert-error form-alert" id="form-fail">@lang('contact.error2')</div>
                            <button id="send-message" type="submit" class="btn btn-primary btn-block">@lang('contact.send')</button>
                        </fieldset>
                    </form>
                </div>
                <div class="span3">
                    <h5>@lang('contact.info')</h5>
                    <p style="text-align: right">@lang('messages.addr')</p>
                    <ul class="list">
                        <li><i class="icon-map-marker"></i> Location: Mountain View, CA 94043</li>
                        <li><i class="icon-phone"></i> Phone: 555-555-555</li>
                        <li><i class="icon-envelope"></i> E-mail: <a href="#">info at vestatavan.com</a>
                        </li>
                    </ul>
                </div>
            @else()
                <div class="span3">
                    <h5>@lang('contact.info')</h5>
                    <p>Integer consectetur eros aliquam fermentum pretium senectus euismod velit ligula ornare euismod etiam ultricies libero hendrerit quam dolor litora himenaeos</p>
                    <ul class="list">
                        <li><i class="icon-map-marker"></i> Location: Mountain View, CA 94043</li>
                        <li><i class="icon-phone"></i> Phone: 555-555-555</li>
                        <li><i class="icon-envelope"></i> E-mail: <a href="#">info at vestatavan.com</a>
                        </li>
                    </ul>
                </div>
                <div class="span3">
                    <form name="contactForm" id="contact-form" class="contact-form" method="post" action="includes/mail/index.html">
                        {{Form::token()}}
                        <fieldset>
                            <div class="row-fluid">
                                <label>@lang('contact.name')</label>
                                <div class="alert form-alert" id="form-alert-name">@lang('contact.error4')</div>
                                <input class="span12" id="name" type="text" placeholder="">
                            </div>
                            <div class="row-fluid">
                                <label>@lang('contact.email')</label>
                                <div class="alert form-alert" id="form-alert-email">@lang('contact.error3')</div>
                                <input class="span12" id="email" type="text" placeholder="">
                            </div>
                            <div class="row-fluid">
                                <label>@lang('contact.message')</label>
                                <div class="alert form-alert" id="form-alert-message">@lang('contact.error1')</div>
                                <textarea class="span12" id="message" placeholder="Your message"></textarea>
                            </div>
                            <div class="alert alert-success form-alert" id="form-success">@lang('contact.success')</div>
                            <div class="alert alert-error form-alert" id="form-fail">@lang('contact.error2')</div>
                            <button id="send-message" type="submit" class="btn btn-primary btn-block">@lang('contact.send')</button>
                        </fieldset>
                    </form>
                </div>
              <div class="span6">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d25923.118561553296!2d51.15810551330638!3d35.69202369781667!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8df063abdfc6b7%3A0xd6b3cb688d4ea97!2z2YjYs9iq2Kcg2KrZiNin2YYg2YHYsdiq2KfaqQ!5e0!3m2!1sen!2sir!4v1446463894194" width="570" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
              </div>
            @endif()

        </div>
        <div class="gap"></div>
    </div>
@stop()