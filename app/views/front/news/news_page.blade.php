<?php $la = App::getLocale();?>
@extends('layout.site_layout')
@section('head')
    <title>{{$news->{$la.'_title'} }}</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="@foreach($news->category as $c){{ $c->{$la.'_ecat_title'} }}, @endforeach" />
    <meta name="description" content="{{$news->{$la.'_title'} }}">
    <meta name="author" content="omid zarinmahd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="@lang('messages.company_name')">
    <meta property="og:site_name" content="@lang('messages.company_name')">
    <meta property="og:locale" content="{{$la}}" />
    @foreach(Config::get('app.alt_langs') as $c)
        <meta property="og:locale:alternate" content="{{$c}}" />
    @endforeach
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:description" content="{{$news->{$la.'_title'} }}">
    <meta property="fb:app_id" content="">
    <meta property="og:type" content="my_namespace:news">
@stop
@section('content')
    <div class="top-title-area bg-img">
        <div class="container">
            <h1 class="title-page">{{$news->{$la.'_title'} }}</h1>
        </div>
    </div>
    <div class="gap"></div>
    <div class="container float-right">
        {{$news->{$la.'_body'} }}
        <div class="gap"></div>
    </div>
@stop()