<?php
$la = App::getLocale();
$description = Setting::where('key','=',$la.'_description')->get()->toArray();
$keywords = Setting::where('key','=',$la.'_keywords')->get()->toArray();
?>
@extends('layout.site_layout')
@section('head')
    <title>{{Lang::get('menu.learning',array(),$la)}}</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="@foreach($p_cat as $c){{ $c->{$la.'_ecat_title'} }}, @endforeach" />
    <meta name="description" content="{{$description[0]['value']}}">
    <meta name="author" content="omid zarinmahd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="@lang('messages.company_name')">
    <meta property="og:locale" content="{{$la}}" />
    @foreach(Config::get('app.alt_langs') as $c)
        <meta property="og:locale:alternate" content="{{$c}}" />
    @endforeach
    <meta property="og:site_name" content="vestatavan">
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:description" content="{{$description[0]['value']}}">
    <meta property="fb:app_id" content="">
    <meta property="og:type" content="my_namespace:news">
@stop
@section('content')
    <div class="top-title-area bg-img">
        <div class="container">
            <h1 class="title-page">@lang("menu.learning")</h1>
        </div>
    </div>
    <div class="gap"></div>
    <div class="container">
        <div class="row">
            @if($la == 'fa')
                <div class="span9">
                    <ul class="list list-border">
                        @foreach($p_list as $e)
                            <li>
                                <div class="thumb">
                                    <div class="thumb-header-right">
                                        <a href="/{{$la}}/@lang('routes.learning')/category/{{$e->{$la.'_title'} }}"><img src="{{url($e->img)}}" width="150" height="150" alt="Image Alternative text" title="{{$e->{$la.'_title'} }}"></a>
                                    </div>
                                    <div class="thumb-caption">
                                        <h5 class="thumb-title"><a href="#">{{$e->{$la.'_title'} }}</a></h5>
                                        <p class="thumb-desc">{{str_limit($e->{$la.'_body'},200,'...' )}}</p><a class="btn btn-primary btn-small" href="/{{$la}}/@lang('routes.learning')/category/{{$e->{$la.'_title'} }}">@lang('messages.read_more')</a>
                                    </div>
                                </div>
                            </li>
                        @endforeach()
                    </ul>
                    <div class="pagination">
                        <ul>
                            <?php echo $p_list->links(); ?>
                        </ul>
                    </div>
                    <div class="gap"></div>
                </div>
                <div class="span3">
                    <aside class="sidebar-right hidden-phone">
                        <h4>@lang('messages.category')</h4>
                        <ul class="list list-icon list-icon-caret-right list-icon-color">
                            @foreach($p_cat as $c)
                                <li><a href="/{{$la}}/@lang('routes.learning')/{{ $c->{$la.'_ecat_title'} }}">{{ $c->{$la.'_ecat_title'} }}</a></li>
                            @endforeach
                        </ul>
                        <div class="gap gap-small"></div>
                        <h4>@lang('messages.recent_posts')</h4>
                        <ul class="list list-block list-posts">
                            <li>
                                <div class="thumb">
                                    @foreach($last as $l)
                                        <div class="thumb-header-left">
                                        </div>
                                        <div class="thumb-caption">
                                            <h5 class="thumb-title"><a href="/{{$la}}/@lang('routes.learning')/category/{{$e->{$la.'_title'} }}">{{$l->{$la.'_title'} }}</a></h5>
                                            <div class="thumb-desc"><p>{{ str_limit( $l->{$la.'_body'},200,'...') }}</p></div>
                                            <div class="gap gap-small"></div>
                                        </div>
                                    @endforeach
                                </div>
                            </li>
                        </ul>
                        <div class="gap gap-small"></div>
                    </aside></div>
            @else()
                <div class="span3">
                    <aside class="sidebar-right hidden-phone">
                        <h4>@lang('messages.category')</h4>
                        <ul class="list list-icon list-icon-caret-right list-icon-color">
                            @foreach($p_cat as $c)
                                <li><a href="/{{$la}}/@lang('routes.learning')/{{ $c->{$la.'_ecat_title'} }}">{{ $c->{$la.'_ecat_title'} }}</a></li>
                            @endforeach
                        </ul>
                        <div class="gap gap-small"></div>
                        <h4>@lang('messages.recent_posts')</h4>
                        <ul class="list list-block list-posts">
                            <li>
                                <div class="thumb">
                                    @foreach($last as $l)
                                        <div class="thumb-header-left">
                                        </div>
                                        <div class="thumb-caption">
                                            <h5 class="thumb-title"><a href="/{{$la}}/@lang('routes.learning')/category/{{$l->{$la.'_title'} }}">{{$l->{$la.'_title'} }}</a></h5>
                                            <div class="thumb-desc"><p>{{ str_limit( $l->{$la.'_body'},200,'...') }}</p></div>
                                            <div class="gap gap-small"></div>
                                        </div>
                                    @endforeach
                                </div>
                            </li>
                        </ul>
                        <div class="gap gap-small"></div>
                    </aside></div>
                <div class="span9">
                    <ul class="list list-border">
                        @foreach($p_list as $e)
                            <li>
                                <div class="thumb">
                                    <div class="thumb-header-left">
                                        <a href="/{{$la}}/@lang('routes.learning')/category/{{$e->{$la.'_title'} }}"><img src="{{url($e->img)}}" width="150" height="150" alt="Image Alternative text" title="{{$e->{$la.'_title'} }}"></a>
                                    </div>
                                    <div class="thumb-caption">
                                        <h5 class="thumb-title"><a href="#">{{$e->{$la.'_title'} }}</a></h5>
                                        <p class="thumb-desc">{{str_limit($e->{$la.'_body'},200,'...' )}}</p><a class="btn btn-primary btn-small" href="/{{$la}}/@lang('routes.learning')/category/{{$e->{$la.'_title'} }}">@lang('messages.read_more')</a>
                                    </div>
                                </div>
                            </li>
                        @endforeach()
                    </ul>
                    <div class="pagination">
                        <ul>
                            <?php echo $p_list->links(); ?>
                        </ul>
                    </div>
                    <div class="gap"></div>
                </div>
            @endif()

        </div>
    </div>
@stop