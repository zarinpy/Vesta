<?php $la = App::getLocale(); ?>
@extends('layout.site_layout')
@section('head')
    <title>{{$team->{$la.'_name'} }} {{$team->{$la.'_family'} }}</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="{{$team->{$la.'_name'} }} {{$team->{$la.'_family'} }}" />
    <meta name="description" content="{{$team->{$la.'_name'} }} {{$team->{$la.'_family'} }}">
    <meta name="author" content="omid zarinmahd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="@lang('messages.company_name')">
    <meta property="og:site_name" content="@lang('messages.company_name')">
    <meta property="og:locale" content="{{$la}}" />
    @foreach(Config::get('app.alt_langs') as $c)
        <meta property="og:locale:alternate" content="{{$c}}" />
    @endforeach
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:description" content="{{$team->{$la.'_name'} }} {{$team->{$la.'_family'} }}">
    <meta property="fb:app_id" content="">
    <meta property="og:type" content="profile">
@stop
@section('content')
    <div class="top-title-area bg-img">
        <div class="container">
            <h1 class="title-page">{{$team->{$la.'_name'} }} {{$team->{$la.'_family'} }} </h1>
        </div>
    </div>
    <div class="gap"></div>
    <div class="container">
        <div class="row">
        @if($la == 'fa')
            <div class="span9">
                {{$team->{$la.'_description'} }}
            </div>
            <div class="span3">
                <img src="{{url($team->image)}}" />
                <div class="gap-small"></div>
                <ul class="list-inline">
                    <li><a href="http://{{$team->google }}" class="icon-google-plus icon-3x"></a></li>
                    <li><a href="http://{{$team->linkedin }}" class="icon-linkedin icon-3x"></a></li>
                    <li><a href="http://{{$team->facebook }}" class="icon-facebook icon-3x"></a></li>
                </ul>
                <div class="gap-small"></div>
                <ul class="list">
                    <li>@lang("messages.name") : {{$team->{$la.'_name'} }}</li>
                    <li>@lang("messages.family") : {{$team->{$la.'_family'} }}</li>
                    <li>@lang("messages.profession") : {{$team->{$la.'_prof_title'} }}</li>
                    <li>@lang("messages.role") : {{$team->{$la.'_cat_title'} }}</li>
                </ul>
            </div>
        @else()
            <div class="span3">
                <img src="{{url($team->image)}}" />
                <div class="gap-small"></div>
                <ul class="list-inline">
                    <li><a href="http://{{$team->google }}" class="icon-google-plus icon-3x"></a></li>
                    <li><a href="http://{{$team->linkedin }}" class="icon-linkedin icon-3x"></a></li>
                    <li><a href="http://{{$team->facebook }}" class="icon-facebook icon-3x"></a></li>
                </ul>
                <div class="gap-small"></div>
                <ul class="list">
                    <li>@lang("messages.name") : {{$team->{$la.'_name'} }}</li>
                    <li>@lang("messages.family") : {{$team->{$la.'_family'} }}</li>
                    <li>@lang("messages.profession") : {{$team->{$la.'_prof_title'} }}</li>
                    <li>@lang("messages.role") : {{$team->{$la.'_cat_title'} }}</li>
                </ul>
            </div>
            <div class="span9">
                {{$team->{$la.'_description'} }}
            </div>
        @endif()
        </div>
        <div class="gap"></div>
    </div>
@stop
