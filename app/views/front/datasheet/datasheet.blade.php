<?php $la = App::getLocale();?>
@extends('layout.site_layout')
@section('head')
    <title>@lang('messages.company_name')</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="omid zarinmahd">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="@lang('messages.company_name')">
    <meta property="og:locale" content="{{$la}}" />
    @foreach(Config::get('app.alt_langs') as $c)
        <meta property="og:locale:alternate" content="{{$c}}" />
    @endforeach
    <meta property="og:site_name" content="vestatavan">
    <meta property="og:url" content="{{URL::current()}}">
    <meta property="og:description" content="">
    <meta property="fb:app_id" content="">
    <meta property="og:type" content="business.business">
@stop()
@section("content")
    <div class="container">
        <div class="row">
            <h1 class="center"></h1>
            <div class="gap"></div>
            <div class="offset4 span4 text-center">
                {{Form::open(array('route'=>array('datasheet.data'),'method'=>'POST','class'=>'form-search'))}}
                {{Form::token()}}
                <h2 class="form-signin-heading">Please enter the code</h2>
                    <div class="input-append">
                        {{Form::text('data',null,array('class'=>'span4 search-query','required',))}}
                        <button type="submit" class="btn">Search</button>
                    </div>
                {{Form::close()}}
            </div>
            <div class="gap"></div>
        </div>
        <div class="gap"></div>
        <div class="row">
            @if(Session::has('gp_access'))
                <h3 class="offset4 " style="color: #c09853;">{{Session::get('data')}}</h3>
                <h3 class="offset4 " style="color: red;"> you don't have access to this file</h3>
                <div class="gap"></div>
            @endif
            @if(Session::has('notfound'))
                <h3 class="offset4 " style="color: #c09853;">{{Session::get('data')}}</h3>
                <div class="gap-small"></div>
                <hr/>
                <h3 class="offset4 " style="color: #c09853;">document not existed</h3>
                <div class="gap"></div>
            @endif
            @if(isset($datasheet) and count($datasheet) != 0)
                <h3 class="offset5 " style="color: #468847;"> @if(isset($data)) {{$data}} @endif() </h3>
                <div class="gap-small"></div>
                <div class="thumb-desc offset5">found {{count($datasheet)}} files</div>
                <div class="gap-small"></div>
                <hr/>
                <ul>
                @foreach($datasheet as $d)
                    <li class=" list list-block">
                        <div class="thumb">
                            <div class="span3">
                                <div class="thumb-caption">
                                    <h5 class="thumb-title">{{ $d->{$la.'_name'} }}</h5>
                                    <p class="thumb-desc"><a href="{{url($d->file)}}">file</a></p>
                                </div>
                            </div>
                            <div class="span9">
                                {{ $d->{$la.'_description'} }}
                            </div>
                        </div>
                    </li>
                    <div class="gap-small"></div>
                    <hr/>
                @endforeach
                </ul>
            @endif()
        </div>
    </div>
@stop()