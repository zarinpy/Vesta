<?php
if (in_array(Request::segment(1), Config::get('app.alt_langs'))){
	App::setLocale(Request::segment(1));
	Session::set('la',App::getLocale());
	Config::set('app.locale_prefix', Request::segment(1));
}
$routes = Lang::get('routes');
foreach($routes as $k => $v){
	Route::pattern($k, $v);
}
$la = App::getLocale();
Route::group(array('prefix'=>Config::get('app.locale_prefix'),"before"=>'auth'),function(){
    /*************************************************************************/
    Route::get("/{customer_portal}","HomeController@datasheet");
    Route::post("/customer_portal/data",['as'=>'datasheet.data','uses'=>'HomeController@datasheet_search']);
});
Route::group(array('prefix' =>Config::get('app.locale_prefix')),function(){
	/*************************************************************************/
	Route::get("/{product}","HomeController@product");
	Route::get("/{product}/{title}","HomeController@product_page");
	/*************************************************************************/
	Route::get("/{projects}","HomeController@project");
	Route::get("/{projects}/{title}/","HomeController@project_page");
	/*************************************************************************/
	Route::get("/",['as'=>'home','uses'=>"HomeController@home"]);
	Route::get("/{about}","HomeController@about");
	/*************************************************************************/
	Route::get("/{learning}","HomeController@news");
    Route::get("/{learning}/{category}/{title}","HomeController@news_page");
    Route::get("/{learning}/{category}/","HomeController@news_cat");
    /*************************************************************************/
    Route::get("/{team}/{title}","HomeController@team");
	/*************************************************************************/
	Route::get("/{service}/{category}","HomeController@service");
	Route::get("/{service}/{category}/{title}","HomeController@service_page");

	/*************************************************************************/
    Route::get("/{contact}","HomeController@contact");
});
Route::group(array('prefix'=>"admin",'before'=>'auth'),function(){
	Route::get('dashboard',function(){
        $arr = array();
        foreach (Auth::user()->group as $gp){
            $arr[] = $gp->en_name;
        }
        $user_gp = $arr[0];
        if($user_gp == "admin"){
            return View::make('backend.index')->with('class_dashboard','class="open active"');
        }else{
            return Redirect::route('home');
        }
	});
	/*************************************************************************/
	Route::resource('teams', 'TeamsController');
	Route::resource('team_categories', 	'TeamCategoriesController');
	Route::resource('team_profession_categories', 'TeamProfessionCategoriesController');
	/*************************************************************************/
	Route::resource('menu','MenusController');
	/*************************************************************************/
	Route::resource('portfolio_categories', 'PortfolioCategoriesController');
	Route::resource('portfolios', 'PortfoliosController');
	/*************************************************************************/
	Route::resource('companies', 'CompaniesController');
	Route::resource('company_categories', 'CompanyCategoriesController');
	/*************************************************************************/
	Route::resource('elearnings', 'ElearningsController');
	Route::resource('elearning_categories', 'ElearningCategoriesController');
	Route::resource('medias', 'MediasController');
	/*************************************************************************/
	Route::resource('products', 'ProductsController');
	Route::resource('product_categories', 'ProductCategoriesController');
	/*************************************************************************/
	Route::resource('pages', 'PagesController');
	/*************************************************************************/
	Route::resource('datasheets', 'DatasheetsController');
	Route::resource('datasheet_codes', 'DatasheetCodesController');
	Route::resource('datasheet_companies', 'DatasheetCompaniesController');
	Route::resource('datasheet_project_types', 'DatasheetProjectTypesController');
	Route::resource('datasheet_customers', 'DatasheetCustomersController');
	/*************************************************************************/
	Route::resource('users', 'UserController');
	Route::resource('groups', 'GroupsController');
	/*************************************************************************/
	Route::resource('services', 'ServicesController');
	/*************************************************************************/
	Route::resource('service_categories', 'ServiceCategoriesController');
	Route::resource('settings', 'SettingsController');
	/*************************************************************************/
	Route::resource('sliders', 'SlidersController');
	Route::resource('portfolio_customers', 'PortfolioCustomersController');
});
Route::controller('password', 'RemindersController');
Route::get('login', array('as' => 'login', 'uses' => 'UserController@loginshow'));
Route::post('login', array('as' => 'login', 'uses' => 'UserController@authenticate'));
Route::get('logout', array('as' => 'logout', 'uses' => 'UserController@logout'));
Route::get('sitemap',function(){
    $portfolio = Portfolio::orderBy('created_at')->get();
    $learning = Elearning::orderBy('created_at')->get();
    $service = Service::orderBy('created_at')->get();
    $team = Team::orderBy('created_at')->get();
    $product = Product::orderBy('created_at')->get();
	$sitemap = App::make('sitemap');
    //$sitemap->setCache('laravel.sitemap', 3600);
	$sitemap->add(url('/'.App::getLocale().'/'.Lang::get('routes.about',array(),App::getLocale())),date("Y-m-d"),'weekly','0.8');
	$sitemap->add(url('/'.App::getLocale()),date("Y-m-d"),'weekly','0.8');
	$sitemap->add(url('/'.App::getLocale().'/'.Lang::get('routes.product',array(),App::getLocale())),date("Y-m-d"),'weekly','0.8');
	$sitemap->add(url('/'.App::getLocale().'/'.Lang::get('routes.projects',array(),App::getLocale())),date("Y-m-d"),'weekly','0.8');
	$sitemap->add(url('/'.App::getLocale().'/'.Lang::get('routes.learning',array(),App::getLocale())),date("Y-m-d"),'weekly','0.8');
	$sitemap->add(url('/'.App::getLocale().'/'.Lang::get('routes.about',array(),App::getLocale())),date("Y-m-d"),'weekly','0.8');
	$sitemap->add(url('/'.App::getLocale().'/'.Lang::get('routes.certificate',array(),App::getLocale())),date("Y-m-d"),'weekly','0.8');
	$sitemap->add(url('/'.App::getLocale().'/'.Lang::get('routes.mission_statement',array(),App::getLocale())),date("Y-m-d"),'weekly','0.8');
	$sitemap->add(url('/'.App::getLocale().'/'.Lang::get('routes.organizational_value',array(),App::getLocale())),date("Y-m-d"),'weekly','0.8');
	$sitemap->add(url('/'.App::getLocale().'/'.Lang::get('routes.customers',array(),App::getLocale())),date("Y-m-d"),'weekly','0.8');
    foreach ($product as $p) {
        $sitemap->add(
            url('/'.App::getLocale().'/'.Lang::get('routes.projects',array(),App::getLocale()).'/'.$p->{App::getLocale().'_title'} ),
            date("Y-m-d"),
            'weekly',
            '0.3'

        );
    }
    foreach ($portfolio as $p) {
        $sitemap->add(
            url('/'.App::getLocale().'/'.Lang::get('routes.projects',array(),App::getLocale()).'/'.$p->{App::getLocale().'_title'} ),
            date("Y-m-d"),
            'weekly',
            '0.3'

        );
    }
    foreach ($learning as $l) {
        $sitemap->add(
            url('/'.App::getLocale().'/'.Lang::get('routes.learning',array(),App::getLocale()).'/'.$l->{App::getLocale().'_title'} ),
            date("Y-m-d"),
            'weekly',
            '0.3'
        );
    }
    foreach ($service as $s) {
        $sitemap->add(
            url('/'.App::getLocale().'/'.Lang::get('routes.service',array(),App::getLocale()).'/'.$s->{App::getLocale().'_title'} ),
            date("Y-m-d"),
            'weekly',
            '0.3'
        );
    }
    foreach ($team as $t) {
        $sitemap->add(
            url('/'.App::getLocale().'/'.Lang::get('routes.team',array(),App::getLocale()).'/'.$t->{App::getLocale().'_family'} ),
            date("Y-m-d"),
            'weekly',
            '0.3'
        );
    }

    return $sitemap->render('xml');
});
App::missing(function(){
	return Response::view('layout.front.404',array('title'=>'not found'),404);
});