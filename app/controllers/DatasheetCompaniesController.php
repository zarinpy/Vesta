<?php
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class DatasheetCompaniesController extends \BaseController {
	/**
	 * Display a listing of datasheetcompanies
	 *
	 * @return Response
	 */
	public function index()
	{
		$datasheetcompanies = Datasheetcompany::all();
		return View::make('backend.datasheet_companies.index', compact('datasheetcompanies'))
		->with('title','لیست مشتریان')
        ->with('class_datasheets',"class='open active'");
	}
	/**
	 * Show the form for creating a new datasheetcompany
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.datasheet_companies.create')
        ->with('class_datasheets',"class='open active'")
        ->with('title',"ایجاد مشتری");
	}
	/**
	 * Store a newly created datasheetcompany in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Datasheetcompany::$rules);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}
		Datasheetcompany::create($data);

		return Redirect::route('admin.datasheet_companies.index')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد')
		->with('class_datasheets','class="active open"');
	}

	/**
	 * Display the specified datasheetcompany.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$datasheetcompany = Datasheetcompany::findOrFail($id);
			return View::make('backend.datasheet_companies.show', compact('datasheetcompany'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.datasheet_companies.index');
		}
	}

	/**
	 * Show the form for editing the specified datasheetcompany.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$datasheetcompany = Datasheetcompany::find($id);
			return View::make('backend.datasheet_companies.edit', compact('datasheetcompany'))
				->with('class_datasheets',"class='open active'")
				->with('title',"ویرایش مشتری");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.datasheet_companies.index');
		}
	}

	/**
	 * Update the specified datasheetcompany in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$datasheetcompany = Datasheetcompany::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Datasheetcompany::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$datasheetcompany->update($data);
		return Redirect::route('admin.datasheet_companies.index')
		->with('class_datasheets',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}

	/**
	 * Remove the specified datasheetcompany from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Datasheetcompany::destroy($id);
		return Redirect::route('admin.datasheet_companies.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_datasheets',"class='open active'");
	}
}
