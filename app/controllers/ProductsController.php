<?php
use Intervention\Image\ImageManagerStatic as Imagic;
use \Illuminate\Database\Eloquent\ModelNotFoundException;
class ProductsController extends \BaseController {

	/**
	 * Display a listing of products
	 *
	 * @return Response
	 */
	public function index()
	{
		$products = Product::all();
		return View::make('backend.products.index', compact('products'))
		->with('title','لیست محصولات')
        ->with('class_product',"class='open active'");
	}

	/**
	 * Show the form for creating a new product
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.products.create')
		->with('menus', ProductCategoriesController::categoryFullArray())
		->with('title','ایجاد محصول')
        ->with('class_product',"class='open active'")
		->with('info_product',"<div class='alert alert-block alert-info fade in'><button data-dismiss='alert' class='close'' type='button''>×</button>
										<h3 class='alert-heading'><i class='fa fa-info-circle'></i> راهنمایی  </h3>
										<h4><p>کاربر گرامی در نظر داشته باشید برای انتخاب دسته بندی ها چنانچه زیر دسته است ، دسته پدر و یا پدرانش را هم انتخاب کنید</p></h4></div>");
	}

	/**
	 * Store a newly created product in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$product = new Product();
		$validator = Validator::make($data = Input::all(), Product::$rules);
		if ($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('file')){
			$imagefile3 = Input::file('file');
			$filename3 = $imagefile3->getClientOriginalName();
			$width3 = Imagic::make($imagefile3)->width();
			$height3 = Imagic::make($imagefile3)->height();
			$image3 = Imagic::make($imagefile3);
            $lens = $width3 - $height3;
			if(($width3 == $height3) or ($height3 > $width3)){
				return Redirect::back()->withInput()->with('img','تصویر سوم یا پرتره است یا طول و عرضش برابرست');
			}elseif($lens >= 200){
				$path1 ="backend/assets/images/portfolio/".$filename3;
				$image3->save(public_path($path1));
			}
		}
		else{
			$path1 = "https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=1920&h=900&txttrack=0";
		}

		$product_id=Product::create(array(
			'img'=>$path1,
			'fa_title'=>Input::get('fa_title'),
			'ru_title'=>Input::get('fa_title'),
			'en_title'=>Input::get('en_title'),
			'fa_description'=>Input::get('fa_description'),
			'ru_description'=>Input::get('fa_description'),
			'en_description'=>Input::get('en_description'),
		));
		$id = $product_id->id;
		$product->find($id)->category()->attach(Input::get('category'));
		return Redirect::route('admin.products.index')
			->with('success','اطلاعات جدید با موفقیت ذخیره شد')
			->with('title','لیست محصولات')
			->with('class_product','class="active open"');
	}

	/**
	 * Display the specified product.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$product = Product::findOrFail($id);
			return View::make('backend.products.show', compact('product'));
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.products.index');
		}
	}

	/**
	 * Show the form for editing the specified product.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$product = Product::find($id);
			$pcat = $product->category;
			return View::make('backend.products.edit', array('product'=>$product, 'pcat'=>$pcat))
				->with('class_product',"class='open active'")
				->with('menus', ProductCategoriesController::categoryFullArray())
				->with('info_product_edit',"<div class='alert alert-block alert-info fade in'><button data-dismiss='alert' class='close'' type='button''>×</button><h3 class='alert-heading'><i class='fa fa-info-circle'></i> راهنمایی  </h3>
										<h4><p>کاربر گرامی اگر مایل به تغییر دسته بندی محصول هستید دسته بندی جدید را وارد کنید سیستم به صورت اتوماتیک دسته بندی های قبلی را حذف خواهد کرد</p></h4></div>")
				->with('title',"ویرایش محصول");
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.products.index');
		}
	}

	/**
	 * Update the specified product in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$product =  Product::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Product::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('file')){
			$img1 = Input::file('file');
			$filename_img1 = $img1->getClientOriginalName();
			$path1 ="backend/assets/images/products/".$filename_img1;
			File::delete(array(public_path($product->img)));
			Imagic::make($img1->getRealPath())->save(public_path($path1));
			$data['file'] = $path1;
		}
		else{
			$data['file']=$product->img1;
		}
		$product->img=$data['file'];
		$product->fa_title=Input::get('fa_title');
		$product->ru_title=Input::get('ru_title');
		$product->en_title=Input::get('en_title');
		$product->fa_description=Input::get('fa_description');
		$product->ru_description=Input::get('ru_description');
		$product->en_description=Input::get('en_description');
		$product->save();
		if(array_key_exists('category',$data)) {
			$product->find($id)->category()->sync(Input::get('category'));
		}
		return Redirect::route('admin.products.index')
		->with('class_product',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}
	/**
	 * Remove the specified product from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Product::destroy($id);
		return Redirect::route('admin.products.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_product',"class='open active'");
	}
}
