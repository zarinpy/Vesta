<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class MenusController extends \BaseController {

	/**
	 * Display a listing of menus
	 *
	 * @return Response
	 */
	public static function categoryFullArray()
	{
		$categories = Menu::where('parent_id', '=', null)->get();
		$menu = self::createTreeString($categories);
		return $menu;
	}
	public static function createTreeString($category_array)
	{
		if($category_array == null){
			return null;
		}
		$cat_array=array();
		foreach ($category_array as $category) {
			$cat_array[$category->id] = array(
				'id'=>$category->id,
				'name'=>$category->name,
				'parent_id'=>$category->parent_id,
				'active'=>$category->active
			);

			$categories = Menu::where('parent_id', '=', $category->id)->get();
			if ($categories->count() > 0)
			{
				$cat_array[$category->id]['child'] = self::createTreeString($categories);
			}
			else
				$cat_array[$category->id]['child'] = null;
		}
		return $cat_array;
	}
	public function index()
	{
		$menus = Menu::all();
		return View::make('backend.menus.index', compact('menus'))
		->with('title','لیست منو ها')
        ->with('class_menu',"class='open active'");
	}

	/**
	 * Show the form for creating a new menus
	 *
	 * @return Response
	 */
	public function create()
	{
		$menu = self::createTreeString(Menu::all());
		return View::make('backend.menus.create',compact('menu'))
		->with('title','ایجاد منو')
        ->with('class_menu',"class='open active'");
	}

	/**
	 * Store a newly created menus in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Menu::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::get('active') == 'on'){
			$data['active'] = 1;
		}else{
			$data['active'] = 0;
		}
		if(Input::get('parent_id')=='null'){
			$data['parent_id'] = null;
		}else{
			$data['parent_id'] = Input::get('parent_id');
		}
		Menu::create($data);
		return Redirect::route('admin.menus.create')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد')
		->with('class_menu','class="active open"');
	}

	/**
	 * Display the specified menus.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$menus = Menu::findOrFail($id);
            		return View::make('backend.menus.show', compact('menus'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.menus.index');
		}
	}

	/**
	 * Show the form for editing the specified menus.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$menus = Menu::find($id);
			return View::make('backend.menus.edit', compact('menus'))
				->with('class_menu',"class='open active'")
				->with('title',"");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.$COLLECTION.index');
		}
	}

	/**
	 * Update the specified menus in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$menus = Menu::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Menu::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$menus->update($data);

		return Redirect::route('admin.menus.index')
		->with('class_menu',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}

	/**
	 * Remove the specified menus from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Menu::destroy($id);

		return Redirect::route('admin.menus.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_menu',"class='open active'")
		->with('title','');
	}

}
