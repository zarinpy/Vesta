<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class GroupsController extends \BaseController {

	/**
	 * Display a listing of groups
	 *
	 * @return Response
	 */
	public function index()
	{
		$groups = Group::all();
		return View::make('backend.groups.index', compact('groups'))
		->with('title','لیست گروه ها')
        ->with('class_datasheets',"class='open active'");
	}

	/**
	 * Show the form for creating a new group
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.groups.create')
			->with('title','ایجاد گروه جدید')
        ->with('class_datasheets',"class='open active'");
	}

	/**
	 * Store a newly created group in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Group::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Group::create($data);

		return Redirect::route('admin.groups.index')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد')
		->with('class_datasheets','class="active open"');
	}

	/**
	 * Display the specified group.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$group = Group::findOrFail($id);
            		return View::make('backend.groups.show', compact('group'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.groups.index');
		}
	}

	/**
	 * Show the form for editing the specified group.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$group = Group::find($id);
			return View::make('backend.groups.edit', compact('group'))
				->with('class_datasheets',"class='open active'")
				->with('title',"ویرایش گروه");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.groups.index');
		}
	}

	/**
	 * Update the specified group in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$group = Group::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Group::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$group->update($data);

		return Redirect::route('admin.groups.index')
		->with('class_datasheets',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}

	/**
	 * Remove the specified group from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Group::destroy($id);

		return Redirect::route('admin.groups.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_datasheets',"class='open active'");
	}

}
