<?php
use Intervention\Image\ImageManagerStatic as Imagic;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class ServicesController extends \BaseController {
	/**
	 * Display a listing of services
	 *
	 * @return Response
	 */
	public function index()
	{
		$services = Service::all();
		return View::make('backend.services.index', compact('services'))
		->with('title','لیست خدمات')
        ->with('class_service',"class='open active'");
	}
	/**
	 * Show the form for creating a new service
	 *
	 * @return Response
	 */
	public function create()
	{
		$category = ServiceCategory::all();
		return View::make('backend.services.create',compact('category'))
			->with('title','ایجاد خدمات')
        ->with('class_service',"class='open active'");
	}
	/**
	 * Store a newly created service in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$path1 = null;
		$validator = Validator::make($data = Input::all(), Service::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('file')){
			$img1 = Input::file('file');
			$filename1 = $img1->getClientOriginalName();
			$width = Imagic::make($img1)->width();
			$height = Imagic::make($img1)->height();
			if($width == $height){
				$image = Imagic::make($img1)->resize(125,125);
				$thumbnail ="backend/assets/images/service/".$filename1;
				$image->save(public_path($thumbnail));
			}else{
				return Redirect::back()->withInput()->with('img','طول و عرض تصویر کوچک برابر نیست');
			}
			$path1 ="backend/assets/images/service/".$filename1;
			Imagic::make($img1->getRealPath())->save(public_path($path1));
		}else{
			$path1 = "https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=125&h=125&txttrack=0";
		}
		$service = new Service();
		$service->fa_title = Input::get('fa_title');
		$service->ru_title = Input::get('ru_title');
		$service->en_title = Input::get('en_title');
		$service->fa_body = Input::get('fa_body');
		$service->ru_body = Input::get('ru_body');
		$service->en_body = Input::get('en_body');
		$service->img = $path1;
		$service->save();
		$id = $service->id;
		$service = new Service();
		$service->find($id)->category()->attach(Input::get('category'));
		return Redirect::route('admin.services.index')
			->with('success','اطلاعات جدید با موفقیت ذخیره شد')
			->with('class_service','class="active open"');
	}
	/**
	 * Display the specified service.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$service = Service::findOrFail($id);
            		return View::make('backend.services.show', compact('service'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.services.index');
		}
	}
	/**
	 * Show the form for editing the specified service.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$result_scat=[];
			$service = Service::find($id);
			$category = ServiceCategory::all();
			$servicecaregory = $service->category;
			foreach($servicecaregory as $scat){
				$result_scat[]=$scat->fa_title;
			}
			return View::make('backend.services.edit', array('service'=>$service,'category'=>$category,'result_scat'=>$result_scat))
				->with('class_service',"class='open active'")
				->with('title',"ویرایش خدمات");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.services.index');
		}
	}
	/**
	 * Update the specified service in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$service = Service::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Service::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('file')){
			$img1 = Input::file('file');
			$filename_img1 = $img1->getClientOriginalName();
			$path1 ="backend/assets/images/service/".$filename_img1;
			File::delete(array(public_path($service->img)));
			Imagic::make($img1->getRealPath())->save(public_path($path1));
			$data['file'] = $path1;
		}else{
			$data['file']=$service->img;
		}
		$service->fa_title = Input::get('fa_title');
		$service->ru_title = Input::get('ru_title');
		$service->en_title = Input::get('en_title');
		$service->en_body = Input::get('en_body');
		$service->ru_body = Input::get('ru_body');
		$service->fa_body = Input::get('fa_body');
		$service->save();
		$service = new Service();
		$service->find($id)->category()->sync(Input::get('category'));
		return Redirect::route('admin.services.index')
		->with('class_service',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}
	/**
	 * Remove the specified service from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$service = Service::find($id);
		if(File::exists($service->img)){
			File::delete(array(public_path($service->img)));
		}
		Service::destroy($id);
		return Redirect::route('admin.services.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_service',"class='open active'");
	}
}