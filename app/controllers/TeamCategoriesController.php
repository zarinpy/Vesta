<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class TeamCategoriesController extends \BaseController {
	/**
	 * Display a listing of teamcategories
	 *
	 * @return Response
	 */
	public function index()
	{
		$teamcategories = Teamcategory::all();
		return View::make('backend.team_categories.index', compact('teamcategories'))
			->with('class_team',"class='open active'")
			->with('title','لیست سمت ها');
	}
	/**
	 * Show the form for creating a new teamcategory
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.team_categories.create')
			->with('class_team',"class='open active'")
			->with('title','ایجاد سمت جدید');
	}
	/**
	 * Store a newly created teamcategory in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Teamcategory::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		Teamcategory::create($data);
		return Redirect::route('admin.team_categories.index')
			->with('success','اطلاعات با موفقیت ذخیره شد')
			->with('class_team',"class='open active'");
	}
	/**
	 * Display the specified teamcategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$teamcategory = Teamcategory::findOrFail($id);
			return View::make('backend.team_categories.show', compact('teamcategory'));
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.team_categories.index');
		}
	}
	/**
	 * Show the form for editing the specified teamcategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$teamcategory = Teamcategory::find($id);
			return View::make('backend.team_categories.edit', compact('teamcategory'))
				->with('class_team',"class='open active'")
				->with('title',"ویرایش سمت");
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.team_categories');
		}
	}
	/**
	 * Update the specified teamcategory in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$teamcategory = Teamcategory::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Teamcategory::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$teamcategory->update($data);
		return Redirect::route('admin.team_categories.index')
			->with('update','اطلاعات با موفقیت ویرایش شد')
			->with('class_team',"class='open active'");
	}
	/**
	 * Remove the specified teamcategory from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Teamcategory::destroy($id);
		return Redirect::route('admin.team_categories.index')
			->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
			->with('class_team',"class='open active'")
			->with('title','لیست سمت ها');
	}
}
