<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class HomeController extends BaseController {
	private $locale = null;
	public function __construct(){
		$this->locale = App::getLocale();
	}
    public function team($team,$title){
        $team = DB::table('teams')
            ->leftJoin('team_categories','teams.category','=','team_categories.id')
            ->leftJoin('team_profession_categories','teams.profession','=','team_profession_categories.id')
            ->where('teams.'.$this->locale.'_family','=',$title)
            ->get(array(
                'teams.'.$this->locale.'_family',
                'teams.'.$this->locale.'_name',
                'teams.facebook',
                'teams.image',
                'teams.'.$this->locale.'_description',
                'teams.google',
                'teams.linkedin',
                'team_profession_categories.'.$this->locale.'_prof_title',
                'team_categories.'.$this->locale.'_cat_title',
            ));

        return View::make('front.team.team',array('team'=>$team[0]));
    }
    public function contact(){
        return View::make('front.contact.contact');
    }
    public function about(){
        $teams = DB::table('teams')
            ->leftJoin('team_categories', 'team_categories.id', '=', 'teams.category')
            ->leftJoin('team_profession_categories', 'team_profession_categories.id', '=', 'teams.profession')
            ->get(array(
                'teams.image',
                'teams.id',
                'teams.fa_name',
                'teams.en_name',
                'teams.ru_name',
                'teams.en_family',
                'teams.ru_family',
                'teams.fa_family',
                'team_categories.fa_cat_title',
                'team_categories.en_cat_title',
                'team_categories.ru_cat_title',
                'team_profession_categories.fa_prof_title',
                'team_profession_categories.en_prof_title',
                'team_profession_categories.ru_prof_title',
            ));
        return View::make('front.about.about',compact('teams'));
    }
	public function datasheet(){
		return View::make('front.datasheet.datasheet');
	}
    public function datasheet_search(){
        $arr = array();
        foreach (Auth::user()->group as $gp) {
            $arr[] = $gp->id;
        }
        $user_gp = $arr[0];
        $data = Input::get('data');
		try{
            $code = DatasheetCode::where('short','=',$data)->get();
        }catch (ModelNotFoundException $e){
            return Redirect::back()->withInput()->with('notfound','notfound');
        }
        /*echo "<pre>";print_r($code);exit;*/
        $codes = array();
        foreach($code as $c){
            $codes[] = $c->id;
        }
        if(empty($codes)){
            return Redirect::back()->withInput()
                ->with('notfound','notfound')
                ->with('data',$data);
        }
        $datasheet = DB::table('datasheet_datasheet_code')
            ->leftJoin('datasheets','datasheet_datasheet_code.datasheet_id','=','datasheets.id')
            ->where('datasheet_datasheet_code.datasheet_id','=',$codes[0])
            ->get(array(
                'datasheets.'.$this->locale.'_name',
                'datasheets.'.$this->locale.'_description',
                'datasheets.file',
                'datasheets.id',
            ));
        $gp = DB::table('datasheet_group')
            ->leftJoin('groups','datasheet_group.group_id','=','groups.id')
            ->get(array(
                'groups.id'
            ));
        $gps = array();
        foreach($gp as $g){
            $gps[] = $g->id;
        }
        if(!array_search($user_gp,$gps)){
            return Redirect::back()->withInput()
                ->with('gp_access','gp_access')
                ->with('data',$data);
        }
        return View::make('front.datasheet.datasheet',array(
            'datasheet'=>$datasheet,
            'data'=>$data,
        ));
    }
	public function project_page($title,$projects){
        $p = new Portfolio();
		$p = $p->where($this->locale.'_title', '=', $projects)->firstOrFail();
		return View::make('front.projects.project_page',array('project'=>$p));
	}
	public function project(){
		$p_cat = PortfolioCategory::all(array($this->locale.'_pcat_title','id'));
		$p_list = Portfolio::all(array($this->locale.'_title',$this->locale.'_body','img1','id'));
		return View::make('front.projects.portfolio_list')
			->with('p_list',$p_list)
			->with('p_cat',$p_cat)
			->with('url',Lang::get('routes.project',array(),$this->locale))
			->with('title',Lang::get('messages.project'));
	}
	public function service($category,$service){
        $all_cat = ServiceCategory::all();
        $cat = ServiceCategory::where($this->locale.'_title','=',$service)->get(array(
            $this->locale.'_title',
            $this->locale.'_description',
        ));
		$service = DB::table('service_service_category')
			->leftJoin('services','service_service_category.service_id','=','services.id')
			->leftJoin('service_categories','service_service_category.service_category_id','=','service_categories.id')
			->where('service_categories.'.$this->locale.'_title','=',$service)
			->get(array(
                'services.'.$this->locale.'_title',
                'services.'.$this->locale.'_body',
                'services.'.'img','services.'.'id'
            ));
		return View::make('front.service.services',array(
			'service'=>$service,
			'all_cat'=>$all_cat,
			'cat'=>$cat[0],
		));
	}
	public function service_page($title,$category,$service){
		$service = Service::where($this->locale.'_title','=',$service)->firstOrFail();
		return View::make('front.service.service_page',array(
			'service'=>$service,
			'category'=>$category,
		));
	}
	public function news(){
        $new_all = Elearning::paginate(10);
        $last = Elearning::limit(5)->orderBy('created_at')->get();
		$category = ElearningCategory::all(array($this->locale.'_ecat_title','id'));
		return View::make('front.news.news')
			->with('last',$last)
			->with('p_list',$new_all)
			->with('p_cat',$category)
			->with('title',Lang::get('messages.news',array(),$this->locale));
	}
    public function news_cat($learnig,$category){
       $cat_p = DB::table('elearning_elearning_category')
           ->leftJoin('elearnings','elearning_elearning_category.elearning_id','=','elearnings.id')
           ->leftJoin('elearning_categories','elearning_elearning_category.elearning_category_id','=','elearning_categories.id')
           ->where('elearning_categories.'.$this->locale.'_ecat_title','=',$category)
           ->get(array('elearnings.'.$this->locale.'_title','elearnings.'.$this->locale.'_body','elearnings.'.'img','elearnings.'.'id'));
       $last = Elearning::limit(5)->orderBy('created_at')->get();
       $category = ElearningCategory::all(array($this->locale.'_ecat_title','id'));
       /*echo"<pre>";
       print_r($cat_p);exit;*/
        return View::make('front.news.news_cat')
            ->with('p_list',$cat_p)
            ->with('last',$last)
            ->with('p_cat',$category)
            ->with('title',Lang::get('messages.news',array(),$this->locale));
    }
	public function news_page($learning,$category,$title){

		$p = Elearning::where($this->locale.'_title', '=', $title)->firstOrFail();
		return View::make('front.news.news_page',array('news'=>$p));
	}
	public function product(){
		$p_list = Product::all(array($this->locale.'_title',$this->locale.'_description','id','img'));
		$p_cat = ProductCategory::where('parent_id','=',null)->get();
		return View::make('front.products.product_list')
			->with('p_list',$p_list)
			->with('p_cat',$p_cat)
			->with('url',Lang::get('routes.products',array(),$this->locale))
			->with('title',Lang::get('messages.products',array(),$this->locale));
	}
	public function product_page($title,$products){
		$p = Product::where($this->locale.'_title', '=', $products)->firstOrFail();
		return View::make('front.products.product_page',array('product'=>$p));
	}
	public function home(){
		$news = Elearning::limit(6)->get();
		$customers = PortfolioCustomer::all()->count();
		$products = Product::all()->count();
		$external = PortfolioCategory::find(9)->portfolio()->count();
		$internal = PortfolioCategory::find(10)->portfolio()->count();
		$partner = Company::all();
		$about = Page::find(13);	$organization = Page::find(5);
		$glance = Page::find(12);	$vision = Page::find(11);
		$slider=Slider::all(array($this->locale.'_slug1',$this->locale.'_slug2','img'));
		$service = ServiceCategory::all(array($this->locale.'_title',$this->locale.'_description','id','img'));
		$portfolio = Portfolio::where('top','=',1)->get();
		$p_cats = PortfolioCategory::all(array($this->locale.'_pcat_title'));
		$top_project= Lang::get('messages.last_project',array(),$this->locale);
		$services = Lang::get('messages.services',array(),$this->locale);
		return View::make('front.index',array(
			'products'=>$products, 'customers'=>$customers, 'external'=>$external,
			'internal'=>$internal,'partner'=>$partner,
			'p_cats'=>$p_cats,'services'=>$services,
			'organization'=>$organization,
			'top_project'=>$top_project,'about'=>$about,'news'=>$news,'portfolio'=>$portfolio,
			'glance'=>$glance,	'slider'=>$slider,	'service'=>$service, 'vision'=>$vision,
		));
	}
    public function CodeBreaker($code){
        $find = strpos($code,1);
    }
}