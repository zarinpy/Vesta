<?php

class About extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	private $locale = null;
	public function __construct()
	{
		$this->locale = App::getLocale();
	}
	public function about()
	{
		return "about-".App::getLocale();
		$about = Page::find(13);
		return View::make('front.about',compact('about'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function history()
	{
		return "history-".$this->locale;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function certificate()
	{
		return View::make('front.about.certificate');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
