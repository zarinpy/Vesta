<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class ProductCategoriesController extends \BaseController {

	/**
	 * Display a listing of productgategories
	 *
	 */
	public function __construct()
	{
		$this->beforeFilter('csrf', array('on'=>'post'));
	}
	public static function categoryFullArray()
	{
		$categories = ProductCategory::where('parent_id', '=', null)->get();
		$menu = self::createTreeString($categories);
		return $menu;
	}
	public static function createTreeString($category_array)
	{
		if($category_array == null){
			return null;
		}
		$cat_array=array();
		foreach ($category_array as $category) {
			$cat_array[$category->id] = array(
				'id'=>$category->id,
				'fa_name'=>$category->fa_name,
				'en_name'=>$category->en_name,
				'ru_name'=>$category->ru_name,
				'parent_id'=>$category->parent_id,
				'active'=>$category->active
			);

			$categories = ProductCategory::where('parent_id', '=', $category->id)->get();
			if ($categories->count() > 0)
			{
				$cat_array[$category->id]['child'] = self::createTreeString($categories);
			}
			else
				$cat_array[$category->id]['child'] = null;
		}
		return $cat_array;
	}
	public function index()
	{
		$productgategories = ProductCategory::all();
		return View::make('backend.product_categories.index', compact('productgategories'))
		->with('title','لیست دسته بندی')
			->with('menus', self::categoryFullArray())
        ->with('class_product',"class='open active'");
	}
	/**
	 * Show the form for creating a new productgategory
	 *
	 * @return Response
	 */
	public function create()
	{
		$productgategories =self::createTreeString(ProductCategory::all());
		return View::make('backend.product_categories.create',compact('productgategories'))
		->with('title','ایجاد دسته بندی')
			->with('categories', ProductCategory::all())
			->with('menus', self::categoryFullArray())
        ->with('class_product',"class='open active'");
	}

	/**
	 * Store a newly created productgategory in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), ProductCategory::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::get('active') == 'on'){
			$data['active'] = 1;
		}else{
			$data['active'] = 0;
		}
		if(Input::get('parent_id')=='null'){
			$data['parent_id'] = null;
		}else{
			$data['parent_id'] = Input::get('parent_id');
		}
		ProductCategory::create($data);
		return Redirect::route('admin.product_categories.create')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد')
		->with('class_product','class="active open"');
	}
	/**
	 * Display the specified productgategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$productgategory = Productcategory::findOrFail($id);
			return View::make('backend.product_categories.show', compact('productgategory'));
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.product_categories.index');
		}
	}
	/**
	 * Show the form for editing the specified productgategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$productgategory = ProductCategory::find($id);
			return View::make('backend.product_categories.edit', compact('productgategory'))
				->with('class_product',"class='open active'")
				->with('menus', self::categoryFullArray())
				->with('title',"ویرایش دسته بندی");
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.product_categories.index');
		}
	}
	/**
	 * Update the specified productgategory in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$productgategory = ProductCategory::findOrFail($id);
		$validator = Validator::make($data = Input::all(), ProductCategory::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::get('active') == 'on'){
			$data['active'] = 1;
		}else{
			$data['active'] = 0;
		}
		$data['fa_name'] = Input::get('fa_name');
		$data['en_name'] = Input::get('en_name');
		$data['ru_name'] = Input::get('ru_name');
		$productgategory->update($data);
		return Redirect::route('admin.product_categories.index')
		->with('class_product',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}
	/**
	 * Remove the specified productgategory from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		ProductCategory::destroy($id);
		return Redirect::route('admin.product_categories.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_product',"class='open active'");
	}
}
