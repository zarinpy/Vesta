<?php

class ElearningCategoriesController extends \BaseController {

	/**
	 * Display a listing of elearningcategories
	 *
	 * @return Response
	 */
	public function index()
	{
		$elearningcategories = Elearningcategory::all();
		return View::make('backend.elearning_categories.index', compact('elearningcategories'))
		->with('title','لیست دسته بندی')
        ->with('class_elearning',"class='open active'");
	}

	/**
	 * Show the form for creating a new elearningcategory
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.elearning_categories.create')
		->with('title','ایجاد دسته بندی جدید')
        ->with('class_elearning',"class='open active'");
	}

	/**
	 * Store a newly created elearningcategory in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Elearningcategory::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		Elearningcategory::create($data);
		return Redirect::route('admin.elearning_categories.index')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد')
		->with('title','لیست دسته بندی')
		->with('class_elearning','class="active open"');
	}

	/**
	 * Display the specified elearningcategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$elearningcategory = Elearningcategory::findOrFail($id);

		return View::make('backend.elearning_categories.show', compact('elearningcategory'));
	}

	/**
	 * Show the form for editing the specified elearningcategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$elearningcategory = Elearningcategory::find($id);
		return View::make('backend.elearning_categories.edit', compact('elearningcategory'))
		->with('class_elearning',"class='open active'")
		->with('title',"ویرایش دسته بندی");
	}

	/**
	 * Update the specified elearningcategory in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$elearningcategory = Elearningcategory::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Elearningcategory::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$elearningcategory->update($data);
		return Redirect::route('admin.elearning_categories.index')
		->with('class_elearning',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}

	/**
	 * Remove the specified elearningcategory from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Elearningcategory::destroy($id);
		return Redirect::route('admin.elearning_categories.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_elearning',"class='open active'")
		->with('title','لیست دسته بندی');
	}

}
