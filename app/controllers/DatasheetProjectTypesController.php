<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class DatasheetProjectTypesController extends \BaseController {

	/**
	 * Display a listing of datasheetprojecttypes
	 *
	 * @return Response
	 */
	public function index()
	{
		$datasheetprojecttypes = Datasheetprojecttype::all();
		return View::make('backend.datasheet_project_types.index', compact('datasheetprojecttypes'))
		->with('title','لیست نوع داده ها')
        ->with('class_datasheets',"class='open active'");
	}

	/**
	 * Show the form for creating a new datasheetprojecttype
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.datasheet_project_types.create')
        ->with('class_datasheetprojecttypes',"class='open active'")
        ->with('title',"ایجاد نوع جدید");
	}

	/**
	 * Store a newly created datasheetprojecttype in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Datasheetprojecttype::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Datasheetprojecttype::create($data);

		return Redirect::route('admin.datasheet_project_types.index')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد')
		->with('class_datasheets','class="active open"');
	}

	/**
	 * Display the specified datasheetprojecttype.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$datasheetprojecttype = Datasheetprojecttype::findOrFail($id);
            		return View::make('backend.datasheet_project_types.show', compact('datasheetprojecttype'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.datasheet_project_types.index');
		}
	}

	/**
	 * Show the form for editing the specified datasheetprojecttype.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$datasheetprojecttype = Datasheetprojecttype::find($id);
			return View::make('backend.datasheet_project_types.edit', compact('datasheetprojecttype'))
				->with('class_datasheets',"class='open active'");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.datasheet_project_types.index');
		}
	}

	/**
	 * Update the specified datasheetprojecttype in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$datasheetprojecttype = Datasheetprojecttype::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Datasheetprojecttype::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$datasheetprojecttype->update($data);

		return Redirect::route('admin.datasheet_project_types.index')
		->with('class_datasheets',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}

	/**
	 * Remove the specified datasheetprojecttype from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Datasheetprojecttype::destroy($id);

		return Redirect::route('admin.datasheet_project_types.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_datasheets',"class='open active'");
	}

}
