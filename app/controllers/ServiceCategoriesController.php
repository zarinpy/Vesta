<?php
use Intervention\Image\ImageManagerStatic as Imagic;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class ServiceCategoriesController extends \BaseController {

	/**
	 * Display a listing of servicecategories
	 *
	 * @return Response
	 */
	public function index()
	{
		$servicecategories = Servicecategory::all();
		return View::make('backend.service_categories.index', compact('servicecategories'))
		->with('title','لیست دسته بندی')
        ->with('class_service',"class='open active'");
	}

	/**
	 * Show the form for creating a new servicecategory
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.service_categories.create')
			->with('title','ایجاد دسته بندی')
        ->with('class_service',"class='open active'");
	}
	/**
	 * Store a newly created servicecategory in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Servicecategory::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('file')){
			$img1 = Input::file('file');
			$filename1 = $img1->getClientOriginalName();
			$width = Imagic::make($img1)->width();
			$height = Imagic::make($img1)->height();
			if($width == $height){
				$image = Imagic::make($img1)->fit(125,125);
				$thumbnail ="backend/assets/images/service/".$filename1;
				$image->save(public_path($thumbnail));
			}else{
				return Redirect::back()->withInput()->with('img','طول و عرض تصویر کوچک برابر نیست');
			}
			$path1 ="backend/assets/images/service_category/".$filename1;
			Imagic::make($img1->getRealPath())->save(public_path($path1));
		}else{
			$path1 = "https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=125&h=125&txttrack=0";
		}
		$service_category = new ServiceCategory();
		$service_category->fa_title = Input::get('fa_title');
		$service_category->ru_title = Input::get('ru_title');
		$service_category->en_title = Input::get('en_title');
		$service_category->fa_description = Input::get('fa_description');
		$service_category->ru_description = Input::get('ru_description');
		$service_category->en_description = Input::get('en_description');
		$service_category->img = $path1;
		$service_category->save();
		return Redirect::route('admin.service_categories.index')
			->with('success','اطلاعات جدید با موفقیت ذخیره شد')
			->with('class_service','class="active open"');


	}

	/**
	 * Display the specified servicecategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$servicecategory = Servicecategory::findOrFail($id);
            		return View::make('backend.service_categories.show', compact('servicecategory'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.service_categories.index');
		}
	}

	/**
	 * Show the form for editing the specified servicecategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$servicecategory = Servicecategory::find($id);
			return View::make('backend.service_categories.edit', compact('servicecategory'))
				->with('class_service',"class='open active'")
				->with('title',"ویرایش دسته بندی");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.service_categories.index');
		}
	}

	/**
	 * Update the specified servicecategory in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$servicecategory = Servicecategory::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Servicecategory::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('file')){
			$img1 = Input::file('file');
			$filename_img1 = $img1->getClientOriginalName();
			$path1 ="backend/assets/images/service/".$filename_img1;
			File::delete(array(public_path($servicecategory->img)));
			Imagic::make($img1->getRealPath())->save(public_path($path1));
			$data['file'] = $path1;
		}else{
			$data['file']=$servicecategory->img;
		}
		$servicecategory->fa_title = Input::get('fa_title');
		$servicecategory->fa_description = Input::get('fa_description');
		$servicecategory->en_title = Input::get('en_title');
		$servicecategory->en_description = Input::get('en_description');
		$servicecategory->ru_title = Input::get('ru_title');
		$servicecategory->ru_description = Input::get('ru_description');
		$servicecategory->img = $data['file'];
		$servicecategory->save();
		return Redirect::route('admin.service_categories.index')
		->with('class_service',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}

	/**
	 * Remove the specified servicecategory from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$service = Service::find($id);
		if(File::exists($service->img)){
			File::delete(array(public_path($service->img)));
		}
		Servicecategory::destroy($id);
		return Redirect::route('admin.service_categories.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_service',"class='open active'");
	}
}
