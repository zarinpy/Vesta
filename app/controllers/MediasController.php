<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MediasController extends \BaseController {
	/**
	 * Display a listing of medias
	 *
	 * @return Response
	 */
	public function index()
	{
		$medias = Medias::all();

		return View::make('backend.medias.index', compact('medias'))
		->with('title','لیست رساانه ها')
        ->with('class_media',"class='open active'");
	}

	/**
	 * Show the form for creating a new media
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.medias.create')
		->with('title','بارگزاری رسانه')
        ->with('class_media',"class='open active'");
	}
	/**
	 * Store a newly created media in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Medias::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
			if (Input::hasFile('file1')) {
				$file1 = Input::file('file1');
				$filename1 =$file1->getClientOriginalName();
				try{
					switch (Input::get('category')) {
						case '11':
							$path1 = "backend/assets/images/medias/image/";
							break;
						case '21':
							$path1 = "backend/assets/images/medias/pdf/" ;
							break;
						case '31':
							$path1 = "backend/assets/images/medias/zip_rar/" ;
							break;
						case '41':
							$path1 = "backend/assets/images/medias/doc/" ;
							break;
						case '51':
							$path1 = "backend/assets/images/medias/excel/" ;
							break;
						case '61':
							$path1 = "backend/assets/images/medias/eplan/" ;
							break;
						case '71':
							$path1 = "backend/assets/images/medias/other/" ;
							break;
						default :
							$path1 = "backend/assets/images/medias/";
					}
					$media = new Medias();
					$media->m_title = Input::get('title');
					$media->address = $path1;
					$media->category = Input::get('category');
					$media->save();
					//echo "<pre>";	print_r($data);exit();
					$file1->move(public_path().'/'.$path1,$filename1);
					Medias::create(array(
						'address' => $path1.''.$filename1,
						'category' => Input::get('category'),
						'm_title' => Input::get('title')
					));
					return Redirect::route('admin.medias.create')
						->with('success','اطلاعات جدید با موفقیت ذخیره شد')
						->with('title','لیست رسانه ها')
						->with('class_media','class="active open"');
				}
				catch(Exception $e){
					return Redirect::back()->with('warning','سیستم نمی تواند فایل ارسالی را بخواند اگر پسوند فایل tar.gz یا zip است نوع آن را  به rar تبدیل کنید');
				}
			}
			else{
				return Redirect::back()->withErrors($validator)->with('warning','مشکلی در بارگزاری فایل بوجود آمده است')->withInput();
			}
	}
	/**
	 * Display the specified media.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$media = Medias::findOrFail($id);
			return View::make('backend.medias.show', compact('media'));
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.medias.index');
		}
	}

	/**
	 * Show the form for editing the specified media.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$media = Medias::find($id);
			return View::make('backend.medias.edit', compact('media'))
				->with('class_media',"class='open active'")
				->with('title',"ویرایش رسانه");
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.medias.index');
		}

	}

	/**
	 * Update the specified media in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$media = Medias::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Medias::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$media->update($data);

		return Redirect::route('admin.medias.index')
		->with('class_media',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}

	/**
	 * Remove the specified media from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Medias::destroy($id);

		return Redirect::route('admin.medias.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_media',"class='open active'")
		->with('title','لیست رسانه ها');
	}

}
