<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Intervention\Image\ImageManagerStatic as Imagic;
class DatasheetsController extends \BaseController {
	/**
	 * Display a listing of datasheets
	 *
	 * @return Response
	 */
	public function index()
	{
		$datasheets = Datasheet::all();
		return View::make('backend.datasheets.index', compact('datasheets'))
		->with('title','لیست اطلاعات')
        ->with('class_datasheets',"class='open active'");
	}
	/**
	 * Show the form for creating a new datasheet
	 *
	 * @return Response
	 */
	public function create()
	{
		$code = DatasheetCode::all();
		$gp = Group::all();
		return View::make('backend.datasheets.create',compact('code','gp'))
		->with('title','ایجاد داده جدید')
        ->with('class_datasheets',"class='open active'");
	}
	/**
	 * Store a newly created datasheet in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$validator = Validator::make($data = Input::all(), Datasheet::$rules);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('file')){
			if(Input::file('file')->isValid()){
				$img1 = Input::file('file');
				$filename1 = $img1->getClientOriginalName();
				$path1 ="backend/assets/images/datasheet/".$filename1;
				$dir = "backend/assets/images/datasheet/";
				$img1->move(public_path().'/'.$dir,$filename1);
				$datasheet = Datasheet::create(array(
					'fa_name'=>Input::get('fa_name'),
					'file'=>$path1,
					'en_name'=>Input::get('en_name'),
					'en_description'=>Input::get('en_description'),
					'fa_description'=>Input::get('fa_description'),
				));
				$id = $datasheet->id;
				$group = new Datasheet();
				$group->find($id)->group()->attach(Input::get('group'));
				$code = new Datasheet();
				$code->find($id)->code()->attach(Input::get('code'));
				return Redirect::route('admin.datasheets.index')
					->with('success','اطلاعات جدید با موفقیت ذخیره شد')
					->with('class_datasheets','class="active open"');
			}
		}
		return Redirect::back()->withInput()->with('file','بارگزاری فایل اجباریست');
	}
	/**
	 * Display the specified datasheet.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$datasheet = Datasheet::findOrFail($id);
            		return View::make('backend.datasheets.show', compact('datasheet'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.$COLLECTION.index');
		}
	}

	/**
	 * Show the form for editing the specified datasheet.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$datasheet = Datasheet::find($id);
			return View::make('backend.datasheets.edit', compact('datasheet'))
				->with('class_datasheets',"class='open active'")
				->with('title',"ویرایش اطلاعات");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.datasheets.index');
		}
	}

	/**
	 * Update the specified datasheet in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$datasheet = Datasheet::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Datasheet::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$datasheet->update($data);

		return Redirect::route('admin.datasheets.index')
		->with('class_datasheets',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}

	/**
	 * Remove the specified datasheet from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Datasheet::destroy($id);
		return Redirect::route('admin.datasheets.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_datasheets',"class='open active'");
	}
}
