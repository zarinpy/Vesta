<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class TeamProfessionCategoriesController extends \BaseController {

	/**
	 * Display a listing of team_profession_categories
	 *
	 * @return Response
	 */
	public function index()
	{
		$teamprofessioncategories = Teamprofessioncategory::all();
		return View::make('backend.team_profession_categories.index', compact('teamprofessioncategories'))
			->with('class_team',"class='open active'")
			->with('title','لیست حرفه ها');
	}
	/**
	 * Show the form for creating a new team_profession_categories
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.team_profession_categories.create')
			->with('class_team',"class='open active'")
			->with('title','ایجاد حرفه جدید');
	}
	/**
	 * Store a newly created team_profession_categories in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Teamprofessioncategory::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		Teamprofessioncategory::create($data);
		return Redirect::route('admin.team_profession_categories.index')
			->with('success','اطلاعات با موفقیت ذخیره شد')
			->with('class_team',"class='open active'");
	}
	/**
	 * Display the specified team_profession_categories.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$teamprofessioncategory = Teamprofessioncategory::findOrFail($id);
			return View::make('backend.team_profession_categories.show', compact('teamprofessioncategory'));
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.team_profession_categories.index');
		}

	}
	/**
	 * Show the form for editing the specified team_profession_categories.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$teamprofessioncategory = Teamprofessioncategory::find($id);
			return View::make('backend.team_profession_categories.edit', compact('teamprofessioncategory'))
				->with('title','ویرایش حرفه')
				->with('class_team',"class='open active'");
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.team_profession_categories.index');
		}
	}
	/**
	 * Update the specified team_profession_categories in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$teamprofessioncategory = Teamprofessioncategory::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Teamprofessioncategory::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$teamprofessioncategory->update($data);
		return Redirect::route('admin.team_profession_categories.index')
			->with('class_team',"class='open active'")
			->with('update','اطلاعات با موفقیت ویرایش شد')
			->with('class_team',"class='open active'");
	}

	/**
	 * Remove the specified team_profession_categories from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Teamprofessioncategory::destroy($id);
		return Redirect::route('admin.team_profession_categories.index')
			->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
			->with('class_team',"class='open active'");
	}

}
