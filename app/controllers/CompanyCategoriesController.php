<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class CompanyCategoriesController extends \BaseController {

	/**
	 * Display a listing of companycategories
	 *
	 * @return Response
	 */
	public function index()
	{
		$companycategories = Companycategory::all();
		return View::make('backend.company_categories.index', compact('companycategories'))
		->with('title','لیست دسته بندی ها')
        ->with('class_portfolio',"class='open active'");
	}
	/**
	 * Show the form for creating a new companycategory
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.company_categories.create')
			->with('title','ایجاد دسته بندی')
        ->with('class_portfolio',"class='open active'");
	}
	/**
	 * Store a newly created companycategory in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Companycategory::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		Companycategory::create($data);
		return Redirect::route('admin.company_categories.index')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد')
		->with('class_portfolio','class="active open"');
	}

	/**
	 * Display the specified companycategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$companycategory = Companycategory::findOrFail($id);
            		return View::make('backend.company_categories.show', compact('companycategory'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.company_categories.index');
		}
	}

	/**
	 * Show the form for editing the specified companycategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$companycategory = Companycategory::find($id);
			return View::make('backend.company_categories.edit', compact('companycategory'))
				->with('class_portfolio',"class='open active'")
				->with('title',"ویرایش دسته بندی");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.company_categories.index');
		}
	}

	/**
	 * Update the specified companycategory in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$companycategory = Companycategory::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Companycategory::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$companycategory->update($data);
		return Redirect::route('admin.company_categories.index')
		->with('class_company',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}

	/**
	 * Remove the specified companycategory from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Companycategory::destroy($id);
		return Redirect::route('admin.companycategories.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_company',"class='open active'");
	}

}
