<?php
use Intervention\Image\ImageManagerStatic as Imagic;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class SlidersController extends \BaseController {

	/**
	 * Display a listing of sliders
	 *
	 * @return Response
	 */
	public function index()
	{
		$sliders = Slider::all();
		return View::make('backend.sliders.index', compact('sliders'))
		->with('title','لیست تصاویر')
        ->with('class_slider',"class='open active'");
	}

	/**
	 * Show the form for creating a new slider
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.sliders.create')
			->with('title','ایجاد اسلاید')
        ->with('class_slider',"class='open active'");
	}

	/**
	 * Store a newly created slider in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		/*echo "<pre>";
		print_r(Input::all());exit();*/
		$validator = Validator::make($data = Input::all(), Slider::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('file')){
			$img1 = Input::file('file');
			$filename1 = $img1->getClientOriginalName();
			$path1 ="backend/assets/images/slider/".$filename1;
			Imagic::make($img1->getRealPath())->save(public_path($path1));
			$slider = new Slider();
			$slider->fa_slug1 = Input::get('fa_slug1');
			$slider->fa_slug2 = Input::get('fa_slug2');
			$slider->en_slug1 = Input::get('en_slug1');
			$slider->en_slug2 = Input::get('en_slug2');
			$slider->ru_slug1 = Input::get('ru_slug1');
			$slider->ru_slug2 = Input::get('ru_slug2');
			$slider->animate = Input::get('animate');
			$slider->img = $path1;
			$slider->save();
			return Redirect::route('admin.sliders.index')
				->with('success','اطلاعات جدید با موفقیت ذخیره شد')
				->with('class_slider','class="active open"');
		}else{
			return Redirect::back()->with('img','تصویر عضو الزامیست');
		}
	}

	/**
	 * Display the specified slider.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$slider = Slider::findOrFail($id);
            		return View::make('backend.sliders.show', compact('slider'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.class_sliders.index');
		}
	}

	/**
	 * Show the form for editing the specified slider.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$slider = Slider::find($id);
			return View::make('backend.sliders.edit', compact('slider'))
				->with('class_sliders',"class='open active'")
				->with('title',"ویرایش اسلاید");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.class_sliders.index');
		}
	}

	/**
	 * Update the specified slider in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$slider = Slider::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Slider::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('file')){
			$img1 = Input::file('file');
			$filename_img1 = time().$img1->getClientOriginalName();
			$path1 ="backend/assets/images/slider/".$filename_img1;
			File::delete(array(public_path($slider->file)));
			Imagic::make($img1->getRealPath())->save(public_path($path1));
			$data['file'] = $path1;
		}else{
			$data['file']=$slider->file;
		}
		$slider->fa_slug1 = Input::get('fa_slug1');
		$slider->fa_slug2 = Input::get('fa_slug2');
		$slider->en_slug1 = Input::get('en_slug1');
		$slider->en_slug2 = Input::get('en_slug2');
		$slider->ru_slug1 = Input::get('ru_slug1');
		$slider->ru_slug2 = Input::get('ru_slug2');
		$slider->animate = Input::get('animate');
		$slider->save();
		return Redirect::route('admin.sliders.index')
		->with('class_slider',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}

	/**
	 * Remove the specified slider from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$slider = Slider::find($id);
		File::delete(array(public_path($slider->file)));
		Slider::destroy($id);
		return Redirect::route('admin.sliders.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_slider',"class='open active'");
	}
}
