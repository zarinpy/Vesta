<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Intervention\Image\ImageManagerStatic as Imagic;
class TeamsController extends \BaseController {

	/**
	 * Display a listing of teams
	 *
	 * @return Response
	 */
	public function __construct()
	{
		$this->beforeFilter('csrf', array('on' => 'post'));
	}
	public function index()
	{
		$teams = DB::table('teams')
			->leftJoin('team_categories', 'team_categories.id', '=', 'teams.category')
			->leftJoin('team_profession_categories', 'team_profession_categories.id', '=', 'teams.profession')
			->get(array('teams.image','teams.id','teams.fa_name','teams.fa_family','team_categories.fa_cat_title','team_profession_categories.fa_prof_title'));
		return View::make('backend.teams.index', compact('teams'))
			->with('class_team',"class='open active'")
			->with('title','لیست اعضای تیم');
	}
	/**
	 * Show the form for creating a new team
	 *
	 * @return Response
	 */
	public function create()
	{
		$category = TeamCategory::lists('fa_cat_title','id');
		$profession = TeamProfessionCategory::lists('fa_prof_title','id');
		return View::make('backend.teams.create',array('category'=>$category,'profession'=>$profession))
			->with('class_team',"class='open active'")
			->with('title','ایجاد عضو جدید');
	}
	/**
	 * Store a newly created team in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Team::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('image')){
			$imagefile = Input::file('image');
			$filename = $imagefile->getClientOriginalName();
			$path ="backend/assets/images/team/".$filename;
			$width = Imagic::make($imagefile)->width();
			$height = Imagic::make($imagefile)->height();
            $lens = $width - $height;
			if($lens >= 200){
				$image = Imagic::make($imagefile);
				$image->save(public_path().'/'.$path);
			}else{
				return Redirect::back()->withInput()->with('img','طول و عرض تصویر کوچک برابر نیست');
			}
		}else{
			$path = "https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=300&h=300&txttrack=0";
		}
		$team = new Team();
		$team->ru_name = Input::get('ru_name');				$team->ru_family = Input::get('ru_family');
		$team->image = $path; 								$team->fa_name = Input::get('fa_name');
		$team->en_name = Input::get('en_name');				$team->en_family = Input::get('en_family');
		$team->fa_family = Input::get('fa_family'); 		$team->facebook = Input::get('facebook');
		$team->linkedin = Input::get('linkedin'); 			$team->google = Input::get('google');
		$team->en_description = Input::get('en_description');$team->category = Input::get('category');
		$team->fa_description = Input::get('fa_description');
		$team->ru_description = Input::get('ru_description');
		$team->profession = Input::get('profession');	$team->save();
		return Redirect::route('admin.teams.index')
			->with('success','اطلاعات جدید با موفقیت ذخیره شد')
			->with('class_team',"class='open active'")
			->with('title','لیست اعضای تیم');
	}
	/**
	 * Display the specified team.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$team = Team::findOrFail($id);
			return View::make('backend.teams.show', compact('team'));
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.teams.index');
		}
	}
	/**
	 * Show the form for editing the specified team.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$team = Team::findOrFail($id);
			$category = TeamCategory::lists('fa_cat_title','id');
			$profession = TeamProfessionCategory::lists('fa_prof_title','id');
			return View::make('backend.teams.edit',array('team'=>$team,'category'=>$category,'profession'=>$profession))
				->with('class_team',"class='open active'")
				->with('title','ویرایش اعضای تیم');
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.teams.index');
		}
	}

	/**
	 * Update the specified team in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$team = Team::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Team::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('image')){
			$team = Team::find($id);
			File::delete(array(public_path($team->image)));
			$imagefile = Input::file('image');
			$filename = time().$imagefile->getClientOriginalName();
			$path ="backend/assets/images/team/".$filename;
			$team->image = Input::file('image');  $team->image = $path;
			Imagic::make($imagefile->getRealPath())->save(public_path().'/'.$path);
		}
		$team->ru_name = Input::get('ru_name');	  $team->ru_family = Input::get('ru_family');
		$team->fa_name = Input::get('fa_name');	  $team->fa_family = Input::get('fa_family'); 	$team->facebook = Input::get('facebook');
		$team->en_name = Input::get('en_name');	  $team->en_family = Input::get('en_family');	$team->en_description = Input::get('en_description');
		$team->google = Input::get('google'); $team->linkedin = Input::get('linkedin'); $team->fa_description = Input::get('fa_description');
		$team->ru_description = Input::get('ru_description');
		$team->category = Input::get('category'); $team->profession = Input::get('profession');
		$team->save();
		return Redirect::route('admin.teams.index')
			->with('update','اطلاعات با موفقیت ویرایش شد')
			->with('class_team',"class='open active'")
			->with('title','لیست اعضای تیم');
	}

	/**
	 * Remove the specified team from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$team = Team::find($id);
		if(File::exists($team->image)){
			File::delete(array(public_path($team->image)));
		}
		Team::destroy($id);
		return Redirect::route('admin.teams.index')
			->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
			->with('class_team',"class='open active'")
			->with('title','لیست اعضای تیم');
	}
}
