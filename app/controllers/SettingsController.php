<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class SettingsController extends \BaseController {
	/**
	 * Display a listing of settings
	 *
	 * @return Response
	 */
	public function index()
	{
		$settings = Setting::all();
		return View::make('backend.settings.index', compact('settings'))
		->with('title','لیست تنظیمات')
        ->with('class_setting',"class='open active'");
	}

	/**
	 * Show the form for creating a new setting
	 *
	 * @return Response
	 */
	public function create()
	{
		$about1 = Setting::find(1);$about2 = Setting::find(2);
		return View::make('backend.settings.create',array(
			'about1'=>$about1
		))
			->with('title',' تنظیمات')
        ->with('class_setting',"class='open active'");
	}
	/**
	 * Store a newly created setting in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		DB::table('settings')->where('id', 10)
			->update(array('value' => Input::get('fa_description')));
		DB::table('settings')->where('id', 11)
			->update(array('value' => Input::get('fa_keywords')));
		DB::table('settings')->where('id', 12)
			->update(array('value' => Input::get('en_keywords')));
		DB::table('settings')->where('id', 13)
			->update(array('value' => Input::get('en_description')));
		DB::table('settings')->where('id', 14)
			->update(array('value' => Input::get('ru_description')));
		DB::table('settings')->where('id', 15)
			->update(array('value' => Input::get('ru_keywords')));
		DB::table('settings')->where('id', 20)
			->update(array('value' => Input::get('fa_slug')));
		DB::table('settings')->where('id', 21)
			->update(array('value' => Input::get('en_slug')));
		DB::table('settings')->where('id', 22)
			->update(array('value' => Input::get('ru_slug')));
		return Redirect::route('admin.settings.create')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد')
		->with('class_setting','class="active open"');
	}
	/**
	 * Display the specified setting.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$setting = Setting::findOrFail($id);
            		return View::make('backend.settings.show', compact('setting'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.settings.index');
		}
	}

	/**
	 * Show the form for editing the specified setting.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$setting = Setting::find($id);
			return View::make('backend.settings.edit', compact('setting'))
				->with('class_settings',"class='open active'")
				->with('title',"ویرایش تنظیمات");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.$COLLECTION.index');
		}
	}

	/**
	 * Update the specified setting in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$setting = Setting::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Setting::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$setting->update($data);
		return Redirect::route('admin.settings.index')
		->with('class_setting',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}
	/**
	 * Remove the specified setting from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Setting::destroy($id);
		return Redirect::route('admin.settings.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_setting',"class='open active'");
	}
}
