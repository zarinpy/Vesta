<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class DatasheetCodesController extends \BaseController {

	/**
	 * Display a listing of datasheetcodes
	 *
	 * @return Response
	 */
	public function index()
	{
		$datasheetcodes = Datasheetcode::all();
		return View::make('backend.datasheet_codes.index', compact('datasheetcodes'))
		->with('title','لیست سریال محصولات')
        ->with('class_datasheets',"class='open active'");
	}

	/**
	 * Show the form for creating a new datasheetcode
	 *
	 * @return Response
	 */
	public function create()
	{
		$day = array();
		$year = array();
		$month = array();
		for($i=1;$i<=31;$i++){
			$day[$i]=$i;
		}
		for($i=2012;$i<=2099;$i++){
			$year[$i]=$i;
		}
		for($i=1;$i<=12;$i++){
			$month[$i]=$i;
		}
		$company = DatasheetCompany::all();
		$type = DatasheetProjectType::all();
		return View::make('backend.datasheet_codes.create',array(
			'type'=>$type,
			'company'=>$company,
			'day'=>$day,
			'year'=>$year,
			'month'=>$month))
		->with('title','ایجاد سریال محصول')
        ->with('class_datasheets',"class='open active'");
	}

	/**
	 * Store a newly created datasheetcode in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$data = Input::all();
		if($data['day'][0]==0){
			return Redirect::back()->with('warning','فیلد روز نمی تواند خالی باشد')->withInput();
		}
		if($data['year'][0]==0){
			return Redirect::back()->with('warning','فیلد سال نمی تواند خالی باشد')->withInput();
		}
		if($data['month'][0]==0){
			return Redirect::back()->with('warning','فیلد ماه نمی تواند خالی باشد')->withInput();
		}
		if($data['company'][0]==0){
			return Redirect::back()->with('warning','فیلد شرکت نمی تواند خالی باشد')->withInput();
		}
		if($data['type'][0]==0){
			return Redirect::back()->with('warning','فیلد نوع داده نمی تواند خالی باشد')->withInput();
		}
        if($data['extra']!=null){
            $extra = '-'.$data['extra'];
        }else{
            $extra='';
        }
		$company = DatasheetCompany::find($data['company'][0]);
		$type = DatasheetProjectType::find($data['type'][0]);
		Datasheetcode::create(array(
			"short"=>$company->short.'-'.$data['day'][0].$data['year'][0].$data['month'][0].'-'.$type->short.$extra,
		));
		return Redirect::route('admin.datasheet_codes.index')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد')
		->with('class_datasheets','class="active open"');
	}

	/**
	 * Display the specified datasheetcode.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$datasheetcode = Datasheetcode::findOrFail($id);
			return View::make('backend.datasheet_codes.show', compact('datasheetcode'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.datasheetcodes.index');
		}
	}

	/**
	 * Show the form for editing the specified datasheetcode.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$datasheetcode = Datasheetcode::find($id);
			return View::make('backend.datasheet_codes.edit', compact('datasheetcode'))
				->with('class_datasheets',"class='open active'")
				->with('title',"ویرایش سریال محصول");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.datasheet_codes.index');
		}
	}

	/**
	 * Update the specified datasheetcode in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$datasheetcode = Datasheetcode::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Datasheetcode::$rules);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$datasheetcode->update($data);
		return Redirect::route('admin.datasheet_codes.index')
		->with('class_datasheets',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}
	/**
	 * Remove the specified datasheetcode from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Datasheetcode::destroy($id);
		return Redirect::route('admin.datasheetcodes.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_datasheets',"class='open active'");
	}
}
