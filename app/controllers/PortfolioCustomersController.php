<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class PortfolioCustomersController extends \BaseController {

	/**
	 * Display a listing of portfoliocustomers
	 *
	 * @return Response
	 */
	public function index()
	{
		$portfoliocustomers = Portfoliocustomer::all();
		return View::make('backend.portfolio_customers.index', compact('portfoliocustomers'))
		->with('title','لیست شرکت ها')
        ->with('class_portfolio',"class='open active'");
	}

	/**
	 * Show the form for creating a new portfoliocustomer
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.portfolio_customers.create')
			->with('title','ایجاد شرکت')
        ->with('class_portfolio',"class='open active'");
	}

	/**
	 * Store a newly created portfoliocustomer in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Portfoliocustomer::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		PortfolioCustomer::create(array(
			'fa_name'=>Input::get('fa_name'),
			'en_name'=>Input::get('en_name'),
			'ru_name'=>Input::get('ru_name'),

		));

		return Redirect::route('admin.portfolio_customers.index')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد')
		->with('class_portfolio','class="active open"');
	}

	/**
	 * Display the specified portfoliocustomer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$portfoliocustomer = Portfoliocustomer::findOrFail($id);
            		return View::make('backend.portfolio_customers.show', compact('portfoliocustomer'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.portfolio_customers.index');
		}
	}

	/**
	 * Show the form for editing the specified portfoliocustomer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$portfoliocustomer = Portfoliocustomer::find($id);
			return View::make('backend.portfolio_customers.edit', compact('portfoliocustomer'))
				->with('class_portfolio',"class='open active'")
				->with('title',"ویرایش شرکت");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.$COLLECTION.index');
		}
	}

	/**
	 * Update the specified portfoliocustomer in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$portfoliocustomer = Portfoliocustomer::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Portfoliocustomer::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$portfoliocustomer->fa_name = Input::get('fa_name');
		$portfoliocustomer->en_name = Input::get('en_name');
		$portfoliocustomer->ru_name = Input::get('ru_name');
		$portfoliocustomer->save();

		return Redirect::route('admin.portfolio_customers.index')
		->with('class_portfolio',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}

	/**
	 * Remove the specified portfoliocustomer from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Portfoliocustomer::destroy($id);
		return Redirect::route('admin.portfolio_customers.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_portfolio',"class='open active'");
	}
}
