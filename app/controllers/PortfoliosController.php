<?php
use Intervention\Image\ImageManagerStatic as Imagic;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class PortfoliosController extends \BaseController {
	public function __construct()
	{
		$this->beforeFilter('csrf', array('on' => 'post'));
	}
	public function index()
	{
		$portfolios = Portfolio::all();
		return View::make('backend.portfolios.index', compact('portfolios'))
		->with('title','لیست پروژه ها')
        ->with('class_portfolio',"class='open active'");
	}
	public function create()
	{
		$customer = PortfolioCustomer::all();
		$category = portfolioCategory::all();
		return View::make('backend.portfolios.create',array('category'=>$category,'customer'=>$customer))
		->with('title','ایجاد پروژه جدید')
        ->with('class_portfolio',"class='open active'");
	}
	public function store(){
		$path1 = null;$path2 = null;$path3 = null;$thumbnail = null;
		$validator = Validator::make($data = Input::all(), Portfolio::$rules);
		if ($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput();
		}
        if(empty(Input::get('category'))){
            return Redirect::back()->withInput()
                ->with('category','دسته بندی را انتخاب کنید');
        }
        if(empty(Input::get('customer'))){
            return Redirect::back()->withInput()
                ->with('customer','مشتری را انتخاب کنید');
        }
		if(Input::hasFile('img1')){
			$imagefile1 = Input::file('img1');
			$filename1 = $imagefile1->getClientOriginalName();
			$width1 = Imagic::make($imagefile1)->width();
			$height1 = Imagic::make($imagefile1)->height();
			$image1 = Imagic::make($imagefile1);
            $lens = $width1 - $height1;
			if(($width1 == $height1) or ($height1 > $width1)){
				return Redirect::back()->withInput()->with('img','تصویر اول یا پرتره است یا طول و عرضش برابرست');
			}elseif($lens >= 200){
				$path1 ="backend/assets/images/portfolio/".$filename1;
				$image1->save(public_path($path1));
			}
		}else{
			$path1 = "https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=1920&h=900&txttrack=0";
		}
		/*****************************************************************************************************/
		if(Input::hasFile('img2')){
			$imagefile2 = Input::file('img2');
			$filename2 = $imagefile2->getClientOriginalName();
			$width2 = Imagic::make($imagefile2)->width();
			$height2 = Imagic::make($imagefile2)->height();
			$image2 = Imagic::make($imagefile2);
            $lens = $width2 - $height2;
			/***************************************************************************************************/
			if(($width2 == $height2) or ($height2 > $width2)){
				return Redirect::back()->withInput()->with('img','تصویر دوم یا پرتره است یا طول و عرضش برابرست');
			}elseif($lens >= 200){
				$path2 ="backend/assets/images/portfolio/".$filename2;
				$image2->save(public_path($path2));
			}
		}
		else{
			$path2 = "https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=1920&h=900&txttrack=0";
		}
		/***************************************************************************************************/
		if(Input::hasFile('img3')){
			$imagefile3 = Input::file('img3');
			$filename3 = $imagefile3->getClientOriginalName();
			$width3 = Imagic::make($imagefile3)->width();
			$height3 = Imagic::make($imagefile3)->height();
			$image3 = Imagic::make($imagefile3);
            $lens = $width3 - $height3;
			if(($width3 == $height3) or ($height3 > $width3)){
				return Redirect::back()->withInput()->with('img','تصویر سوم یا پرتره است یا طول و عرضش برابرست');
			}elseif($lens >= 200){
				$path3 ="backend/assets/images/portfolio/".$filename3;
				$image3->save(public_path($path3));
			}
		}else{
			$path3 = "https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=1920&h=900&txttrack=0";
		}
		if(Input::hasFile('thumbnail')){
			$imagefile = Input::file('thumbnail');
			$filename = $imagefile->getClientOriginalName();
			$width = Imagic::make($imagefile)->width();
			$height = Imagic::make($imagefile)->height();
			if($width == $height){
				$image = Imagic::make($imagefile)->fit(200,200);
				$thumbnail ="backend/assets/images/portfolio/thumbnail/".$filename;
				$image->save(public_path($thumbnail));
			}
			else{
				return Redirect::back()->withInput()->with('img','طول و عرض تصویر کوچک برابر نیست');
			}
		}else{
			$thumbnail = "https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=%D8%A8%D8%AF%D9%88%D9%86+%D8%AA%D8%B5%D9%88%DB%8C%D8%B1&w=200&h=200&txttrack=0";
		}
		if(Input::get('top') == 'on'){
			$data['top'] = 1;
		}else{
			$data['top'] = 0;
		}
		$portfolio = Portfolio::create(array(
			'fa_title'=>Input::get('fa_title'),
			'en_title'=>Input::get('en_title'),
			'ru_title'=>Input::get('ru_title'),
			'fa_body'=>Input::get('fa_body'),
			'en_body'=>Input::get('en_body'),
			'ru_body'=>Input::get('ru_body'),
			'thumbnail'=>$thumbnail,
			'img1'=>$path1,
			'img2'=>$path2,
			'top'=>$data['top'],
			'img3'=>$path3));
		$id = $portfolio->id;
		$category = new  Portfolio();
		$category->find($id)->category()->sync(Input::get('category'));
		$customer = new  Portfolio();
		$customer->find($id)->customer()->sync(Input::get('customer'));
		return Redirect::route('admin.portfolios.index')
			->with('success','اطلاعات جدید با موفقیت ذخیره شد')
			->with('title','لیست پروژه ها')
			->with('class_portfolio','class="active open"');
	}
	public function show($id){
		try{
			$portfolio = Portfolio::findOrFail($id);
			return View::make('backend.portfolios.show', compact('portfolio'));
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.portfolios.index');
		}
	}
	public function edit($id){
		try{
			$customer = PortfolioCustomer::all();
			$portfolio = Portfolio::find($id);
			$category = portfolioCategory::all();
			$portfoliocustomers = $portfolio->customer;
            $portfoliocategory = $portfolio->category;
			foreach($portfoliocategory as $pcat){
				$result_pcat[]=$pcat->fa_pcat_title;
			}
            foreach ($portfoliocustomers as $cus) {
                $result_ccat[]=$cus->fa_name;
            }

            return View::make('backend.portfolios.edit', array(
				'customer'=>$customer,
				'category'=>$category,
				'portfolio'=>$portfolio,
				'portfolio_category'=>$portfoliocategory,
				'result_pcat'=>$result_pcat,
				'result_ccat'=>$result_ccat,
            ))
				->with('class_portfolio',"class='open active'")
				->with('title',"ویرایش پروژه");
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.portfolios.index');
		}
	}
	public function update($id){
		$portfolio=  Portfolio::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Portfolio::$rules);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('img1')){
			$img1 = Input::file('img1');
			$filename_img1 = $img1->getClientOriginalName();
			$path1 ="backend/assets/images/portfolio/".$filename_img1;
			File::delete(array(public_path($portfolio->img1)));
			Imagic::make($img1->getRealPath())->save(public_path($path1));
			$data['img1'] = $path1;
		}
		else{
			$data['img1']=$portfolio->img1;
		}
		if(Input::hasFile('img2')){
			$img2 = Input::file('img2');
			$filename_img2 = $img2->getClientOriginalName();
			$path2 ="backend/assets/images/portfolio/".$filename_img2;
			File::delete(array(public_path($portfolio->img2)));
			Imagic::make($img2->getRealPath())->save(public_path($path2));
			$data['img2'] = $path2;
		}
		else{
			$data['img2']=$portfolio->img2;
		}
		if(Input::hasFile('img3')){
			$img3 = Input::file('img3');
			$filename_img3 = $img3->getClientOriginalName();
			$path3 ="backend/assets/images/portfolio/".$filename_img3;
			File::delete(array(public_path($portfolio->img3)));
			Imagic::make($img3->getRealPath())->save(public_path($path3));
			$data['img3'] = $path3;
		}
		else{
			$data['img3']=$portfolio->img3;
		}
		if(Input::hasFile('thumbnail')){
			$img3 = Input::file('thumbnail');
			$filename_img3 = $img3->getClientOriginalName();
			$path3 ="backend/assets/images/portfolio/thumbnail/".$filename_img3;
			File::delete(array(public_path($portfolio->img3)));
			Imagic::make($img3->getRealPath())->save(public_path($path3));
			$data['thumbnail'] = $path3;
		}else{
			$data['thumbnail']=$portfolio->thumbnail;
		}if(Input::get('top') == 1){
			$data['top'] = 1;
		}else{
			$data['top'] = 0;
		}
		$portfolio->thumbnail = $data['thumbnail'];
		$portfolio->img1 = $data['img1'];
		$portfolio->img2 = $data['img2'];
		$portfolio->img3 = $data['img3'];
		$portfolio->fa_body = Input::get('fa_body');
		$portfolio->ru_body = Input::get('ru_body');
		$portfolio->en_body = Input::get('en_body');
		$portfolio->en_title = Input::get('en_title');
		$portfolio->fa_title = Input::get('fa_title');
		$portfolio->ru_title = Input::get('ru_title');
		$portfolio->top = $data['top'];
		$portfolio->save();
		$portfolio = new Portfolio();
		$portfolio->find($id)->category()->sync(Input::get('category'));
		$portfolio = new Portfolio();
		$portfolio->find($id)->customer()->sync(Input::get('customer'));
		return Redirect::route('admin.portfolios.index')
		->with('class_portfolio',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}
	public function destroy($id){
		$portfolio = Portfolio::find($id);
		if(File::exists($portfolio->img1)){
			File::delete(array(public_path($portfolio->img1)));
		}if(File::exists($portfolio->img2)){
			File::delete(array(public_path($portfolio->img2)));
		}if(File::exists($portfolio->img3)){
			File::delete(array(public_path($portfolio->img3)));
		}if(File::exists($portfolio->thumbnail)){
			File::delete(array(public_path($portfolio->thumbnail)));
		}
		Portfolio::destroy($id);
		return Redirect::route('admin.portfolios.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_portfolio',"class='open active'")
		->with('title','لیست پروژه ها');
	}
}