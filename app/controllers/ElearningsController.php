<?php
use Intervention\Image\ImageManagerStatic as Imagic;
use \Illuminate\Database\Eloquent\ModelNotFoundException;
class ElearningsController extends \BaseController {

	/**
	 * Display a listing of elearnings
	 *
	 * @return Response
	 */
	public function index()
	{
		$elearnings = Elearning::all();

		return View::make('backend.elearnings.index', array('elearnings'=>$elearnings))
		->with('title','کارگاه آموزشی')
        ->with('class_elearning',"class='open active'");
	}
	/**
	 * Show the form for creating a new elearning
	 *
	 * @return Response
	 */
	public function create()
	{
		$category = ElearningCategory::all();
		return View::make('backend.elearnings.create',array('category'=>$category))
		->with('title','ایجاد خبر')
        ->with('class_elearning',"class='open active'");
	}

	/**
	 * Store a newly created elearning in storage.
	 *
	 * @return Response
	 */
	public function store(){
		$validator = Validator::make($data = Input::all(), Elearning::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('file')) {
			$img1 = Input::file('file');
			$filename1 = $img1->getClientOriginalName();
			$path1 ="backend/assets/images/news/".$filename1;
			$width = Imagic::make($img1)->width();
			$height = Imagic::make($img1)->height();
            if($width == $height ){
                $image3 = Imagic::make($img1);
                $image3->resize(150,150);
                $image3->save(public_path($path1));
            }else{
                return Redirect::back()->withInput()->with('img','طول و عرض تصویر باهم برابر نیست');
            }
		}else{
			$path1 = "https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=500&h=239&txttrack=0";
		}
		$learning = Elearning::create(
			array(
				'img'=>$path1,
				'fa_title'=>Input::get('fa_title'),
				'fa_body'=>Input::get('fa_body'),
				'en_body'=>Input::get('en_body'),
				'ru_body'=>Input::get('ru_body'),
				'en_title'=>Input::get('en_title'),
				'ru_title'=>Input::get('ru_title'),
			)
		);
		$id = $learning->id;
		$learning = new Elearning();
		$learning->find($id)->category()->attach(Input::get('category'));
		return Redirect::route('admin.elearnings.index')
			->with('success','اطلاعات جدید با موفقیت ذخیره شد')
			->with('title','لیست اخبار ')
			->with('class_elearning','class="active open"');
	}
	/**
	 * Display the specified elearning.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$elearning = Elearning::findOrFail($id);
		return View::make('backend.elearnings.show', compact('elearning'));
	}
	/**
	 * Show the form for editing the specified elearning.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$result_ecat=[];
		$elearning = Elearning::find($id);
		$category = ElearningCategory::all();
		$portfoliocategory = $elearning->category;
		foreach($portfoliocategory as $pcat){
			$result_ecat[]=$pcat->fa_ecat_title;
		}
		return View::make('backend.elearnings.edit', array('result_ecat'=>$result_ecat,'elearning'=>$elearning,'category'=>$category))
		->with('class_elearning',"class='open active'")
		->with('title',"ویرایش خبر");
	}
	/**
	 * Update the specified elearning in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$elearning = new Elearning();
		$e = $elearning->findOrFail($id);
		$validator = Validator::make($data = Input::all(), Elearning::$rules);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('file')){
			$img1 = Input::file('file');
            $width = Imagic::make($img1)->width();
            $height = Imagic::make($img1)->height();
			$filename_img1 = $img1->getClientOriginalName();
			$path1 ="backend/assets/images/news/".$filename_img1;
            if($width == $height ){
                $image3 = Imagic::make($img1);
                $image3->resize(150,150);
                $image3->save(public_path($path1));
                File::delete(array(public_path($elearning->img)));
            }
			$data['file'] = $path1;
		}
		else{
			$data['file']=$elearning->img1;
		}
		$e->img = $data['file'];
		$e->fa_title = Input::get('fa_title');
		$e->ru_title = Input::get('ru_title');
		$e->en_title = Input::get('en_title');
		$e->en_body = Input::get('en_body');
		$e->ru_body = Input::get('ru_body');
		$e->fa_body = Input::get('fa_body');
		$e->save();
		$e->category()->sync(Input::get('category'));
		return Redirect::route('admin.elearnings.index')
		->with('class_elearning',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}
	/**
	 * Remove the specified elearning from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Elearning::destroy($id);
		return Redirect::route('admin.elearnings.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_elearning',"class='open active'")
		->with('title','لیست کارگاه آموزشی');
	}
}