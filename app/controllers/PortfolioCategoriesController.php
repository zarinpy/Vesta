<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class PortfolioCategoriesController extends \BaseController {

	/**
	 *
	 * Display a listing of projectcategories
	 *
	 */
	public function __construct()
	{
		//$this->beforeFilter('auth', array('except' => 'getLogin'));
		$this->beforeFilter('csrf', array('on' => 'post'));
		//$this->afterFilter('log', array('only' =>
			//array('fooAction', 'barAction')));
	}
	public function index()
	{
		$portfoliocategories = Portfoliocategory::all();
		return View::make('backend.portfolio_categories.index', compact('portfoliocategories'))
		->with('title','لیست دسته بندی ها')
        ->with('class_portfolio',"class='open active'");
	}

	/**
	 * Show the form for creating a new portfolio_categories
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.portfolio_categories.create')
		->with('title','ایجاد دسته بندی')
        ->with('class_portfolio',"class='open active'");
	}

	/**
	 * Store a newly created portfolio_categories in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), PortfolioCategory::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		PortfolioCategory::create($data);
		return Redirect::route('admin.portfolio_categories.index')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد')
		->with('title','دسته بندی')
		->with('class_portfolio','class="active open"');
	}
	/**
	 * Display the specified portfolio_categories.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$projectcategory = PortfolioCategory::findOrFail($id);
			return View::make('backend.portfolio_categories.show', compact('projectcategory'));
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.portfolio_categories.index');
		}
	}
	/**
	 * Show the form for editing the specified projectcategory.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$portfoliocategories = PortfolioCategory::findOrFail($id);
			return View::make('backend.portfolio_categories.edit', compact('portfoliocategories'))
				->with('class_portfolio',"class='open active'")
				->with('title',"ویرایش دسته بندی");
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.portfolio_categories.index');
		}
	}

	/**
	 * Update the specified projectcategory in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$projectcategory = PortfolioCategory::findOrFail($id);
		$validator = Validator::make($data = Input::all(), PortfolioCategory::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$projectcategory->update($data);
		return Redirect::route('admin.portfolio_categories.index')
		->with('class_portfolio',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}
	/**
	 * Remove the specified projectcategory from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		PortfolioCategory::destroy($id);
		return Redirect::route('admin.portfolio_categories.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_portfolio',"class='open active'")
		->with('title','لیست دسته بندی ها');
	}
}
