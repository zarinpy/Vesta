<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class PagesController extends \BaseController {

	/**
	 * Display a listing of pages
	 *
	 */
	public function index()
	{
		$pages = Page::all();
		return View::make('backend.pages.index', compact('pages'))
		->with('title','لیست صفحات')
        ->with('class_page',"class='open active'");
	}
	/**
	 * Show the form for creating a new page
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.pages.create')
		->with('title','ایجاد صفحه جدید')
        ->with('class_page',"class='open active'");
	}

	/**
	 * Store a newly created page in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$validator = Validator::make($data = Input::all(), Page::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Page::create($data);
		return Redirect::route('admin.pages.index')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد');
	}

	/**
	 * Display the specified page.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$page = Page::findOrFail($id);
            		return View::make('backend.pages.show', compact('page'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.pages.index');
		}
	}

	/**
	 * Show the form for editing the specified page.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$page = Page::find($id);
			return View::make('backend.pages.edit', compact('page'))
				->with('class_page',"class='open active'")
				->with('title',"ویرایش صفحه");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.pages.index');
		}
	}

	/**
	 * Update the specified page in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$page = Page::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Page::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$page->fa_title = Input::get('fa_title');
		$page->en_title = Input::get('en_title');
		$page->ru_title = Input::get('ru_title');
		$page->fa_content = Input::get('fa_content');
		$page->en_content = Input::get('en_content');
		$page->ru_content = Input::get('ru_content');
		$page->save();
		return Redirect::route('admin.pages.index')
		->with('class_page',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}
	/**
	 * Remove the specified page from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Page::destroy($id);

		return Redirect::route('admin.pages.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_page',"class='open active'");
	}

}
