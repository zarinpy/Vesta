<?php

class BaseController extends Controller {
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public function array_2d_to_1d ($input_array) {
		$output_array = array();
		for($i =0; $i<count($input_array); $i++){
            $output_array[] = $input_array[$i]['id'];
        }
		return $output_array;
	}
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
}
