<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Intervention\Image\ImageManagerStatic as Imagic;

class CompaniesController extends \BaseController {

	/**
	 * Display a listing of companies
	 *
	 */

	public function index()
	{
		$companies = Company::all();
		return View::make('backend.companies.index', compact('companies'))
		->with('title','لیست همکاران')
        ->with('class_portfolio',"class='open active'");
	}

	/**
	 * Show the form for creating a new company
	 *
	 * @return Response
	 */
	public function create()
	{
		$category = CompanyCategory::all();
		return View::make('backend.companies.create',compact('category'))
		->with('title','ایجاد همکار جدید')
        ->with('class_portfolio',"class='open active'");
	}

	/**
	 * Store a newly created company in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$path = null;
		$validator = Validator::make($data = Input::all(), Company::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('logo')){
			$imagefile = Input::file('logo');
			$filename = $imagefile->getClientOriginalName();
			$width = Imagic::make($imagefile)->width();
			$height = Imagic::make($imagefile)->height();
            $lens = $width - $height;
			if($lens >= 200){
                $image = Imagic::make($imagefile);
                $path ="backend/assets/images/company/".$filename;
                $image->save(public_path($path));
			}
			else{
				return Redirect::back()->withInput()->with('logo','طول و عرض تصویر مناسب نیست');
			}
		}
		else{
			$path = "https://placeholdit.imgix.net/~text?txtsize=19&bg=efefef&txtclr=aaaaaa&txt=no+image&w=125&h=125&txttrack=0";
		}
		$company = new Company();
		$company->logo = $path;
		$company->website = Input::get('website');
		$company->fa_name = Input::get('fa_name');
		$company->en_name = Input::get('en_name');
		$company->ru_name = Input::get('ru_name');
		$company->save();
		$id = $company->id;
		$category = new Company();
		$category->find($id)->category()->sync(Input::get('category'));
		return Redirect::route('admin.companies.index')
			->with('success','اطلاعات جدید با موفقیت ذخیره شد')
			->with('title','لیست همکاران')
			->with('class_portfolio','class="active open"');
	}
	/**
	 * Display the specified company.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
		{
		try{
			$company = Company::findOrFail($id);
			return View::make('backend.companies.show', compact('company'));
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.companies.index');
		}

	}

	/**
	 * Show the form for editing the specified company.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$result_pcat=array();
			$company = Company::findOrFail($id);
			$category = CompanyCategory::all();
			$companycategory = $company->category;
			foreach($companycategory as $ccat){
				$result_pcat[]=$ccat->fa_name;
			}
			return View::make('backend.companies.edit', array('category'=>$category,
				'company'=>$company,
				'company_category'=>$companycategory,
				'result_pcat'=>$result_pcat))
				->with('class_company',"class='open active'")
				->with('title',"ویرایش همکار");
		}catch (ModelNotFoundException $e){
			return Redirect::route('admin.companies.index');
		}
	}
	/**
	 * Update the specified company in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$company = Company::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Company::$rules);
		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		if(Input::hasFile('logo')){
			$logo = Company::find($id); File::delete(array(public_path($logo->logo)));
			$imagefile = Input::file('logo');
			$filename = time().$imagefile->getClientOriginalName();
			$path ="backend/assets/images/company/".$filename;
			$company->logo = Input::file('logo');  $company->logo = $path;
			Imagic::make($imagefile->getRealPath())->save(public_path($path));
		}
		$company->fa_name = Input::get('fa_name'); $company->website = Input::get('website');
		$company->en_name = Input::get('en_name');
		$company->ru_name = Input::get('ru_name');
		$company->save();
		$company = new Company();
		$company->find($id)->category()->sync(Input::get('category'));
		return Redirect::route('admin.companies.index')
		->with('class_portfolio',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}
	/**
	 * Remove the specified company from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$company = Company::find($id);
		if(File::exists($company->logo)){
			File::delete(array(public_path($company->logo)));
		}
		Company::destroy($id);
		return Redirect::route('admin.companies.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_portfolio',"class='open active'")
		->with('title','لیست همکاران');
	}
}
