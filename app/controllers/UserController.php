<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends \BaseController {

    /**
     * Display a listing of companies
     *
     */
    public function index()
    {
        $users = User::all();
        return View::make('backend.users.index', compact('users'))
            ->with('title','لیست کاربران')
            ->with('class_datasheets',"class='open active'");
    }
    /**
     * Show the form for creating a new company
     *
     * @return Response
     */
    public function create()
    {
        $group = Group::all();
        return View::make('backend.users.create',compact('group'))
            ->with('title','ایجاد کاربر جدید')
            ->with('class_datasheets',"class='open active'");
    }
    /**
     * Store a newly created company in storage.
     *
     * @return Response
     */
    public function store()
    {
        /*echo "<pre>";
        print_r(Input::all());exit();*/
        $validator = Validator::make($data = Input::all(), User::$rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }
           $user = User::firstOrCreate(array(
                    'name'=>Input::get('name'),
                    'family'=>Input::get('family'),
                    'email'=>Input::get('email'),
                    'password'=>Hash::make($data['password']),
                    'active'=>Input::get('active'),
                ));
            $id = $user->id;
            $user = new User();
            $user->find($id)->group()->attach(Input::get('group'));
            return Redirect::route('admin.users.index');
    }

    /**
     * Display the specified company.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        try{
            $user = Company::findOrFail($id);
            return View::make('backend.users.show', compact('user'));
        }catch (ModelNotFoundException $e){
            return Redirect::route('admin.users.index');
        }
    }

    /**
     * Show the form for editing the specified company.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        try{
            $user = User::findOrFail($id);
            $gp = Group::all();
            return View::make('backend.users.edit', compact('user','gp'))
                ->with('class_datasheets',"class='open active'")
                ->with('title',"ویرایش گروه");
        }catch (ModelNotFoundException $e){
            return Redirect::route('admin.users.index');
        }
    }

    /**
     * Update the specified company in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $rules = array(
            'name'=>'required',
            'family'=>'required',
            'email'=>'required',
        );
        $user = User::findOrFail($id);
        $validator = Validator::make($data = Input::all(), $rules);
        if ($validator->fails()){
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $user->name = Input::get('name'); $user->family = Input::get('family');
        $user->email = Input::get('email'); $user->active = Input::get('active');
        $user->save();
        return Redirect::route('admin.users.index')
            ->with('class_datasheets',"class='open active'")
            ->with('update','اطلاعات با موفقیت ویرایش شد');
    }

    /**
     * Remove the specified company from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        User::destroy($id);
        return Redirect::route('admin.users.index')
            ->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
            ->with('class_datasheets',"class='open active'");
    }
    public function loginshow(){

        return View::make('backend.login');
    }
    public function authenticate(){

        $rules = array(
            'email' => 'required|email',
            'password' => 'required|alpha_num|between:4,20',
        );
        $validator = Validator::make(Input::all(), $rules);
        $users = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }else{
            if (Auth::attempt($users)){
                $arr = array();
                foreach (Auth::user()->group as $gp){
                    $arr[] = $gp->en_name;
                }
                $user_gp = $arr[0];
                if($user_gp == 'admin'){
                    return Redirect::intended('admin.dashboard');
                }else{
                    return Redirect::intended('/'.App::getLocale());
                }
            }else{
                return Redirect::route('login')->with('message', 'نام کاربری و یا رمز عبور شما اشتباه است')->withInput();
            }
        }
    }
    public function logout(){
        Auth::logout();
        return View::make('front.index');
    }
}
