<?php
use Illuminate\Database\Eloquent\ModelNotFoundException;
class DatasheetCustomersController extends \BaseController {
	/**
	 * Display a listing of datasheetcustomers
	 *
	 * @return Response
	 */
	public function index()
	{
		$datasheetcustomers = Datasheetcustomer::all();
		return View::make('backend.datasheet_customers.index', compact('datasheetcustomers'))
		->with('title','لیست مشتریان')
        ->with('class_datasheets',"class='open active'");
	}
	/**
	 * Show the form for creating a new datasheetcustomer
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('backend.datasheet_customers.create')
        ->with('class_datasheets',"class='open active'")
        ->with('title',"ایجاد مشتری");
	}
	/**
	 * Store a newly created datasheetcustomer in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Datasheetcustomer::$rules);
		if ($validator->fails()){
			return Redirect::back()->withErrors($validator)->withInput();
		}
		try{
			Sentry::createGroup(array(
				'name' => $data['en_name'],
				'permissions' => array(
					'admin.create'=>1,
					'admin.update'=>1,
					'admin.delete'=>1,
					'admin.search'=>1,
					'user.search'=>0,
					'user.create'=>0,
					'user.update'=>0,
					'user.delete'=>0,
				)
			));
			Datasheetcustomer::Create($data);
		}catch (Cartalyst\Sentry\Groups\GroupExistsException $e){
			return Redirect::back()->withInput()->with('warning','نام انگلیسی مشتری تکراری می باشد');
		}
		return Redirect::route('admin.datasheet_customers.index')
		->with('success','اطلاعات جدید با موفقیت ذخیره شد')
		->with('class_datasheets','class="active open"');
	}

	/**
	 * Display the specified datasheetcustomer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$datasheetcustomer = Datasheetcustomer::findOrFail($id);
            		return View::make('backend.datasheet_customers.show', compact('datasheetcustomer'));
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.datasheet_customers.index');
		}
	}
	/**
	 * Show the form for editing the specified datasheetcustomer.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		try{
			$datasheetcustomer = Datasheetcustomer::find($id);
			return View::make('backend.datasheet_customers.edit', compact('datasheetcustomer'))
				->with('class_datasheets',"class='open active'")
				->with('title',"ویرایش مشتری");
		}catch(ModelNotFoundException $e){
			return Redirect::route('admin.datasheet_customers.index');
		}
	}

	/**
	 * Update the specified datasheetcustomer in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$datasheetcustomer = Datasheetcustomer::findOrFail($id);
		$validator = Validator::make($data = Input::all(), Datasheetcustomer::$rules);
		if ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		}
		$gp = Sentry::findGroupByName($datasheetcustomer->en_name);
		$gp->name = $data['en_name'];
		$gp->save();
		$datasheetcustomer->update($data);
		return Redirect::route('admin.datasheet_customers.index')
		->with('class_datasheets',"class='open active'")
		->with('update','اطلاعات با موفقیت ویرایش شد');
	}
	/**
	 * Remove the specified datasheetcustomer from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Datasheetcustomer::destroy($id);
		return Redirect::route('admin.datasheet_customers.index')
		->with('success','اطلاعات مورد نظر با موفقیت حذف شد')
		->with('class_datasheets',"class='open active'");
	}
}
