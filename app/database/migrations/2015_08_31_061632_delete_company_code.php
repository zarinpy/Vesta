<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteCompanyCode extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('datasheet_codes',function(Blueprint $table){
			$table->dropColumn('Company_id');
			$table->dropColumn('ProjectType_id');
			$table->dropColumn('code');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('datasheet_codes',function(Blueprint $table){
			$table->integer('Company_id');
			$table->integer('ProjectType_id');
			$table->string('code');
		});
	}

}
