<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RussianLang extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up(){
		Schema::table('companies',function(Blueprint $table){
			$table->string('ru_name',30)->nullable();
		});
		Schema::table('elearnings',function(Blueprint $table){
			$table->string('ru_title',100)->nullable();
			$table->text('ru_body')->nullable();
		});
		Schema::table('elearning_categories',function(Blueprint $table){
			$table->string('ru_ecat_title',50)->nullable();
		});
		Schema::table('elearning_categories',function(Blueprint $table){
			$table->string('ru_name',30)->nullable();
		});
		Schema::table('pages',function(Blueprint $table){
			$table->string('ru_title',50)->nullable();
			$table->string('ru_content',50)->nullable();
		});
		Schema::table('portfolios',function(Blueprint $table){
			$table->string('ru_title',25)->nullable();
			$table->text('ru_body')->nullable();
		});
		Schema::table('portfolio_categories',function(Blueprint $table){
			$table->string('ru_pcat_title',30)->nullable();
		});
		Schema::table('products',function(Blueprint $table){
			$table->string('ru_title',30)->nullable();
			$table->text('ru_description')->nullable();
		});
		Schema::table('product_categories',function(Blueprint $table){
			$table->string('ru_name',30)->nullable();
		});
		Schema::table('services',function(Blueprint $table){
			$table->string('ru_title',25)->nullable();
			$table->text('ru_body')->nullable();
		});
		Schema::table('service_categories',function(Blueprint $table){
			$table->string('ru_title',25)->nullable();
			$table->text('ru_description')->nullable();
		});
		Schema::table('teams',function(Blueprint $table){
			$table->string('ru_name',30)->nullable();
			$table->string('ru_family',30)->nullable();
			$table->text('ru_description')->nullable();
		});
		Schema::table('team_categories',function(Blueprint $table){
			$table->string('ru_cat_title',30)->nullable();
		});
		Schema::table('team_profession_categories',function(Blueprint $table){
			$table->string('ru_prof_title',30)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
