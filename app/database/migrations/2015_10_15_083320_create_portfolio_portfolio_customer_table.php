<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePortfolioPortfolioCustomerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('portfolio_portfolio_customer', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('portfolio_id')->unsigned()->index();
			$table->foreign('portfolio_id')->references('id')->on('portfolios')->onDelete('cascade');
			$table->integer('portfolio_customer_id')->unsigned()->index();
			$table->foreign('portfolio_customer_id')->references('id')->on('portfolio_customers')->onDelete('cascade');
			$table->timestamps();
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('portfolio_portfolio_customer');
	}

}
