<?php

//use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ImageSize extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE vtf_projects MODIFY COLUMN img1	VARCHAR(300);");
		DB::statement("ALTER TABLE vtf_projects MODIFY COLUMN img2	VARCHAR(300);");
		DB::statement("ALTER TABLE vtf_projects MODIFY COLUMN img3	VARCHAR(300);");
		DB::statement("ALTER TABLE vtf_teams 	MODIFY COLUMN image	VARCHAR(300);");
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}
}
