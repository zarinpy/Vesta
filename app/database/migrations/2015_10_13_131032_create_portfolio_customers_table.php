<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
class CreatePortfolioCustomersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('portfolio_customers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('fa_name',50);
			$table->string('en_name',50)->nullable();
			$table->string('ru_name',50)->nullable();
			$table->timestamps();
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('portfolio_customers');
	}
}