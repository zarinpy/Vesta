<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DatasheetCodeChange extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		Schema::table('datasheet_codes',function(Blueprint $table){
			$table->string('code',50);
			$table->integer('Company_id')->unsigned()->index();
			$table->foreign('Company_id')->references('id')->on('Datasheet_companies')->onDelete('cascade');
			$table->integer('ProjectType_id')->unsigned()->index();
			$table->foreign('ProjectType_id')->references('id')->on('Datasheet_project_types')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
