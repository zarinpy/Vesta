<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EnDatasheet extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('datasheets',function(Blueprint $table){
			$table->renameColumn('name','fa_name');
			$table->text('en_name');
			$table->text('en_description');
			$table->renameColumn('description','fa_description');


		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('datasheets',function(Blueprint $table){
			$table->renameColumn('fa_name','name');
			$table->dropColumn('en_name');
			$table->dropColumn('en_description');
			$table->renameColumn('fa_description','description');


		});
	}

}
