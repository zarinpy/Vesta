<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MultiLang extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('companies',function(Blueprint $table){
			$table->string('en_name');
		});
		Schema::table('elearnings',function(Blueprint $table){
			$table->string('en_title');
			$table->string('en_body');
		});
		Schema::table('elearning_categories',function(Blueprint $table){
			$table->string('en_ecat_title');
		});
		Schema::table('menus',function(Blueprint $table){
			$table->string('en_name');
		});
		Schema::table('portfolios',function(Blueprint $table){
			$table->string('en_title');
			$table->string('en_body');
		});
		Schema::table('portfolio_categories',function(Blueprint $table){
			$table->string('en_pcat_title');
		});
		Schema::table('products',function(Blueprint $table){
			$table->string('en_title');
			$table->string('en_description');
		});
		Schema::table('product_categories',function(Blueprint $table){
			$table->string('en_name');
		});
		Schema::table('teams',function(Blueprint $table){
			$table->string('en_name');
			$table->string('en_family');
			$table->string('en_description');
		});
		Schema::table('team_categories',function(Blueprint $table){
			$table->string('en_cat_title');
		});
		Schema::table('team_profession_categories',function(Blueprint $table){
			$table->string('en_prof_title');
		});
		Schema::table('pages',function(Blueprint $table){
			$table->string('en_title');
			$table->string('en_content');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('companies',function(Blueprint $table){
			$table->dropColumn('en_name');
		});
		Schema::table('elearnings',function(Blueprint $table){
			$table->dropColumn('en_title');
			$table->dropColumn('en_body');
		});
		Schema::table('elearning_categories',function(Blueprint $table){
			$table->dropColumn('en_ecat_title');
		});
		Schema::table('menus',function(Blueprint $table){
			$table->dropColumn('en_name');
		});
		Schema::table('menus',function(Blueprint $table){
			$table->dropColumn('en_title');
			$table->dropColumn('en_content');
		});
		Schema::table('portdolios',function(Blueprint $table){
			$table->dropColumn('en_title');
			$table->dropColumn('en_body');
		});
		Schema::table('portfolio_categories',function(Blueprint $table){
			$table->dropColumn('en_pcat_title');
		});
		Schema::table('products',function(Blueprint $table){
			$table->dropColumn('en_title');
			$table->dropColumn('en_description');
		});
		Schema::table('product_categories',function(Blueprint $table){
			$table->dropColumn('en_name');
		});
		Schema::table('teams',function(Blueprint $table){
			$table->dropColumn('en_name');
			$table->dropColumn('en_family');
			$table->dropColumn('en_description');
		});
		Schema::table('team_categories',function(Blueprint $table){
			$table->dropColumn('en_cat_title');
		});
		Schema::table('team_profession_categories',function(Blueprint $table){
			$table->dropColumn('en_prof_title');
		});
	}

}
