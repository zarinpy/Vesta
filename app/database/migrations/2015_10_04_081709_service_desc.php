<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServiceDesc extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('service_categories',function(Blueprint $t){
			$t->renameColumn('description','fa_description');
			$t->text('en_description');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('service_categories',function(Blueprint $t){
			$t->renameColumn('fa_description','description');
			$t->dropColumn('en_description');
		});
	}

}
