<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
class CreateDatasheetDatasheetCodeTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('datasheet_datasheet_code', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('datasheet_id')->unsigned()->index();
			$table->foreign('datasheet_id')->references('id')->on('datasheets')->onDelete('cascade');
			$table->integer('datasheet_code_id')->unsigned()->index();
			$table->foreign('datasheet_code_id')->references('id')->on('datasheet_codes')->onDelete('cascade');
			$table->timestamps();
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('datasheet_datasheet_code');
	}
}
