<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DelMenu extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('menus',function(Blueprint $table){
			$table->drop();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::create('menus', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title',25);
			$table->string('url',120);
			$table->boolean('active',true);
			$table->integer('parent',0)->nullable();
			$table->boolean('new_page',0);
		});
	}

}
