<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DatasheetProjectType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('datasheet_project_types',function(Blueprint $table){
			$table->increments('id');
			$table->string('fa_name',40);
			$table->string('en_name',40);
			$table->timestamps();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('datasheet_project_types',function(Blueprint $table){
			$table->drop('datasheet_project_types');
		});
	}

}
