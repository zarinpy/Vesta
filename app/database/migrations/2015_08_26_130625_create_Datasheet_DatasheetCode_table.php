<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDatasheetDatasheetCodeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Datasheet_Datasheet_Code', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('Datasheet_id')->unsigned()->index();
			$table->foreign('Datasheet_id')->references('id')->on('Datasheets')->onDelete('cascade');
			$table->integer('DatasheetCode_id')->unsigned()->index();
			$table->foreign('DatasheetCode_id')->references('id')->on('Datasheet_Codes')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Datasheet_DatasheetCode');
	}

}
