<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateElearningElearningCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('elearning_elearning_category', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('elearning_id')->unsigned()->index();
			$table->foreign('elearning_id')->references('id')->on('elearnings')->onDelete('cascade');
			$table->integer('elearning_category_id')->unsigned()->index();
			$table->foreign('elearning_category_id')->references('id')->on('elearning_categories')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('elearning_elearning_category');
	}

}
