<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePortfolioPortfolioCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('portfolio_portfolio_category', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('portfolio_id')->unsigned()->index();
			$table->foreign('portfolio_id')->references('id')->on('portfolios')->onDelete('cascade');
			$table->integer('portfolio_category_id')->unsigned()->index();
			$table->foreign('portfolio_category_id')->references('id')->on('portfolio_categories')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('portfolio_portfolio_category');
	}

}
