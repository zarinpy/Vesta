<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDatasheetsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('datasheets', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name',50);
			$table->string('file',500);
			$table->integer('company');
			$table->text('description');
			$table->integer('project_type');
			$table->timestamps();
		});
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('datasheets');
	}
}