<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Timestamp extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('teams',function(Blueprint $table){
			$table->timestamps();
		});
		Schema::table('team_categories',function(Blueprint $table){
			$table->timestamps();
		});
		Schema::table('team_profession_categories',function(Blueprint $table){
			$table->timestamps();
		});
		Schema::table('menus',function(Blueprint $table){
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
