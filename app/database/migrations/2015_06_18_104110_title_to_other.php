<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TitleToOther extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('team_categories',function(Blueprint $table){
			$table->renameColumn('title','cat_title');
		});
		Schema::table('team_profession_categories',function(Blueprint $table){
			$table->renameColumn('title','prof_title');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
