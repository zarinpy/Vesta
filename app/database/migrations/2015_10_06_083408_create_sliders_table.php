<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSlidersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sliders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('img',400);
			$table->string('fa_slug1',200);
			$table->string('fa_slug2',200);

			$table->string('en_slug1',200)->nullable();
			$table->string('en_slug2',200)->nullable();

			$table->string('ru_slug1',200)->nullable();
			$table->string('ru_slug2',200)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sliders');
	}

}
